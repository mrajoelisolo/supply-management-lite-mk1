﻿namespace SupplyManagementLiteMk1.MainApp
{
    partial class FormMenu
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMenu));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnInput = new System.Windows.Forms.Button();
            this.btnBenefits = new System.Windows.Forms.Button();
            this.btnStockManagement = new System.Windows.Forms.Button();
            this.btnOutput = new System.Windows.Forms.Button();
            this.btnIOManagement = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Navy;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnInput, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnBenefits, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnStockManagement, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.btnOutput, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnIOManagement, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(105, 566);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SupplyManagementLiteMk1.Properties.Resources.Banner;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 87);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // btnInput
            // 
            this.btnInput.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnInput.Enabled = false;
            this.btnInput.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInput.ForeColor = System.Drawing.Color.White;
            this.btnInput.Image = global::SupplyManagementLiteMk1.Properties.Resources.Input;
            this.btnInput.Location = new System.Drawing.Point(18, 199);
            this.btnInput.Name = "btnInput";
            this.btnInput.Size = new System.Drawing.Size(70, 70);
            this.btnInput.TabIndex = 3;
            this.btnInput.Text = "Miditra";
            this.btnInput.UseVisualStyleBackColor = false;
            this.btnInput.Click += new System.EventHandler(this.btnInput_Click);
            // 
            // btnBenefits
            // 
            this.btnBenefits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnBenefits.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnBenefits.Enabled = false;
            this.btnBenefits.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBenefits.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBenefits.ForeColor = System.Drawing.Color.White;
            this.btnBenefits.Image = global::SupplyManagementLiteMk1.Properties.Resources.Benefits;
            this.btnBenefits.Location = new System.Drawing.Point(18, 387);
            this.btnBenefits.Name = "btnBenefits";
            this.btnBenefits.Size = new System.Drawing.Size(70, 70);
            this.btnBenefits.TabIndex = 6;
            this.btnBenefits.Text = "Vola miditra";
            this.btnBenefits.UseVisualStyleBackColor = false;
            this.btnBenefits.Click += new System.EventHandler(this.btnBenefits_Click);
            // 
            // btnStockManagement
            // 
            this.btnStockManagement.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnStockManagement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnStockManagement.Enabled = false;
            this.btnStockManagement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStockManagement.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStockManagement.ForeColor = System.Drawing.Color.White;
            this.btnStockManagement.Image = global::SupplyManagementLiteMk1.Properties.Resources.Stock;
            this.btnStockManagement.Location = new System.Drawing.Point(18, 482);
            this.btnStockManagement.Name = "btnStockManagement";
            this.btnStockManagement.Size = new System.Drawing.Size(70, 70);
            this.btnStockManagement.TabIndex = 2;
            this.btnStockManagement.Text = "Stock";
            this.btnStockManagement.UseVisualStyleBackColor = false;
            this.btnStockManagement.Click += new System.EventHandler(this.btnStockManagement_Click);
            // 
            // btnOutput
            // 
            this.btnOutput.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOutput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnOutput.Enabled = false;
            this.btnOutput.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOutput.ForeColor = System.Drawing.Color.White;
            this.btnOutput.Image = global::SupplyManagementLiteMk1.Properties.Resources.Output;
            this.btnOutput.Location = new System.Drawing.Point(18, 105);
            this.btnOutput.Name = "btnOutput";
            this.btnOutput.Size = new System.Drawing.Size(70, 70);
            this.btnOutput.TabIndex = 4;
            this.btnOutput.Text = "Mivoaka";
            this.btnOutput.UseVisualStyleBackColor = false;
            this.btnOutput.Click += new System.EventHandler(this.btnOutput_Click);
            // 
            // btnIOManagement
            // 
            this.btnIOManagement.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnIOManagement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnIOManagement.Enabled = false;
            this.btnIOManagement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIOManagement.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIOManagement.ForeColor = System.Drawing.Color.White;
            this.btnIOManagement.Image = global::SupplyManagementLiteMk1.Properties.Resources.InputOutput;
            this.btnIOManagement.Location = new System.Drawing.Point(18, 293);
            this.btnIOManagement.Name = "btnIOManagement";
            this.btnIOManagement.Size = new System.Drawing.Size(70, 70);
            this.btnIOManagement.TabIndex = 5;
            this.btnIOManagement.Text = "Vola";
            this.btnIOManagement.UseVisualStyleBackColor = false;
            this.btnIOManagement.Click += new System.EventHandler(this.btnIOManagement_Click);
            // 
            // FormMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 566);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "FormMenu";
            this.Text = "Supply management Lite mk1";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnStockManagement;
        private System.Windows.Forms.Button btnBenefits;
        private System.Windows.Forms.Button btnIOManagement;
        private System.Windows.Forms.Button btnOutput;
        private System.Windows.Forms.Button btnInput;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}