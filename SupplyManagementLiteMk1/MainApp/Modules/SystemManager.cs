using System;
using System.Collections.Generic;
using System.Text;
using TFramework.Reporting;
using System.IO;
using System.Reflection;

namespace SupplyManagementLiteMk1.MainApp.Modules
{
    public class SystemManager
    {
        private static SystemManager m_Instance = new SystemManager();       

        private SystemManager() { }

        public static SystemManager Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public void ResetSystem()
        {
            MSWordUtilities.Instance.Dispose();
        }

        public String GetSystemDirectory()
        {
            String res = Assembly.GetAssembly(typeof(SystemManager)).Location;
            return Directory.GetParent(res).FullName;
        }

        public String ReportFilter
        {
            get
            {
                return "Fichiers Word 97/2000 (*.doc)|*.doc|Fichiers Word 2007 (*.docx)|*.docx";
            }
        }
    }
}
