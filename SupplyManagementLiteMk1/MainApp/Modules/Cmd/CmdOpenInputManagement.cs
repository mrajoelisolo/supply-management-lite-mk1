using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Misc;
using SupplyManagementLiteMk1.StockManagement;
using SupplyManagementLiteMk1.InputManagement;

namespace SupplyManagementLiteMk1.MainApp.Modules.Cmd
{
    public class CmdOpenInputManagement : ICommand
    {
        private Form m_Parent;

        public CmdOpenInputManagement(Form parent)
        {
            m_Parent = parent;
        }

        public void Doing()
        {
            MDIManager.Instance.ShowMDIChild(m_Parent, typeof(FormInputManagement), FormWindowState.Maximized);  
        }
    }
}
