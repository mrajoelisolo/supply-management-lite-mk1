using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Misc;
using SupplyManagementLiteMk1.StoryManagement;

namespace SupplyManagementLiteMk1.MainApp.Modules.Cmd
{
    public class CmdOpenHistoryManagement : ICommand
    {
        private Form m_Parent;

        public CmdOpenHistoryManagement(Form parent)
        {
            m_Parent = parent;
        }

        public void Doing()
        {
            //MDIManager.Instance.ShowMDIChild(m_Parent, typeof(FormPaymentManagement), FormWindowState.Maximized);
            MDIManager.Instance.ShowMDIChild(m_Parent, typeof(FormStoryManagement), FormWindowState.Maximized);
        }
    }
}
