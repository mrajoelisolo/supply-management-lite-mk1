using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Misc;
using SupplyManagementLiteMk1.BenefitsManagement;

namespace SupplyManagementLiteMk1.MainApp.Modules.Cmd
{
    public class CmdOpenBenefits : ICommand
    {
        private Form m_Parent;

        public CmdOpenBenefits(Form parent)
        {
            m_Parent = parent;
        }

        public void Doing() 
        {
            MDIManager.Instance.ShowMDIChild(m_Parent, typeof(FormBenefitsManagement), FormWindowState.Maximized);
        }
    }
}
