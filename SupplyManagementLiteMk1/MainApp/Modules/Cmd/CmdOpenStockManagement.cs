using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.StockManagement;
using SupplyManagementLiteMk1.Misc;

namespace SupplyManagementLiteMk1.MainApp.Modules.Cmd
{
    public class CmdOpenStockManagement : ICommand
    {
        private Form m_Parent;

        public CmdOpenStockManagement(Form parent)
        {
            m_Parent = parent;
        }

        public void Doing()
        {            
            //FormStockManagement f = new FormStockManagement();
            //f.MdiParent = m_Parent;
            //f.WindowState = FormWindowState.Maximized;
            //f.Show();
            MDIManager.Instance.ShowMDIChild(m_Parent, typeof(FormStockManagement), FormWindowState.Maximized);
        }
    }
}
