using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Misc;
using SupplyManagementLiteMk1.OutputManagement;

namespace SupplyManagementLiteMk1.MainApp.Modules.Cmd
{
    public class CmdOpenOutputManagement : ICommand
    {
        private Form m_Parent;

        public CmdOpenOutputManagement(Form parent)
        {
            m_Parent = parent;
        }

        public void Doing()
        {
            MDIManager.Instance.ShowMDIChild(m_Parent, typeof(FormOutputManagement), FormWindowState.Maximized);  
        }
    }
}
