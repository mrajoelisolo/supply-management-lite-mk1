using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SupplyManagementLiteMk1.MainApp
{
    public partial class FormSplash : Form
    {
        private int m_Count = 0;

        public FormSplash()
        {
            InitializeComponent();

            CenterToScreen();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            m_Count++;

            if (m_Count > 3)
            {
                timer1.Enabled = false;
                FormMain f = new FormMain();
                f.Show();
                this.Visible = false;
            }
        }
    }
}