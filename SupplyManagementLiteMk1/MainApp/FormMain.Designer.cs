﻿namespace SupplyManagementLiteMk1.MainApp
{
    partial class FormMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnOpenApplication = new System.Windows.Forms.Button();
            this.btnConnectionSetup = new System.Windows.Forms.Button();
            this.btnGenerateDB = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblCopyright = new System.Windows.Forms.LinkLabel();
            this.btnProfileManagement = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(632, 446);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::SupplyManagementLiteMk1.Properties.Resources.WelcomeAdminBcg;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(632, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.btnProfileManagement, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.btnOpenApplication, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnConnectionSetup, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnGenerateDB, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 103);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(626, 290);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // btnOpenApplication
            // 
            this.btnOpenApplication.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenApplication.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpenApplication.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenApplication.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenApplication.Image")));
            this.btnOpenApplication.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOpenApplication.Location = new System.Drawing.Point(3, 15);
            this.btnOpenApplication.Name = "btnOpenApplication";
            this.btnOpenApplication.Size = new System.Drawing.Size(620, 42);
            this.btnOpenApplication.TabIndex = 2;
            this.btnOpenApplication.Text = "Hanokatra ny fitantanana ny \"stock\"";
            this.btnOpenApplication.UseVisualStyleBackColor = true;
            this.btnOpenApplication.Click += new System.EventHandler(this.btnOpenApplication_Click);
            // 
            // btnConnectionSetup
            // 
            this.btnConnectionSetup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConnectionSetup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConnectionSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnectionSetup.Image = ((System.Drawing.Image)(resources.GetObject("btnConnectionSetup.Image")));
            this.btnConnectionSetup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConnectionSetup.Location = new System.Drawing.Point(3, 87);
            this.btnConnectionSetup.Name = "btnConnectionSetup";
            this.btnConnectionSetup.Size = new System.Drawing.Size(620, 42);
            this.btnConnectionSetup.TabIndex = 4;
            this.btnConnectionSetup.Text = "Hanova ny \"paramètres de connexion\"";
            this.btnConnectionSetup.UseVisualStyleBackColor = true;
            this.btnConnectionSetup.Click += new System.EventHandler(this.btnConnectionSetup_Click);
            // 
            // btnGenerateDB
            // 
            this.btnGenerateDB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerateDB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerateDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateDB.Image = ((System.Drawing.Image)(resources.GetObject("btnGenerateDB.Image")));
            this.btnGenerateDB.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGenerateDB.Location = new System.Drawing.Point(3, 160);
            this.btnGenerateDB.Name = "btnGenerateDB";
            this.btnGenerateDB.Size = new System.Drawing.Size(620, 40);
            this.btnGenerateDB.TabIndex = 5;
            this.btnGenerateDB.Text = "[Administrateur] Initialiser la base de données";
            this.btnGenerateDB.UseVisualStyleBackColor = true;
            this.btnGenerateDB.Click += new System.EventHandler(this.btnGenerateDB_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.lblCopyright, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 396);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(632, 50);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // lblCopyright
            // 
            this.lblCopyright.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lblCopyright.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCopyright.AutoSize = true;
            this.lblCopyright.Location = new System.Drawing.Point(380, 18);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(249, 13);
            this.lblCopyright.TabIndex = 5;
            this.lblCopyright.TabStop = true;
            this.lblCopyright.Text = "Copyright © Myrmidon Software Engineering 2011";
            this.lblCopyright.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblCopyright_LinkClicked);
            // 
            // btnProfileManagement
            // 
            this.btnProfileManagement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProfileManagement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProfileManagement.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProfileManagement.Image = ((System.Drawing.Image)(resources.GetObject("btnProfileManagement.Image")));
            this.btnProfileManagement.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProfileManagement.Location = new System.Drawing.Point(3, 233);
            this.btnProfileManagement.Name = "btnProfileManagement";
            this.btnProfileManagement.Size = new System.Drawing.Size(620, 40);
            this.btnProfileManagement.TabIndex = 6;
            this.btnProfileManagement.Text = "Gérer les profils utilisateurs";
            this.btnProfileManagement.UseVisualStyleBackColor = true;
            this.btnProfileManagement.Click += new System.EventHandler(this.btnProfileManagement_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(632, 446);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "Tongasoa";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnOpenApplication;
        private System.Windows.Forms.Button btnConnectionSetup;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.LinkLabel lblCopyright;
        private System.Windows.Forms.Button btnGenerateDB;
        private System.Windows.Forms.Button btnProfileManagement;

    }
}