using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.MainApp.Cmd;
using TFramework.Reporting;
using SupplyManagementLiteMk1.Datasource;
using SupplyManagementLiteMk1.MainApp.Modules;

namespace SupplyManagementLiteMk1.MainApp
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();

            CenterToParent();
        }

        private void btnOpenApplication_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdOpenApplication(this);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnConnectionSetup_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdOpenConnectionSetup(this);
            Invoker.Instance.Invoke(cmd);
        }

        private void lblCopyright_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ICommand cmd = new CmdAboutBox(this);
            Invoker.Instance.Invoke(cmd);            
        }

        private void btnGenerateDB_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdGenerateDB();
            Invoker.Instance.Invoke(cmd);
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            SystemManager.Instance.ResetSystem();
            Application.Exit();
        }

        private void btnProfileManagement_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdOpenProfileManagement(this);
            Invoker.Instance.Invoke(cmd);
        }
    }
}