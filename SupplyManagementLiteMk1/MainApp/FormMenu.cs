using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.MainApp.Modules.Cmd;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.ProfileManagement.Util;

namespace SupplyManagementLiteMk1.MainApp
{
    public partial class FormMenu : Form
    {
        public FormMenu()
        {
            InitializeComponent();

            CenterToParent();
        }

        private void btnStockManagement_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdOpenStockManagement(this);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnInput_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdOpenInputManagement(this);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnOutput_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdOpenOutputManagement(this);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnIOManagement_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdOpenHistoryManagement(this);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnBenefits_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdOpenBenefits(this);
            Invoker.Instance.Invoke(cmd);
        }

        public void SetPrivilege(ProfileUtilities.EnumPrivilege privilege)
        {
            switch (privilege)
            {
                case ProfileUtilities.EnumPrivilege.ADMIN :
                {
                    btnOutput.Enabled = true;
                    btnInput.Enabled = true;
                    btnIOManagement.Enabled = true;
                    btnBenefits.Enabled = true;
                    btnStockManagement.Enabled = true;
                }break;

                case ProfileUtilities.EnumPrivilege.MAGASINIER :
                {
                    btnOutput.Enabled = true;
                    btnInput.Enabled = true;
                }break;

                case ProfileUtilities.EnumPrivilege.GERANT:
                {
                     btnOutput.Enabled = true;
                     btnInput.Enabled = true;
                     btnIOManagement.Enabled = true;
                     btnBenefits.Enabled = true;
                     btnStockManagement.Enabled = true;
                } break;
            }
        }
    }
}