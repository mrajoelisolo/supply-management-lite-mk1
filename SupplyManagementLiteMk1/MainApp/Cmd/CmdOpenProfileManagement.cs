﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.ProfileManagement;
using SupplyManagementLiteMk1.Misc;
using ORMFramework.DbUtil;

namespace SupplyManagementLiteMk1.MainApp.Cmd
{
    public class CmdOpenProfileManagement : ICommand
    {
        private Form m_Parent;

        public CmdOpenProfileManagement(Form parent)
        {
            m_Parent = parent;
        }

        public void Doing()
        {
            ConnectionDatasource cda = ConnectionDataSerializer.Instance.Deserialize();
            ConnectionManager.EnumConnectionResult res = ConnectionManager.Instance.Connect(cda);
            if (res == ConnectionManager.EnumConnectionResult.LOGON_FAILED)
            {
                MessageBox.Show("Impossible de se connecter au serveur,\nvérifier si le serveur est en marche et/ou\nles paramètres de connexion", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            bool b = SecurityUtilities.Instance.Authentify();
            if(!b) return;

            FormProfileManagement f = new FormProfileManagement();
            m_Parent.Hide();
            f.ShowDialog();
            m_Parent.Show();
        }
    }
}
