using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Misc;
using SupplyManagementLiteMk1.ConnectionManagement;

namespace SupplyManagementLiteMk1.MainApp.Cmd
{
    public class CmdOpenConnectionSetup : ICommand
    {
        private Form m_Parent;

        public CmdOpenConnectionSetup(Form parent)
        {
            m_Parent = parent;
        }

        public void Doing()
        {
            Boolean res = SecurityUtilities.Instance.Authentify();

            if (!res) return;
            FormConnectionSetup f = new FormConnectionSetup();
            f.ShowDialog();
        }
    }
}
