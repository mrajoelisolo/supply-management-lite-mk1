using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;

namespace SupplyManagementLiteMk1.MainApp.Cmd
{
    public class CmdAboutBox : ICommand
    {
        private Form m_Parent;

        public CmdAboutBox(Form parent)
        {
            m_Parent = parent;
        }

        public void Doing()
        {
            FormAbout f = new FormAbout();
            f.ShowDialog();
        }
    }
}
