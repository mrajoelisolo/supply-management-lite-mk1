using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.MainApp;
using ORMFramework.DbUtil;
using SupplyManagementLiteMk1.ProfileManagement;
using SupplyManagementLiteMk1.ProfileManagement.Util;

namespace SupplyManagementLiteMk1.MainApp.Cmd
{
    public class CmdOpenApplication : ICommand
    {
        private Form m_Parent;

        public CmdOpenApplication(Form parent)
        {
            m_Parent = parent;
        }

        public void Doing()
        {
            ConnectionDatasource cda = ConnectionDataSerializer.Instance.Deserialize();
            ConnectionManager.EnumConnectionResult res = ConnectionManager.Instance.Connect(cda);
            if (res == ConnectionManager.EnumConnectionResult.LOGON_FAILED)
            {
                MessageBox.Show("Impossible de se connecter au serveur,\nv�rifier si le serveur est en marche et/ou\nles param�tres de connexion", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //Authentification
            FormLogin f1 = new FormLogin();
            if (f1.ShowDialog() == DialogResult.Cancel) return;

            String login = f1.GetLogin();
            String pwd = f1.GetPassword();

            ProfileUtilities.EnumPrivilege privilege = ProfileUtilities.Instance.Authentify(login, pwd);
            if (privilege == ProfileUtilities.EnumPrivilege.ABORTED)
            {
                MessageBox.Show("Acc�s refus�", "Authentification", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            //Fin authentification

            m_Parent.Hide();
            FormMenu f = new FormMenu();
            f.SetPrivilege(privilege);
            f.ShowDialog();
            m_Parent.Show();
        }
    }
}
