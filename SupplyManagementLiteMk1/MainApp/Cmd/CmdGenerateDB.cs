using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.Misc;
using ORMFramework.DbUtil;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.MainApp.Cmd
{
    public class CmdGenerateDB : ICommand
    {
        public CmdGenerateDB()
        {
        }

        public void Doing()
        {
            FormSecretCode f = new FormSecretCode();
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.No) return;
            if (!f.GetPassword().Equals("admin")) return;

            ConnectionDatasource cda = ConnectionDataSerializer.Instance.Deserialize();
            ConnectionManager.EnumConnectionResult res = ConnectionManager.Instance.Connect(cda);

            if (ConnectionManager.Instance.Connect(cda) == ConnectionManager.EnumConnectionResult.LOGON_FAILED)
            {
                System.Windows.Forms.MessageBox.Show("Connection failed");
                return;
            }

            try
            {
                EntityLoader.Instance.ExecuteNonQuery("DROP DATABASE " + cda.Database + ";");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }

            StringBuilder str = new StringBuilder();
            str.Append("CREATE DATABASE `" + cda.Database + "`;");

            str.Append("DROP TABLE IF EXISTS `" + cda.Database + "`.`element_entrees`;");
            str.Append("CREATE TABLE  `" + cda.Database + "`.`element_entrees` (");
            str.Append("`idelement_entrees` bigint(20) unsigned NOT NULL auto_increment,");
            str.Append("`libelle` varchar(255) default NULL,");
            str.Append("`pu` float default NULL,");
            str.Append("`qte` float default NULL,");
            str.Append("`unite` varchar(255) default NULL,");
            str.Append("`facteur` float default NULL,");
            str.Append("`idhistorique_entrees` bigint(20) unsigned default NULL,");
            str.Append("`idstock` bigint(20) unsigned default NULL,");
            str.Append("PRIMARY KEY  (`idelement_entrees`)");
            str.Append(") ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;");

            str.Append("DROP TABLE IF EXISTS `" + cda.Database + "`.`element_sorties`;");
            str.Append("CREATE TABLE  `" + cda.Database + "`.`element_sorties` (");
            str.Append("`idelement_sorties` bigint(20) unsigned NOT NULL auto_increment,");
            str.Append("`libelle` varchar(255) default NULL,");
            str.Append("`pu` float default NULL,");
            str.Append("`qte` float default NULL,");
            str.Append("`unite` varchar(255) default NULL,");
            str.Append("`facteur` float default NULL,");
            str.Append("`idhistorique_sorties` bigint(20) unsigned default NULL,");
            str.Append("`idstock` bigint(20) unsigned default NULL,");            
            str.Append("PRIMARY KEY  (`idelement_sorties`)");            
            str.Append(") ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;");

            str.Append("DROP TABLE IF EXISTS `" + cda.Database + "`.`fournisseur`;");
            str.Append("CREATE TABLE  `" + cda.Database + "`.`fournisseur` (");
            str.Append("`idFournisseur` bigint(20) unsigned NOT NULL auto_increment,");
            str.Append("`ref` varchar(255) default NULL,");
            str.Append("`designation` varchar(255) default NULL,");
            str.Append("`nStat` varchar(255) default NULL,");
            str.Append("`adresse` varchar(255) default NULL,");
            str.Append("`telephone` varchar(255) default NULL,");
            str.Append("`email` varchar(255) default NULL,");
            str.Append("PRIMARY KEY  (`idFournisseur`)");
            str.Append(") ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;");

            str.Append("DROP TABLE IF EXISTS `" + cda.Database + "`.`hist_paie_entrees`;");
            str.Append("CREATE TABLE  `" + cda.Database + "`.`hist_paie_entrees` (");
            str.Append("`idHist_paie_entrees` bigint(20) unsigned NOT NULL auto_increment,");
            str.Append("`idHistorique_entrees` bigint(20) unsigned default NULL,");
            str.Append("`datePaiement` date default NULL,");
            str.Append("`montant` float default NULL,");
            str.Append("PRIMARY KEY  (`idHist_paie_entrees`)");
            str.Append(") ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;");

            str.Append("DROP TABLE IF EXISTS `" + cda.Database + "`.`hist_paie_sorties`;");
            str.Append("CREATE TABLE  `" + cda.Database + "`.`hist_paie_sorties` (");
            str.Append("`idHist_paie_sorties` bigint(20) unsigned NOT NULL auto_increment,");
            str.Append("`idHistorique_sorties` bigint(20) unsigned default NULL,");
            str.Append("`datePaiement` date default NULL,");
            str.Append("`montant` float default NULL,");
            str.Append("PRIMARY KEY  USING BTREE (`idHist_paie_sorties`)");
            str.Append(") ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;");

            str.Append("DROP TABLE IF EXISTS `" + cda.Database + "`.`historique_entrees`;");
            str.Append("CREATE TABLE  `" + cda.Database + "`.`historique_entrees` (");
            str.Append("`idhistorique_Entrees` int(10) unsigned NOT NULL auto_increment,");
            str.Append("`dateCreation` date default NULL,");
            str.Append("`numero` varchar(255) default NULL,");
            str.Append("`idTiers` varchar(45) default NULL,");
            str.Append("`resteMontant` float default NULL,");
            str.Append("PRIMARY KEY  USING BTREE (`idhistorique_Entrees`)");
            str.Append(") ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;");

            str.Append("DROP TABLE IF EXISTS `" + cda.Database + "`.`historique_sorties`;");
            str.Append("CREATE TABLE  `" + cda.Database + "`.`historique_sorties` (");
            str.Append("`idhistorique_sorties` bigint(20) unsigned NOT NULL auto_increment,");
            str.Append("`dateCreation` date default NULL,");
            str.Append("`numero` varchar(255) default NULL,");
            str.Append("`idTiers` varchar(45) default NULL,");
            str.Append("`resteMontant` float default NULL,");
            str.Append("PRIMARY KEY  (`idhistorique_sorties`)");
            str.Append(") ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;");

            str.Append("DROP TABLE IF EXISTS `" + cda.Database + "`.`stock`;");
            str.Append("CREATE TABLE  `" + cda.Database + "`.`stock` (");
            str.Append("`idStock` bigint(20) unsigned NOT NULL auto_increment,");
            str.Append("`libelle` varchar(255) default NULL,");
            str.Append("`pu_achat` float default NULL,");
            str.Append("`pu` float default NULL,");
            str.Append("`qte` float default NULL,");
            str.Append("`unite` varchar(255) default NULL,");
            str.Append("PRIMARY KEY  (`idStock`)");
            str.Append(") ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;");

            str.Append("DROP TABLE IF EXISTS `" + cda.Database + "`.`tiers`;");
            str.Append("CREATE TABLE  `" + cda.Database + "`.`tiers` (");
            str.Append("`idTiers` bigint(20) unsigned NOT NULL auto_increment,");
            str.Append("`ref` varchar(255) default NULL,");
            str.Append("`nom` varchar(255) default NULL,");
            str.Append("`sexe` varchar(255) default NULL,");
            str.Append("`adresse` varchar(255) default NULL,");
            str.Append("`email` varchar(255) default NULL,");
            str.Append("`tel` varchar(255) default NULL,");
            str.Append("PRIMARY KEY  (`idTiers`)");
            str.Append(") ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;");

            str.Append("DROP TABLE IF EXISTS `" + cda.Database + "`.`unitestock`;");
            str.Append("CREATE TABLE  `" + cda.Database + "`.`unitestock` (");
            str.Append("`idUniteStock` bigint(20) unsigned NOT NULL auto_increment,");
            str.Append("`idStock` bigint(20) unsigned NOT NULL,");
            str.Append("`nom` varchar(255) default NULL,");
            str.Append("`facteur` varchar(255) default NULL,");
            str.Append("PRIMARY KEY  (`idUniteStock`)");
            str.Append(") ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;");

            str.Append("DROP TABLE IF EXISTS `" + cda.Database + "`.`profile`;");
            str.Append("CREATE TABLE  `" + cda.Database + "`.`profile` (");
            str.Append("`idProfile` int(10) unsigned NOT NULL auto_increment,");
            str.Append("`firstName` varchar(45) NOT NULL,");
            str.Append("`lastName` varchar(45) NOT NULL,");
            str.Append("`login` varchar(45) NOT NULL,");
            str.Append("`pwd` varchar(45) NOT NULL,");
            str.Append("`privilege` int(10) unsigned NOT NULL,");
            str.Append("PRIMARY KEY  (`idProfile`)");
            str.Append(") ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;");

            try
            {
                EntityLoader.Instance.ExecuteNonQuery(str.ToString());
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("La base de donn�es n'a pas pu �tre cr��e", "Echec", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                System.Console.WriteLine(ex.Message);
                return;
            }

            System.Windows.Forms.MessageBox.Show("La base de donn�es a �t� initialis�e", "Succes", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
        }
    }
}