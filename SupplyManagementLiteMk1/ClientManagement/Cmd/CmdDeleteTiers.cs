using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.ClientManagement.Cmd
{
    public class CmdDeleteTiers : ICommand
    {
        private BindingSource m_TiersBindingSource;
        private IObserver m_Observer;

        public CmdDeleteTiers(BindingSource tiersBindingSource, IObserver observer)
        {
            m_TiersBindingSource = tiersBindingSource;
            m_Observer = observer;
        }

        public void Doing()
        {
            Tiers tiers = (Tiers)m_TiersBindingSource.Current;
            if (tiers == null) return;

            DialogResult res = MessageBox.Show("Etes vous sur de vouloir supprimer ?", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.No) return;
            try
            {
                EntityLoader.Instance.Delete(tiers);
                m_Observer.Notify(null);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
