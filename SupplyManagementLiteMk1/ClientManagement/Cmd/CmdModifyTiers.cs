using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using SupplyManagementLiteMk1.TiersManagement;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.ClientManagement.Cmd
{
    public class CmdModifyTiers : ICommand
    {
        private BindingSource m_TiersBindingSource;
        private IObserver m_Observer;

        public CmdModifyTiers(BindingSource tiersBindingSource, IObserver observer)
        {
            m_TiersBindingSource = tiersBindingSource;
            m_Observer = observer;
        }

        public void Doing()
        {
            Tiers tiers = (Tiers)m_TiersBindingSource.Current;
            if (tiers == null) return;
            FormEditTiers f = new FormEditTiers(tiers);
            if (f.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    EntityLoader.Instance.Update(tiers);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }

            m_Observer.Notify(null);
        }
    }
}
