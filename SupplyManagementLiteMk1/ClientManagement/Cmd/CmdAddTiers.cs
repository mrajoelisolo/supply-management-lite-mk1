using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using SupplyManagementLiteMk1.TiersManagement;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.ClientManagement.Cmd
{
    public class CmdAddTiers : ICommand
    {
        private IObserver m_Observer;
        private Tiers m_Tiers;
        private TextBox m_TxtName;
        private TextBox m_TxtRef;

        public CmdAddTiers(IObserver observer, TextBox txtName, TextBox txtRef)
        {
            m_Observer = observer;
            m_TxtRef = txtRef;
            m_TxtName = txtName;
        }

        public void Doing()
        {
            m_Tiers = new Tiers();
            FormEditTiers f = new FormEditTiers(m_Tiers);

            if (f.ShowDialog() == DialogResult.Cancel) return;

            try
            {
                //Vérification d'intégrité, added since 12/04/2011
                String whereClause = Tiers.REF_COL + " LIKE '" + m_Tiers.Ref + "' AND " + Tiers.NOM_COL + " LIKE '" + m_Tiers.Nom + "'";
                List<object> res = EntityLoader.Instance.LoadData(typeof(Tiers), whereClause);
                if (res.Count > 0)
                {
                    MessageBox.Show("Efa misy ao anaty lisitra ilay voasoratra", "Tsy mety", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                EntityLoader.Instance.Save(m_Tiers);

                m_TxtRef.Text = m_Tiers.Ref;
                m_TxtName.Text = m_Tiers.Nom;

                m_Observer.Notify("");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
