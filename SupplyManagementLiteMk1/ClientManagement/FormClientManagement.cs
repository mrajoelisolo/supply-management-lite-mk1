using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.ClientManagement.Cmd;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.ClientManagement
{
    public partial class FormClientManagement : Form, IObserver
    {        
        public FormClientManagement()
        {
            InitializeComponent();

            Notify(null);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdAddTiers(this, txtName, txtRef);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdModifyTiers(tiersBindingSource, this);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdDeleteTiers(tiersBindingSource, this);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

        }

        public Tiers SelectedTiers
        {
            get
            {
                return (Tiers)tiersBindingSource.Current;
            }
        }        

        private void txtRef_KeyUp(object sender, KeyEventArgs e)
        {
            searchRef();
        }

        private void txtName_KeyUp(object sender, KeyEventArgs e)
        {
            searchName();
        }

        private void searchName()
        {
            String whereClause = "";

            if (!String.IsNullOrEmpty(txtName.Text))
                whereClause += Tiers.REF_COL + " LIKE '" + txtRef.Text + "%' AND " + Tiers.NOM_COL + " LIKE '" + txtName.Text + "%'";

            tiersBindingSource.DataSource = EntityLoader.Instance.LoadData(typeof(Tiers), whereClause, null, Tiers.REF_COL);
        }

        private void searchRef()
        {
            String whereClause = "";

            if (!String.IsNullOrEmpty(txtRef.Text))
                whereClause += Tiers.REF_COL + " LIKE '" + txtRef.Text + "%' AND " + Tiers.NOM_COL + " LIKE '" + txtName.Text + "%'";

            tiersBindingSource.DataSource = EntityLoader.Instance.LoadData(typeof(Tiers), whereClause, null, Tiers.REF_COL);
        }

        public void Notify(String o)
        {
            if (o != null)
            {
                searchName();
                searchRef();
            }
            else
                tiersBindingSource.DataSource = EntityLoader.Instance.LoadData(typeof(Tiers));
        }
    }
}