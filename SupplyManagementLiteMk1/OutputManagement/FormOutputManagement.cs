using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.OutputManagement.Cmd;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.OutputManagement
{
    public partial class FormOutputManagement : Form, IObserver
    {
        private Boolean m_Dirty = false;

        public FormOutputManagement()
        {
            InitializeComponent();
            outputBindingSource.DataSource = new List<OutputGoods>();
            storyOutputBindingSource.DataSource = new StoryOutputGoods();
        }        

        private void SearchInput()
        {            
            if (!String.IsNullOrEmpty(txtLibelle.Text))
            {
                String whereClause = "";
                whereClause += Stock.LIBELLE_COL + " LIKE '" + txtLibelle.Text + "%'";
                stockBindingSource.DataSource = EntityLoader.Instance.LoadData(typeof(Stock), whereClause, null, Stock.LIBELLE_COL);
            }
            else 
                ClearResult();
        }

        private void ClearResult()
        {
            stockBindingSource.Clear();
            txtLibelle.Text = "";
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchInput();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdRemoveStock(stockBindingSource, outputBindingSource, txtLibelle.Text, this);
            Invoker.Instance.Invoke(cmd);
        }       

        private void btnCancel_Click(object sender, EventArgs e)
        {
            //DialogResult = DialogResult.Cancel;            

            Close();
        }

        private void btnValidate_Click(object sender, EventArgs e)
        {
            String str = txtTiers.Text;
            if (String.IsNullOrEmpty(str))
            {
                MessageBox.Show("Tsy voasoratra ny anaran'ny client", "Tsy mety", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            m_Dirty = false;

            ICommand cmd = new CmdValidateOutput(storyOutputBindingSource, outputBindingSource, dateTimePicker1.Value, this);
            Invoker.Instance.Invoke(cmd);            
        }        

        private void txtLibelle_KeyUp(object sender, KeyEventArgs e)
        {
            SearchInput();
        }        

        private void btnRemoveItem_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdRemoveItemOutput(outputBindingSource, this);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnTiers_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdBrowseTiers(storyOutputBindingSource, txtTiers);
            Invoker.Instance.Invoke(cmd);
        }

        private float GetMontantTotal()
        {
            float sum = 0f;

            List<OutputGoods> objs = (List<OutputGoods>)outputBindingSource.DataSource;
            foreach (OutputGoods oGoods in objs)
                sum += oGoods.Montant;

            return sum;
        }

        private void FormOutputManagement_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (m_Dirty)
            {
                DialogResult res = MessageBox.Show("Hidina ve ?", "Entana mivoaka", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (res == DialogResult.No)
                    e.Cancel = true;
            }
        }

        public void Notify(String value)
        {
            ClearResult();
            txtMontantTotal.Text = "" + GetMontantTotal();

            if(value != null)
                if(value.Equals("dirty"))
                    m_Dirty = true;
        }
    }
}