using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using SupplyManagementLiteMk1.StoryManagement.Cmd;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.OutputManagement.Cmd
{
    public class CmdValidateOutput : ICommand
    {
        private BindingSource m_StoryOutputBindingSource;
        private BindingSource m_OutputBindingSource;
        private DateTime m_InputDate;
        private Form m_Parent;

        public CmdValidateOutput(BindingSource storyInputBindingSource, BindingSource outputBindingSource, DateTime inputDate, Form parent)
        {
            m_StoryOutputBindingSource = storyInputBindingSource;
            m_OutputBindingSource = outputBindingSource;
            m_InputDate = inputDate;
            m_Parent = parent;
        }

        public void Doing()
        {
            List<OutputGoods> outputs = (List<OutputGoods>)m_OutputBindingSource.DataSource;
            if (outputs == null || outputs.Count == 0)
            {
                MessageBox.Show("Tsy misy entana ao anaty lisitra", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            DialogResult res = MessageBox.Show("Ekena ve ireo lisitra ireo ?", "Famoahana entana", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.No) return;

            StoryOutputGoods story = (StoryOutputGoods)m_StoryOutputBindingSource.Current;

            //Anontaniana ve aloha daholo sa ampahiny
            FormEditOutputAmount f = new FormEditOutputAmount(m_StoryOutputBindingSource, m_OutputBindingSource);
            if (f.ShowDialog() == DialogResult.Cancel) return;

            if (story == null) story = new StoryOutputGoods();
            story.DateCreation = m_InputDate;
            //story.MovementType = 1;                        
                        
            EntityLoader.Instance.Save(story);
            //Update n� Facture
            //String szFacture = "F" + story.Id;
            //story.Numero = "" + Math.Abs(szFacture.GetHashCode());
            story.Numero = "" + story.Id;
            EntityLoader.Instance.Update(story);

            foreach (OutputGoods output in outputs)
            {
                Stock stock = (Stock) EntityLoader.Instance.LoadData(typeof(Stock), output.IdStock);
                if(!output.IsRecent)
                    stock.Qte = stock.Qte - output.Qte * output.Factor; //Added factor since 11/04/11
                output.IdStoryOutputGoods = story.Id;

                EntityLoader.Instance.Save(output);
                EntityLoader.Instance.Update(stock);
            }                       

            //On enregistre les paiements partiels
            //S'il n'ya eu aucun paiement, alors on n'enregistre rien dans l'historique
            if (story.RemainingAmount > 0 && story.LastVersedAmount > 0)
            {
                OutputPay outputPay = new OutputPay();
                outputPay.Amount = story.LastVersedAmount;
                outputPay.DateOfPay = DateTime.Now;
                outputPay.IdStoryOutputGoods = story.Id;
                story.LastVersedAmount = 0f;

                EntityLoader.Instance.Save(outputPay);
            }

            //On sort la facture
            ICommand cmd = null;
            if (story.RemainingAmount > 0)
                cmd = new CmdFacturateOutputPayment(m_StoryOutputBindingSource);
            else if (story.RemainingAmount == 0)
                cmd = new CmdFacturateOutput(m_StoryOutputBindingSource);
            Invoker.Instance.Invoke(cmd);

            //m_Parent.DialogResult = DialogResult.OK;
            m_Parent.Close();
        }
    }
}
