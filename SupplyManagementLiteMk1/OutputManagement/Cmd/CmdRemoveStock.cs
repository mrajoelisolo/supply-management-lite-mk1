using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.StockManagement;
using SupplyManagementLiteMk1.InputManagement;
using SupplyManagementLiteMk1.OutputManagement.Cmd.Misc;

namespace SupplyManagementLiteMk1.OutputManagement.Cmd
{
    public class CmdRemoveStock : ICommand
    {
        private BindingSource m_StockBindingSource;
        private BindingSource m_OutputGoodsBindingSource;
        private String m_TxtLibelle;
        private IObserver m_Observer;

        public CmdRemoveStock(BindingSource stockBindingSource, BindingSource outputGoodsBindingSource, String txtLibelle, IObserver observer)
        {
            m_StockBindingSource = stockBindingSource;
            m_OutputGoodsBindingSource = outputGoodsBindingSource;
            m_TxtLibelle = txtLibelle;
            m_Observer = observer;
        }

        public void Doing()
        {            
            Stock stock = (Stock)m_StockBindingSource.Current;

            if (stock == null && String.IsNullOrEmpty(m_TxtLibelle)) return;

            //V�rifier d'abord si l'article n'�xiste pas d�ja dans le panier
            List<OutputGoods> goods = (List<OutputGoods>)m_OutputGoodsBindingSource.DataSource;
            foreach (OutputGoods outputGoods in goods)
            {
                if (stock == null) break; //Solution for E1
                if (outputGoods.IdStock == stock.Id) //There is an error when I try to add several new stocks : Resolved since 06/04/2011 E1
                {
                    DialogResult res = MessageBox.Show("Efa misy ao anaty lisitrin'ny entana avoaka io entana io. Alefa ihany ve ?", "", MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question);
                    if(res == DialogResult.No) return;
                }
            }
            //Added since 30/03/2011 : Gestion d'erreurs et d'intergit�

            bool isRecent = false;
            float inputQty = 0f;
            StockUnit stockUnit;
            if (stock == null)
            {               
                DialogResult res = MessageBox.Show("Tsy misy ao anaty lisitra ilay entana", "Entana avoaka", MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                isRecent = true;
                return;                
            } else {
                if (stock.Qte == 0)
                {
                    MessageBox.Show("Tsy misy ao amin'y stock intsony ilay entana", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                FormEditOutputStock f = new FormEditOutputStock(stock);
                if (f.ShowDialog() == DialogResult.Cancel) return;

                //!!! V�rification si l'on peut retirer tel quantit� de stock
                inputQty = f.Qte;
                stockUnit = f.SelectedUnit;

                if (!StockUtilities.Instance.CanGatherSufficientStock(stock, inputQty, stockUnit, m_OutputGoodsBindingSource))
                {
                    MessageBox.Show("Tsy ampy ny entana ao amin'ny 'stock'", "Tsy mety", MessageBoxButtons.OK, MessageBoxIcon.Error);                    
                    return;
                }
            }            
           
            OutputGoods outputStock = new OutputGoods();
            outputStock.Libelle = stock.Libelle;
            outputStock.Pu = stock.Pu;
            outputStock.Qte = inputQty;
            outputStock.IdStock = stock.Id;
            //outputStock.Unite = stock.Unite;
            outputStock.Unite = stockUnit.Name; //Since 10/04/2011  
            outputStock.Factor = stockUnit.Factor; //Since 10/04/2011  
            outputStock.IsRecent = isRecent;
            m_OutputGoodsBindingSource.Add(outputStock);
            m_Observer.Notify("dirty");
        }
    }
}