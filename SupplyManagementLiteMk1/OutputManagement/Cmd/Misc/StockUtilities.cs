using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.Datasource;
using System.Windows.Forms;

namespace SupplyManagementLiteMk1.OutputManagement.Cmd.Misc
{
    public class StockUtilities
    {
        //Singleon pattern. Added since 13/04/2011
        private static StockUtilities m_Instance = new StockUtilities();

        public StockUtilities() { }

        public static StockUtilities Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public Boolean CanGatherSufficientStock(Stock stock, float qty, StockUnit unit, BindingSource outputGoodsBindingSource)
        {
            float remainingQty = stock.Qte;

            List<OutputGoods> cart = (List<OutputGoods>) outputGoodsBindingSource.DataSource;
            foreach (OutputGoods outputGoods in cart)
            {
                if (outputGoods.IdStock == stock.Id)
                    remainingQty -= outputGoods.Qte * outputGoods.Factor;
            }

            remainingQty -= qty * unit.Factor;

            return (remainingQty >= 0);
        }
    }
}
