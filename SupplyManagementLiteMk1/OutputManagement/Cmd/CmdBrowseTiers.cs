using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.Datasource;
using SupplyManagementLiteMk1.ClientManagement;
using System.Windows.Forms;

namespace SupplyManagementLiteMk1.OutputManagement.Cmd
{
    public class CmdBrowseTiers : ICommand
    {
        private BindingSource m_IoStoryBindingSource;
        private TextBox m_TxtTiers;

        public CmdBrowseTiers(BindingSource storyBindingSource, TextBox txtTiers)
        {
            m_IoStoryBindingSource = storyBindingSource;
            m_TxtTiers = txtTiers;
        }

        public void Doing()
        {
            StoryOutputGoods story = (StoryOutputGoods)m_IoStoryBindingSource.Current;

            FormClientManagement f = new FormClientManagement();
            if (f.ShowDialog() == DialogResult.OK)
            {
                Tiers tiers = f.SelectedTiers;
                story.IdTiers = tiers.Id;

                m_TxtTiers.Text = tiers.FullName;
                //EntityLoader.Instance.Update(story);
            }
        }
    }
}