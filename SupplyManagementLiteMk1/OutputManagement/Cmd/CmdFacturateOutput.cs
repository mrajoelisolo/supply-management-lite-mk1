using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using TFramework.Reporting;
using System.IO;
using TFramework.Utilities;
using SupplyManagementLiteMk1.MainApp.Modules;
using System.Reflection;

namespace SupplyManagementLiteMk1.OutputManagement.Cmd
{
    public class CmdFacturateOutput : ICommand
    {
        private BindingSource m_StoryOutputGoodsBindingSource;

        public CmdFacturateOutput(BindingSource storyOutputGoodsBindingSource)
        {
            m_StoryOutputGoodsBindingSource = storyOutputGoodsBindingSource;
        }

        public void Doing()
        {
            StoryOutputGoods story = (StoryOutputGoods)m_StoryOutputGoodsBindingSource.Current;
            if (story == null) return;

            if (story.RemainingAmount > 0)
            {
                MessageBox.Show("Mbola misy vola tsy voaloa, tsy afaka avoaka amin'ny faktiora tsotra ?", "Tsy mety", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            DialogResult res = MessageBox.Show("Avoaka ve ny faktiora ?", "Entana mivoaka", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.No) return;

            String src = SystemManager.Instance.GetSystemDirectory() + "\\ModeleFacture.rep";

            SaveFileDialog sf = new SaveFileDialog();
            sf.Filter = SystemManager.Instance.ReportFilter;

            sf.FileName = "Facture_" + story.Numero + ".doc";
            if (sf.ShowDialog() == DialogResult.Cancel) return;
            
            String dest = sf.FileName;

            try
            {                
                File.Copy(src, dest);
            }
            catch (Exception ex)
            {
                File.Delete(dest);
                File.Copy(src, dest);
                System.Console.WriteLine(ex.Message);
            }

            MSWordUtilities.Instance.Initialize(dest);

            MSWordUtilities.Instance.ReplaceField("NomClient", story.SzTiersFullName);
            MSWordUtilities.Instance.ReplaceField("NoFacture", story.Numero);
            MSWordUtilities.Instance.ReplaceField("CreationDate", story.szDateCreation);
            MSWordUtilities.Instance.ReplaceField("EditionDate", story.SzDateEdition);

            List<List<String>> rows = new List<List<String>>();
            foreach (OutputGoods goods in story.Goods)
            {
                List<String> row = new List<String>();
                
                row.Add(goods.Libelle);
                row.Add(DigitToLetters.Instance.ToNbAmount(goods.Pu, ""));
                row.Add("" + goods.Qte);
                row.Add(goods.Unite);
                row.Add(DigitToLetters.Instance.ToNbAmount(goods.Montant, ""));

                rows.Add(row);
            }
            MSWordUtilities.Instance.FillTable("TableauFacture", rows);

            MSWordUtilities.Instance.ReplaceField("TotalMontant", DigitToLetters.Instance.ToNbAmount(story.MontantTotal, ""));

            MSWordUtilities.Instance.Visible = true;
            MSWordUtilities.Instance.SaveDocument();
        }
    }
}
