using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;

namespace SupplyManagementLiteMk1.OutputManagement
{
    public partial class FormEditOutputAmount : Form
    {
        public FormEditOutputAmount(BindingSource storyOutputBindingSource, BindingSource outputGoodsBindingSource)
        {
            InitializeComponent();

            CenterToParent();

            this.storyOutputGoodsBindingSource = storyOutputBindingSource;
            this.outputGoodsBindingSource = outputGoodsBindingSource;

            txtTotalAmount.Text = "" + GetTotalAmount();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            float amount = 0f;

            if (radioButton1.Checked)
                amount = GetTotalAmount();
            else if (radioButton2.Checked)
            {
                amount = GetPartialAmount();
                if ((amount < 0) || (amount > GetTotalAmount()))
                {
                    MessageBox.Show("Tsy mety ny voasoratra", "Tsy mety", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            SetVersedAmount(amount);


            DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            txtPartialAmount.Enabled = radioButton2.Checked;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            txtTotalAmount.Enabled = radioButton1.Checked;
        }

        private float GetPartialAmount()
        {
            float res = 0f;

            try
            {
                res = float.Parse(txtPartialAmount.Text);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }

            return res;
        }

        private float GetTotalAmount()
        {
            float sum = 0f;

            List<OutputGoods> objs = (List<OutputGoods>)outputGoodsBindingSource.DataSource;
            if (objs != null)
                foreach (OutputGoods o in objs)
                    sum += o.Montant;

            return sum;
        }

        private void SetVersedAmount(float amount)
        {
            StoryOutputGoods story = (StoryOutputGoods)storyOutputGoodsBindingSource.DataSource;
            if (story == null) return;

            story.RemainingAmount = GetTotalAmount() - amount;
            story.LastVersedAmount = amount;
        }        
    }
}