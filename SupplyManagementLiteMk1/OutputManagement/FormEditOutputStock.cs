using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using SupplyManagementLiteMk1.Misc;

namespace SupplyManagementLiteMk1.InputManagement
{
    public partial class FormEditOutputStock : Form
    {
        private Stock m_Stock;
        private StockUnit m_SelectedUnit;

        public FormEditOutputStock(Stock stock)
        {
            InitializeComponent();

            CenterToParent();

            m_Stock = stock;

            StockUnit stockUnit = new StockUnit(stock.Unite, 1f);
            m_SelectedUnit = stockUnit;

            txtLibelle.Text = stock.Libelle;
            lblUnit.Text = "[" + stock.Unite + "]";
        }

        public float Qte
        {
            get
            {
                try
                {
                    float res = float.Parse(txtQte.Text);
                    return res;
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                    return 0f;
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            //if (Qte * SelectedUnit.Factor > m_Stock.Qte) //Added Selected Unit since 11/04/2011
            //{
            //    MessageBox.Show("Tsy ampy ny entana ao amin'ny 'stock'", "Tsy mety", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}
            //else 
            if (Qte <= 0)
                return;

            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void txtQte_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnOk.PerformClick();
            else if (e.KeyCode == Keys.Escape)
                btnCancel.PerformClick();
        }

        private void btnBrowseUnit_Click(object sender, EventArgs e)
        {
            FormBrowseUnits f = new FormBrowseUnits(m_Stock, false);
            if (f.ShowDialog() == DialogResult.OK)
            {
                lblUnit.Text = "[" + f.SelectedUnit.Name + "]";
                m_SelectedUnit = f.SelectedUnit;
            }
        }

        public StockUnit SelectedUnit
        {
            get
            {
                return m_SelectedUnit;
            }
        }
    }    
}