using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.Datasource;
using SupplyManagementLiteMk1.StockManagement;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.SupplierManagement.Cmd
{
    public class CmdModifySupplier : ICommand
    {
        private BindingSource m_SupplierBindingSource;
        private IObserver m_Observer;

        public CmdModifySupplier(BindingSource supplierBindingSource, IObserver observer)
        {
            m_SupplierBindingSource = supplierBindingSource;
            m_Observer = observer;
        }

        public void Doing()
        {
            Supplier supplier = (Supplier)m_SupplierBindingSource.Current;
            if (supplier == null) return;
            FormEditSupplier f = new FormEditSupplier(supplier);
            if (f.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    EntityLoader.Instance.Update(supplier);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }

            m_Observer.Notify(null);
        }
    }
}
