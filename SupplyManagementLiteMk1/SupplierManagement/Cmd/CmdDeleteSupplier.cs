using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.SupplierManagement.Cmd
{
    public class CmdDeleteSupplier : ICommand
    {
        private BindingSource m_SupplierBindingSource;
        private IObserver m_Observer;

        public CmdDeleteSupplier(BindingSource supplierBindingSource, IObserver observer)
        {
            m_SupplierBindingSource = supplierBindingSource;
            m_Observer = observer;
        }

        public void Doing()
        {
            Supplier supplier = (Supplier)m_SupplierBindingSource.Current;
            if (supplier == null) return;

            DialogResult res = MessageBox.Show("Etes vous sur de vouloir supprimer ?", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.No) return;
            try
            {
                EntityLoader.Instance.Delete(supplier);
                m_Observer.Notify(null);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
