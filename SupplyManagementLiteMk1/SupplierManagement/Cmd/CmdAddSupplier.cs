using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.SupplierManagement.Cmd
{
    public class CmdAddSupplier : ICommand
    {
        private IObserver m_Observer;
        private TextBox m_TxtRef;
        private TextBox m_TxtDesignation;

        public CmdAddSupplier(IObserver observer, TextBox txtRef, TextBox txtDesignation)
        {
            m_Observer = observer;
            m_TxtRef = txtRef;
            m_TxtDesignation = txtDesignation;
        }
        
        public void Doing()
        {
            Supplier supplier = new Supplier();
            FormEditSupplier f = new FormEditSupplier(supplier);
            
            if (f.ShowDialog() == DialogResult.Cancel) return;

            try
            {
                //Vérification d'intégrité, added since 12/04/2011
                String whereClause = Supplier.REF_COL + " LIKE '" + supplier.Ref + "'";
                List<object> res = EntityLoader.Instance.LoadData(typeof(Supplier), whereClause);
                if (res.Count > 0)
                {
                    MessageBox.Show("Efa misy ao anaty lisitra ilay voasoratra", "Tsy mety", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                EntityLoader.Instance.Save(supplier);

                m_TxtRef.Text = supplier.Ref;
                m_TxtDesignation.Text = supplier.Designation;

                m_Observer.Notify("");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
