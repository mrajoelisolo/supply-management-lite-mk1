using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.ClientManagement.Cmd;
using SupplyManagementLiteMk1.SupplierManagement.Cmd;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.SupplierManagement
{
    public partial class FormSupplierManagement : Form, IObserver
    {
        public FormSupplierManagement()
        {
            InitializeComponent();

            Notify(null);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdAddSupplier(this, txtRef, txtDesignation);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdModifySupplier(supplierBindingSource, this);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdDeleteSupplier(supplierBindingSource, this);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

        }

        public Supplier SelectedSupplier
        {
            get
            {
                return (Supplier)supplierBindingSource.Current;
            }
        }        

        private void txtRef_KeyUp(object sender, KeyEventArgs e)
        {
            searchRef();
        }

        private void txtDesignation_KeyUp(object sender, KeyEventArgs e)
        {
            searchDesignation();
        }

        private void searchRef()
        {
            String whereClause = "";

            if (!String.IsNullOrEmpty(txtRef.Text))
                whereClause += Supplier.REF_COL + " LIKE '" + txtRef.Text + "%' AND " + Supplier.DESIGNATION + " LIKE '" + txtDesignation.Text + "%'";

            supplierBindingSource.DataSource = EntityLoader.Instance.LoadData(typeof(Supplier), whereClause, null, Supplier.REF_COL);
        }

        private void searchDesignation()
        {
            String whereClause = "";

            if (!String.IsNullOrEmpty(txtDesignation.Text))
                whereClause += Supplier.REF_COL + " LIKE '" + txtRef.Text + "%' AND " + Supplier.DESIGNATION + " LIKE '" + txtDesignation.Text + "%'";

            supplierBindingSource.DataSource = EntityLoader.Instance.LoadData(typeof(Supplier), whereClause, null, Supplier.REF_COL);
        }

        public void Notify(String o)
        {
            if (o != null)
            {
                searchRef();
                searchDesignation();
            }
            else
                supplierBindingSource.DataSource = EntityLoader.Instance.LoadData(typeof(Supplier));
        }
    }
}