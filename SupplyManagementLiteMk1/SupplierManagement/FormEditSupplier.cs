using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;

namespace SupplyManagementLiteMk1.SupplierManagement
{
    public partial class FormEditSupplier : Form
    {
        public FormEditSupplier(Supplier supplier)
        {
            InitializeComponent();

            supplierBindingSource.DataSource = supplier;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}