﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.ProfileManagement.Cmd
{
    public class CmdAddProfile : ICommand
    {
        private IObserver m_Observer;

        public CmdAddProfile(IObserver observer)
        {
            m_Observer = observer;
        }

        public void Doing()
        {
            Profile profile = new Profile();

            FormEditProfile f = new FormEditProfile(profile);
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                EntityLoader.Instance.Save(profile);
                m_Observer.Notify("");
            }
        }
    }
}
