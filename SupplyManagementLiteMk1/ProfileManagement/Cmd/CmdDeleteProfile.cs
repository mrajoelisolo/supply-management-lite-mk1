using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.ProfileManagement.Cmd
{
    public class CmdDeleteProfile : ICommand
    {
        private IObserver m_Observer;
        private BindingSource m_ProfileBindingSource;

        public CmdDeleteProfile(BindingSource profileBindingSource, IObserver observer)
        {
            m_Observer = observer;
            m_ProfileBindingSource = profileBindingSource;
        }

        public void Doing()
        {
            Profile profile = (Profile)m_ProfileBindingSource.Current;
            if (profile == null) return;

            DialogResult res = MessageBox.Show("Etes vous sur de vouloir supprimer ?", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.No) return;
            try
            {
                EntityLoader.Instance.Delete(profile);

                m_Observer.Notify(null);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
