﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ORMFramework.AppUtil;
using ORMFramework.DbUtil.Entity;
using SupplyManagementLiteMk1.Datasource;
using System.Windows.Forms;

namespace SupplyManagementLiteMk1.ProfileManagement.Cmd
{    
    public class CmdModifyProfile : ICommand
    {
        private IObserver m_Observer;
        private Profile m_Profile;

        public CmdModifyProfile(IObserver observer, BindingSource bindingSource)
        {
            m_Observer = observer;
            m_Profile = (Profile)bindingSource.Current;
        }

        public void Doing()
        {
            if (m_Profile == null) return;

            FormEditProfile f = new FormEditProfile(m_Profile);
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                EntityLoader.Instance.Update(m_Profile);                
            }

            m_Observer.Notify("");
        }
    }
}
