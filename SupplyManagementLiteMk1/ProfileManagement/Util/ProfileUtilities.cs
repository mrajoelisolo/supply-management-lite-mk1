﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ORMFramework.DbUtil.Entity;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.DbUtil.Entity.Query;

namespace SupplyManagementLiteMk1.ProfileManagement.Util
{
    public class ProfileUtilities
    {
        private static ProfileUtilities m_Instance = new ProfileUtilities();

        private ProfileUtilities() { }

        public static ProfileUtilities Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public EnumPrivilege Authentify(String login, String pwd)
        {
            EnumPrivilege res = EnumPrivilege.ABORTED;

            List<QueryParameter> param = new List<QueryParameter>();
            param.Add(new QueryParameter("login", login));
            param.Add(new QueryParameter("pwd", pwd));
            
            String szWhere = Profile.LOGIN_COL + "=@login AND " + Profile.PWD_COL + "=@pwd";

            List<object> resultats = EntityLoader.Instance.LoadData(typeof(Profile), szWhere, param);
            if (resultats.Count > 0)
            {
                Profile profile = (Profile)resultats[0];

                if (profile.Privilege == 0)
                    res = EnumPrivilege.ADMIN;
                else if (profile.Privilege == 1)
                    res = EnumPrivilege.MAGASINIER;
                else if (profile.Privilege == 2)
                    res = EnumPrivilege.GERANT;
            }

            return res;
        }

        public enum EnumPrivilege
        {
            ADMIN, MAGASINIER, GERANT, ABORTED
        }
    }
}
