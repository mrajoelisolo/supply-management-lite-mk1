﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ORMFramework.DbUtil.Entity;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.ProfileManagement.Cmd;

namespace SupplyManagementLiteMk1.ProfileManagement
{
    public partial class FormProfileManagement : Form, IObserver
    {
        public FormProfileManagement()
        {
            InitializeComponent();
            CenterToParent();

            bindingSourceProfile.DataSource = EntityLoader.Instance.LoadData(typeof(Profile));
        }

        public void Notify(string msg)
        {
            bindingSourceProfile.DataSource = EntityLoader.Instance.LoadData(typeof(Profile));
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdAddProfile(this);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdModifyProfile(this, bindingSourceProfile);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdDeleteProfile(bindingSourceProfile, this);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
