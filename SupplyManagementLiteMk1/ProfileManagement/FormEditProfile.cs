﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;

namespace SupplyManagementLiteMk1.ProfileManagement
{
    public partial class FormEditProfile : Form
    {
        public FormEditProfile(Profile profile)
        {
            InitializeComponent();

            bindingSourceProfile.DataSource = profile;

            if (profile.Privilege == 0)
                rdBtnAdmin.Checked = true;
            else if (profile.Privilege == 1)
                rdBtnMagasinier.Checked = true;
            else if(profile.Privilege == 2)
                rdBtnGerant.Checked = true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Profile profile = (Profile) bindingSourceProfile.DataSource;

            if (rdBtnAdmin.Checked)
                profile.Privilege = 0;
            else if (rdBtnMagasinier.Checked)
                profile.Privilege = 1;
            else if (rdBtnGerant.Checked)
                profile.Privilege = 2;

            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
