using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;

namespace SupplyManagementLiteMk1.StockManagement
{
    public partial class FormEditUnitStock : Form
    {
        StockUnit m_StockUnit;

        public FormEditUnitStock(StockUnit stockUnit)
        {
            InitializeComponent();

            CenterToParent();

            m_StockUnit = stockUnit;
            stockUnitBindingSource.DataSource = stockUnit;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (m_StockUnit.Factor == 0)
            {
                MessageBox.Show("Tsy mety aotra ny 'multiplicateur'", "Tsy mety", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}