using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.StockManagement.Cmd;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.StockManagement
{
    public partial class FormStockManagement : Form, IObserver
    {
        public FormStockManagement()
        {
            InitializeComponent();

            stockBindingSource.DataSource = EntityLoader.Instance.LoadData(typeof(Stock), "", null, Stock.LIBELLE_COL);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdAddStock(stockBindingSource, this);
            Invoker.Instance.Invoke(cmd);

            //MaquetteSaisieStock f = new MaquetteSaisieStock();
            //f.ShowDialog();
        }        

        private void btnModify_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdModifyStock(stockBindingSource, this);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdDeleteStock(stockBindingSource, this);
            Invoker.Instance.Invoke(cmd);
        }        

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchInput();
        }

        private void txtLibelle_KeyUp(object sender, KeyEventArgs e)
        {
            SearchInput();
        }

        private void SearchInput()
        {
            if (!String.IsNullOrEmpty(txtLibelle.Text))
            {
                String whereClause = "";
                whereClause += Stock.LIBELLE_COL + " LIKE '" + txtLibelle.Text + "%'";
                stockBindingSource.DataSource = EntityLoader.Instance.LoadData(typeof(Stock), whereClause, null, Stock.LIBELLE_COL);
            }
            else
                stockBindingSource.DataSource = EntityLoader.Instance.LoadData(typeof(Stock), "", null, Stock.LIBELLE_COL);
        }

        public void Notify(String value)
        {
            txtLibelle.Text = "";
            stockBindingSource.DataSource = EntityLoader.Instance.LoadData(typeof(Stock));
        }
    }
}