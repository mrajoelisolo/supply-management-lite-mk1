using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using SupplyManagementLiteMk1.StockManagement;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.StockManagement.Cmd
{
    public class CmdAddStock : ICommand
    {
        private IObserver m_Observer;
        private BindingSource m_StockBindingSource;

        public CmdAddStock(BindingSource stockBindingSource, IObserver observer)
        {
            m_Observer = observer;
            m_StockBindingSource = stockBindingSource;
        }

        public void Doing()
        {
            Stock stock = new Stock();
            FormEditStock f = new FormEditStock(stock);

            if (f.ShowDialog() == DialogResult.Cancel) return;
            try
            {
                String whereClause = Stock.LIBELLE_COL + " LIKE '" + stock.Libelle + "'";
                List<object> res = EntityLoader.Instance.LoadData(typeof(Stock), whereClause);
                if (res.Count > 0)
                {
                    MessageBox.Show("Efa misy ao anaty lisitrin'ny stock io entana io", "Tsy mety", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                EntityLoader.Instance.Save(stock);

                foreach (StockUnit stockUnit in stock.BufUnits)
                {
                    stockUnit.IdStock = stock.Id;
                    EntityLoader.Instance.Save(stockUnit);
                }

                m_Observer.Notify(null);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
