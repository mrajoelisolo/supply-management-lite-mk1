using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using SupplyManagementLiteMk1.StockManagement;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.StockManagement.Cmd
{
    public class CmdModifyStock : ICommand
    {
        private IObserver m_Observer;
        private BindingSource m_StockBindingSource;

        public CmdModifyStock(BindingSource stockBindingSource, IObserver observer)
        {
            m_Observer = observer;
            m_StockBindingSource = stockBindingSource;
        }

        public void Doing()
        {
            Stock stock = (Stock) m_StockBindingSource.Current;
            if (stock == null) return;
            FormEditStock f = new FormEditStock(stock);
            if (f.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    EntityLoader.Instance.Update(stock);

                    foreach (StockUnit stockUnit in stock.UnitsToBeDeleted)
                        EntityLoader.Instance.Delete(stockUnit);

                    foreach (StockUnit stockUnit in stock.BufUnits)
                    {
                        stockUnit.IdStock = stock.Id;
                        EntityLoader.Instance.Save(stockUnit);
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }

            m_Observer.Notify(null);
        }
    }
}
