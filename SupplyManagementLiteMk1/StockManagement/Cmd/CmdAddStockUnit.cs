using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;

namespace SupplyManagementLiteMk1.StockManagement.Cmd
{
    public class CmdAddStockUnit : ICommand
    {
        private BindingSource m_StockUnitBindingSource;
        private Stock m_Stock;

        public CmdAddStockUnit(Stock stock, BindingSource stockUnitBindingSource)
        {
            m_Stock = stock;
            m_StockUnitBindingSource = stockUnitBindingSource;
        }

        public void Doing()
        {
            StockUnit unitStock = new StockUnit();
            FormEditUnitStock f = new FormEditUnitStock(unitStock);
            if (f.ShowDialog() == DialogResult.OK)
            {
                m_Stock.BufUnits.Add(unitStock);
                //m_Stock.Units.Add(unitStock); //New since 11/04/2011
                m_StockUnitBindingSource.Add(unitStock);
            }
        }
    }
}
