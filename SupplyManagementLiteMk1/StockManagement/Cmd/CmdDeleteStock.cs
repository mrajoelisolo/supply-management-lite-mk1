using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.StockManagement.Cmd
{       
    public class CmdDeleteStock : ICommand
    {
        private IObserver m_Observer;
        private BindingSource m_StockBindingSource;

        public CmdDeleteStock(BindingSource stockBindingSource, IObserver observer)
        {
            m_Observer = observer;
            m_StockBindingSource = stockBindingSource;
        }

        public void Doing()
        {
            Stock stock = (Stock)m_StockBindingSource.Current;
            if (stock == null) return;

            DialogResult res = MessageBox.Show("Etes vous sur de vouloir supprimer ?", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.No) return;
            try
            {
                EntityLoader.Instance.Delete(stock);

                //Contrainte d'integrité : added since 12/04/2011
                foreach (StockUnit stockUnit in stock.Units)
                    EntityLoader.Instance.Delete(stockUnit);

                m_Observer.Notify(null);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
