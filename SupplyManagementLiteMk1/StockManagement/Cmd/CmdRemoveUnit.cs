using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;

namespace SupplyManagementLiteMk1.StockManagement.Cmd
{
    public class CmdRemoveUnit : ICommand
    {
        private Stock m_Stock;
        private BindingSource m_StockUnitBindingSource;

        public CmdRemoveUnit(Stock stock, BindingSource stockUnitBindingSource) 
        {
            m_Stock = stock;
            m_StockUnitBindingSource = stockUnitBindingSource;
        }

        public void Doing()
        {
            DialogResult res = MessageBox.Show("Esorina ve ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.No) return;

            StockUnit stockUnit = (StockUnit)m_StockUnitBindingSource.Current;
            m_StockUnitBindingSource.Remove(stockUnit);
            m_Stock.UnitsToBeDeleted.Add(stockUnit);
        }
    }
}
