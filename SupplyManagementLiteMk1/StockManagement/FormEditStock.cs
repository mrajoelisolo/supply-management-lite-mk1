using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using SupplyManagementLiteMk1.Misc;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.StockManagement.Cmd;

namespace SupplyManagementLiteMk1.StockManagement
{
    public partial class FormEditStock : Form
    {
        private Stock m_Stock;

        public FormEditStock(Stock stock)
        {
            InitializeComponent();

            CenterToParent();

            m_Stock = stock;
            stockBindingSource.DataSource = stock;
            unitsBindingSource.DataSource = stock.Units;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Stock stock = (Stock) stockBindingSource.DataSource;
            if (!DataChecker.Instance.Check(stock)) return;

            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnAddUnit_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdAddStockUnit(m_Stock, unitsBindingSource);
            Invoker.Instance.Invoke(cmd);
        }

        private void btnRemoveUnit_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdRemoveUnit(m_Stock, unitsBindingSource);
            Invoker.Instance.Invoke(cmd);
        }
    }
}