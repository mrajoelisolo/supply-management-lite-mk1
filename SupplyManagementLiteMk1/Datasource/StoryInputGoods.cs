using System;
using System.Collections.Generic;
using System.Text;
using SupplyManagementLiteMk1.Misc;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.Datasource
{
    public class StoryInputGoods
    {
        public const String TABLE_NAME = "HISTORIQUE_ENTREES";

        private int m_Id;
        public const String ID_COL = "IDHISTORIQUE_ENTREES";
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        private DateTime m_DateCreation;
        public const String DATECREATION_COL = "DATECREATION";
        public DateTime DateCreation
        {
            get { return m_DateCreation; }
            set { m_DateCreation = value; }
        }

        private String m_Numero;
        public const String NUMERO_COL = "NUMERO";
        public String Numero
        {
            get { return m_Numero; }
            set { m_Numero = value; }
        }

        private int m_IdTiers;
        public const String ID_TIERS_COL = "IDTIERS";
        public int IdTiers
        {
            get { return m_IdTiers; }
            set { m_IdTiers = value; }
        }

        private float m_RemainingAmount = 0F;
        public const String REMAINING_AMOUNT_COL = "RESTEMONTANT";
        public float RemainingAmount
        {
            get { return m_RemainingAmount; }
            set { m_RemainingAmount = value; }
        }   

        private List<InputGoods> m_Goods = new List<InputGoods>();
        public List<InputGoods> Goods {
            get
            {
                m_Goods.Clear();

                List<object> res = EntityLoader.Instance.LoadData(typeof(InputGoods), InputGoods.ID_STORYINPUTGOODS + "='" + Id + "'");
                foreach(object o in res)
                    m_Goods.Add((InputGoods) o);

                return m_Goods;
            }
        }

        public List<InputPay> m_InputPaiments = new List<InputPay>();
        public List<InputPay> InputPaiments
        {
            get
            {
                m_InputPaiments.Clear();

                List<object> res = EntityLoader.Instance.LoadData(typeof(InputPay), InputPay.IDSTORYINPUTGOODS_COL + "='" + Id + "'");
                foreach (object o in res)
                    m_InputPaiments.Add((InputPay)o);

                return m_InputPaiments;
            }
        }

        public float MontantTotal
        {
            get
            {
                float sum = 0;

                foreach (InputGoods output in Goods)
                    sum += output.Montant;

                return sum;
            }
        }

        public String szDateCreation
        {
            get
            {
                return DateParser.Instance.ParseDate(DateCreation);
            }
        }

        public String SzDateEdition
        {
            get
            {
                return DateParser.Instance.ParseDate(DateTime.Now);
            }
        }

        public Supplier ParentSupplier
        {
            get
            {
                Supplier res = null;

                res = (Supplier)EntityLoader.Instance.LoadData(typeof(Supplier), m_IdTiers);

                return res;
            }
        }

        public String SzSupplierFullName
        {
            get
            {
                String res = "";

                if (ParentSupplier != null) res = ParentSupplier.Ref;

                return res;
            }
        }

        private float m_LastVersedAmount;
        public float LastVersedAmount
        {
            get { return m_LastVersedAmount; }
            set { m_LastVersedAmount = value; }
        }

        private List<InputPay> m_Paiments = new List<InputPay>();
        public List<InputPay> Paiments
        {
            get
            {
                m_Paiments.Clear();

                List<object> res = EntityLoader.Instance.LoadData(typeof(InputPay), InputPay.IDSTORYINPUTGOODS_COL+ " = '" + m_Id + "'");
                foreach (object o in res)
                    m_Paiments.Add((InputPay)o);

                return m_Paiments;
            }
        }

        public float TotalPaiments
        {
            get
            {
                float res = 0f;

                foreach (InputPay pay in Paiments)
                    res += pay.Amount;

                return res;
            }
        }
    }
}
