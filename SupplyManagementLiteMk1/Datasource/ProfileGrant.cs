﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SupplyManagementLiteMk1.Datasource
{
    public class ProfileGrant
    {
        public const String TABLE_NAME = "PROFILEGRANT";

        private int m_Id;
        public const String ID_COL = "IDPROFILEGRANT";
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        private String m_Name;
        public const String NAME_COL = "NAME";
        public String Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
    }
}
