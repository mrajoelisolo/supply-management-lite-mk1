using System;
using System.Collections.Generic;
using System.Text;

namespace SupplyManagementLiteMk1.Datasource
{
    public class StockUnit
    {
        public const String TABLE_NAME = "UNITESTOCK";

        public StockUnit() {}

        public StockUnit(String name, float factor)
        {
            m_Name = name;
            m_Factor = factor;
        }

        private int m_Id;
        public const String ID_COL = "IDUNITESTOCK";
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        private int m_IdStock;
        public const String ID_STOCK_COL = "IDSTOCK";
        public int IdStock
        {
            get { return m_IdStock; }
            set { m_IdStock = value; }
        }
	

        private String m_Name;
        public const String NAME_COL = "NOM";
        public String Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        private float m_Factor;
        public const String FACTOR_COL = "FACTEUR";
        public float Factor
        {
            get { return m_Factor; }
            set { m_Factor = value; }
        }
	
    }
}
