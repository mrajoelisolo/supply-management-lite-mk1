using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.DbUtil.Entity;
using SupplyManagementLiteMk1.Datasource;

namespace SupplyManagementMk1Sharp.AppUtil
{
    public class SchemaBuilder
    {
        private static SchemaBuilder m_instance = new SchemaBuilder();

        private EntitySchema m_schema;

        private SchemaBuilder()
        {
            m_schema = Create();
        }

        public static SchemaBuilder Instance
        {
            get
            {
                return m_instance;
            }
        }

        public EntitySchema GetSchema()
        {
            return m_schema;
        }

        public EntitySchema Create()
        {
            EntitySchema res = new EntitySchema();

            res.SchemaName = "supply_mg_lite";

            //##Table STOCK##
            EntityInfo e = new EntityInfo();
            e.Package = "Datasource";
            e.EntityName = "Stock";
            e.TableName = Stock.TABLE_NAME;

            IFieldEntity f = new FieldPrimaryKey();
            f.SetFieldName("Id");
            f.SetFieldType(typeof(int));
            f.SetColName(Stock.IDSTOCK_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Libelle");
            f.SetFieldType(typeof(String));
            f.SetColName(Stock.LIBELLE_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Pu");
            f.SetFieldType(typeof(float));
            f.SetColName(Stock.PU_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Qte");
            f.SetFieldType(typeof(float));
            f.SetColName(Stock.QTE_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Unite");
            f.SetFieldType(typeof(String));
            f.SetColName(Stock.UNITE_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("PuAchat");
            f.SetFieldType(typeof(float));
            f.SetColName(Stock.PUACHAT_COL);
            e.Fields.Add(f);

            res.EntitySet.Add(e);
            //##Fin Table STOCK##
           
            //##TIERS##
            e = new EntityInfo();
            e.Package = "Datasource";
            e.EntityName = "Tiers";
            e.TableName = Tiers.TABLE_NAME;

            f = new FieldPrimaryKey();
            f.SetFieldName("Id");
            f.SetFieldType(typeof(int));
            f.SetColName(Tiers.ID_COL);
            e.Fields.Add(f);            

            f = new FieldEntity();
            f.SetFieldName("Ref");
            f.SetFieldType(typeof(String));
            f.SetColName(Tiers.REF_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Nom");
            f.SetFieldType(typeof(String));
            f.SetColName(Tiers.NOM_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Sexe");
            f.SetFieldType(typeof(String));
            f.SetColName(Tiers.SEXE_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Adresse");
            f.SetFieldType(typeof(String));
            f.SetColName(Tiers.ADRESSE_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("EMail");
            f.SetFieldType(typeof(String));
            f.SetColName(Tiers.EMAIL_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Tel");
            f.SetFieldType(typeof(String));
            f.SetColName(Tiers.TEL_COL);
            e.Fields.Add(f);

            res.EntitySet.Add(e);
            //##End TIERS##

            //##STORYINPUTGOODS##
            e = new EntityInfo();
            e.Package = "Datasource";
            e.EntityName = "StoryInputGoods";
            e.TableName = StoryInputGoods.TABLE_NAME;

            f = new FieldPrimaryKey();
            f.SetFieldName("Id");
            f.SetFieldType(typeof(int));
            f.SetColName(StoryInputGoods.ID_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("DateCreation");
            f.SetFieldType(typeof(DateTime));
            f.SetColName(StoryInputGoods.DATECREATION_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Numero");
            f.SetFieldType(typeof(String));
            f.SetColName(StoryInputGoods.NUMERO_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("RemainingAmount");
            f.SetFieldType(typeof(float));
            f.SetColName(StoryInputGoods.REMAINING_AMOUNT_COL);
            e.Fields.Add(f);

            f = new FieldForeignKey();
            f.SetFieldName("IdTiers");
            f.SetFieldType(typeof(int));
            f.SetColName(StoryInputGoods.ID_TIERS_COL);
            e.Fields.Add(f);

            res.EntitySet.Add(e);
            //##END STORYINPUTGOODS##

            //##INPUTGOODS##
            e = new EntityInfo();
            e.Package = "Datasource";
            e.EntityName = "InputGoods";
            e.TableName = InputGoods.TABLE_NAME;

            f = new FieldPrimaryKey();
            f.SetFieldName("Id");
            f.SetFieldType(typeof(int));
            f.SetColName(InputGoods.ID_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Libelle");
            f.SetFieldType(typeof(String));
            f.SetColName(InputGoods.LIBELLE_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Pu");
            f.SetFieldType(typeof(float));
            f.SetColName(InputGoods.PU_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Qte");
            f.SetFieldType(typeof(float));
            f.SetColName(InputGoods.QTE_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Unite");
            f.SetFieldType(typeof(String));
            f.SetColName(InputGoods.UNITE_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Factor");
            f.SetFieldType(typeof(float));
            f.SetColName(InputGoods.FACTOR_COL);
            e.Fields.Add(f);

            f = new FieldForeignKey();
            f.SetFieldName("IdStoryInputGoods");
            f.SetFieldType(typeof(int));
            f.SetColName(InputGoods.ID_STORYINPUTGOODS);
            e.Fields.Add(f);

            f = new FieldForeignKey();
            f.SetFieldName("IdStock");
            f.SetFieldType(typeof(int));
            f.SetColName(InputGoods.ID_STOCK_COL);
            e.Fields.Add(f);

            res.EntitySet.Add(e);
            //##END INPUTGOODS##

            //##STORYOUTPUTGOODS##
            e = new EntityInfo();
            e.Package = "Datasource";
            e.EntityName = "StoryOutputGoods";
            e.TableName = StoryOutputGoods.TABLE_NAME;

            f = new FieldPrimaryKey();
            f.SetFieldName("Id");
            f.SetFieldType(typeof(int));
            f.SetColName(StoryOutputGoods.ID_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("DateCreation");
            f.SetFieldType(typeof(DateTime));
            f.SetColName(StoryOutputGoods.DATECREATION_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Numero");
            f.SetFieldType(typeof(String));
            f.SetColName(StoryOutputGoods.NUMERO_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("RemainingAmount");
            f.SetFieldType(typeof(float));
            f.SetColName(StoryOutputGoods.REMAINING_AMOUNT_COL);
            e.Fields.Add(f);

            f = new FieldForeignKey();
            f.SetFieldName("IdTiers");
            f.SetFieldType(typeof(int));
            f.SetColName(StoryOutputGoods.ID_TIERS_COL);
            e.Fields.Add(f);

            res.EntitySet.Add(e);
            //##END STORYOUTPUTGOODS##

            //##OUTPUTGOODS##
            e = new EntityInfo();
            e.Package = "Datasource";
            e.EntityName = "OutputGoods";
            e.TableName = OutputGoods.TABLE_NAME;

            f = new FieldPrimaryKey();
            f.SetFieldName("Id");
            f.SetFieldType(typeof(int));
            f.SetColName(OutputGoods.ID_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Libelle");
            f.SetFieldType(typeof(String));
            f.SetColName(OutputGoods.LIBELLE_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Pu");
            f.SetFieldType(typeof(float));
            f.SetColName(OutputGoods.PU_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Qte");
            f.SetFieldType(typeof(float));
            f.SetColName(OutputGoods.QTE_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Unite");
            f.SetFieldType(typeof(String));
            f.SetColName(OutputGoods.UNITE_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Factor");
            f.SetFieldType(typeof(float));
            f.SetColName(OutputGoods.FACTOR_COl);
            e.Fields.Add(f);

            f = new FieldForeignKey();
            f.SetFieldName("IdStoryOutputGoods");
            f.SetFieldType(typeof(int));
            f.SetColName(OutputGoods.ID_STORYOUTPUTGOODS);
            e.Fields.Add(f);

            f = new FieldForeignKey();
            f.SetFieldName("IdStock");
            f.SetFieldType(typeof(int));
            f.SetColName(OutputGoods.ID_STOCK_COL);
            e.Fields.Add(f);

            res.EntitySet.Add(e);
            //##END OUTPUTGOODS##

            //##SUPPLIER##
            e = new EntityInfo();
            e.Package = "Datasource";
            e.EntityName = "Supplier";
            e.TableName = Supplier.TABLE_NAME;

            f = new FieldPrimaryKey();
            f.SetFieldName("Id");
            f.SetFieldType(typeof(int));
            f.SetColName(Supplier.ID_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Adresse");
            f.SetFieldType(typeof(String));
            f.SetColName(Supplier.ADRESSE_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Designation");
            f.SetFieldType(typeof(String));
            f.SetColName(Supplier.DESIGNATION);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("EMail");
            f.SetFieldType(typeof(String));
            f.SetColName(Supplier.EMAIL_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("NStat");
            f.SetFieldType(typeof(String));
            f.SetColName(Supplier.NSTAT_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Ref");
            f.SetFieldType(typeof(String));
            f.SetColName(Supplier.REF_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Telephone");
            f.SetFieldType(typeof(String));
            f.SetColName(Supplier.TELEPHONE_COL);
            e.Fields.Add(f);

            res.EntitySet.Add(e);
            //##END SUPPLIER##

            //##INPUTPAY##
            e = new EntityInfo();
            e.Package = "Datasource";
            e.EntityName = "InputPay";
            e.TableName = InputPay.TABLE_NAME;

            f = new FieldPrimaryKey();
            f.SetFieldName("Id");
            f.SetFieldType(typeof(int));
            f.SetColName(InputPay.ID_COL);
            e.Fields.Add(f);

            f = new FieldForeignKey();
            f.SetFieldName("IdStoryInputGoods");
            f.SetFieldType(typeof(int));
            f.SetColName(InputPay.IDSTORYINPUTGOODS_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("DateOfPay");
            f.SetFieldType(typeof(DateTime));
            f.SetColName(InputPay.DATEOFPAY_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Amount");
            f.SetFieldType(typeof(DateTime));
            f.SetColName(InputPay.AMOUNT);
            e.Fields.Add(f);

            res.EntitySet.Add(e);
            //##END INPUTPAY##

            //##OUTPUTPAY##
            e = new EntityInfo();
            e.Package = "Datasource";
            e.EntityName = "OutputPay";
            e.TableName = OutputPay.TABLE_NAME;

            f = new FieldPrimaryKey();
            f.SetFieldName("Id");
            f.SetFieldType(typeof(int));
            f.SetColName(OutputPay.ID_COL);
            e.Fields.Add(f);

            f = new FieldForeignKey();
            f.SetFieldName("IdStoryOutputGoods");
            f.SetFieldType(typeof(int));
            f.SetColName(OutputPay.IDSTORYOUTPUTGOODS_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("DateOfPay");
            f.SetFieldType(typeof(DateTime));
            f.SetColName(OutputPay.DATEOFPAY_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Amount");
            f.SetFieldType(typeof(float));
            f.SetColName(OutputPay.AMOUNT);
            e.Fields.Add(f);

            res.EntitySet.Add(e);
            //##END OUTPUTPAY##

            //##UNIT STOCK##
            e = new EntityInfo();
            e.Package = "Datasource";
            e.EntityName = "StockUnit";
            e.TableName = StockUnit.TABLE_NAME;

            f = new FieldPrimaryKey();
            f.SetFieldName("Id");
            f.SetFieldType(typeof(int));
            f.SetColName(StockUnit.ID_COL);
            e.Fields.Add(f);

            f = new FieldForeignKey();
            f.SetFieldName("IdStock");
            f.SetFieldType(typeof(int));
            f.SetColName(StockUnit.ID_STOCK_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Name");
            f.SetFieldType(typeof(String));
            f.SetColName(StockUnit.NAME_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Factor");
            f.SetFieldType(typeof(float));
            f.SetColName(StockUnit.FACTOR_COL);
            e.Fields.Add(f);

            res.EntitySet.Add(e);
            //##END UNIT STOCK##

            //#GESTION DE PROFILS#
            e = new EntityInfo();
            e.Package = "Datasource";
            e.EntityName = "Profile";
            e.TableName = Profile.TABLE_NAME;

            f = new FieldPrimaryKey();
            f.SetFieldName("Id");
            f.SetFieldType(typeof(int));
            f.SetColName(Profile.ID_COL);
            e.Fields.Add(f);            

            f = new FieldEntity();
            f.SetFieldName("FirstName");
            f.SetFieldType(typeof(String));
            f.SetColName(Profile.FIRSTNAME_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("LastName");
            f.SetFieldType(typeof(String));
            f.SetColName(Profile.LASTNAME_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Login");
            f.SetFieldType(typeof(String));
            f.SetColName(Profile.LOGIN_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Pwd");
            f.SetFieldType(typeof(String));
            f.SetColName(Profile.PWD_COL);
            e.Fields.Add(f);

            f = new FieldEntity();
            f.SetFieldName("Privilege");
            f.SetFieldType(typeof(int));
            f.SetColName(Profile.PRIVILEGE_COL);
            e.Fields.Add(f);

            res.EntitySet.Add(e);
            //#END GESTION DE PROFILS#

            //#GESTION DE DROITS D'ACCES#

            //#END GESTION DE DROITS D'ACCES#
            return res;
        }

        public void Initialize()
        {
            EntityLoader.Instance.SetSchema(m_schema);
        }
    }
}
