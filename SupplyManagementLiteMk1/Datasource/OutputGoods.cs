using System;
using System.Collections.Generic;
using System.Text;

namespace SupplyManagementLiteMk1.Datasource
{
    public class OutputGoods
    {
        public const String TABLE_NAME = "ELEMENT_SORTIES";

        private int m_Id;
        public const String ID_COL = "IDELEMENT_SORTIES";
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        private String m_Libelle;
        public const String LIBELLE_COL = "LIBELLE";
        public String Libelle
        {
            get { return m_Libelle; }
            set { m_Libelle = value; }
        }

        private float m_Pu;
        public const String PU_COL = "PU";
        public float Pu
        {
            get { return m_Pu; }
            set { m_Pu = value; }
        }

        private float m_Qte;
        public const String QTE_COL = "QTE";
        public float Qte
        {
            get { return m_Qte; }
            set { m_Qte = value; }
        }

        private String m_Unite;
        public const String UNITE_COL = "UNITE";
        public String Unite
        {
            get { return m_Unite; }
            set { m_Unite = value; }
        }

        private int m_IdStoryOutputGoods;
        public const String ID_STORYOUTPUTGOODS = "IDHISTORIQUE_SORTIES";
        public int IdStoryOutputGoods
        {
            get { return m_IdStoryOutputGoods; }
            set { m_IdStoryOutputGoods = value; }
        }

        private int idStock;
        public const String ID_STOCK_COL = "IDSTOCK";
        public int IdStock
        {
            get { return idStock; }
            set { idStock = value; }
        }

        private bool m_Recent = false;
        public bool IsRecent
        {
            get { return m_Recent; }
            set { m_Recent = value; }
        }

        public float Montant
        {
            get
            {
                return m_Qte * m_Pu * m_Factor;
            }
        }

        private float m_Factor = 1f; //Since 10/04/2011
        public const String FACTOR_COl = "FACTEUR";
        public float Factor
        {
            get { return m_Factor; }
            set { m_Factor = value; }
        }
    }
}
