using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.Datasource
{
    public class Stock
    {
        public const String TABLE_NAME= "STOCK";

        private int m_Id = -1;
        public const String IDSTOCK_COL = "IDSTOCK";
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        private String m_Libelle;
        public const String LIBELLE_COL = "LIBELLE";
        public String Libelle
        {
            get { return m_Libelle; }
            set { m_Libelle = value; }
        }

        private float m_PUAchat;
        public const String PUACHAT_COL = "PU_ACHAT";
        public float PuAchat
        {
            get { return m_PUAchat; }
            set { m_PUAchat = value; }
        }

        private float m_PU;
        public const String PU_COL = "PU";
        public float Pu
        {
            get { return m_PU; }
            set { m_PU = value; }
        }

        private float m_Qte;
        public const String QTE_COL = "QTE";
        public float Qte
        {
            get { return m_Qte; }
            set { m_Qte = value; }
        }

        private String m_Unite;
        public const String UNITE_COL = "UNITE";
        public String Unite
        {
            get { return m_Unite; }
            set { m_Unite = value; }
        }

        public float PrixTotal
        {
            get { return m_PU * m_Qte; }
        }

        private List<StockUnit> m_BufUnits = new List<StockUnit>();
        public List<StockUnit> BufUnits
        {
            get
            {
                return m_BufUnits;
            }
        }

        private List<StockUnit> m_UnitsToBeDeleted = new List<StockUnit>();
        public List<StockUnit> UnitsToBeDeleted
        {
            get
            {
                return m_UnitsToBeDeleted;
            }
        }

        private List<StockUnit> m_Units = new List<StockUnit>();
        public List<StockUnit> Units
        {
            get
            {
                m_Units.Clear();

                List<object> res = EntityLoader.Instance.LoadData(typeof(StockUnit), StockUnit.ID_STOCK_COL + "='" + Id + "'");
                foreach (object o in res)
                    m_Units.Add((StockUnit)o);

                return m_Units;
            }
        }
    }
}
