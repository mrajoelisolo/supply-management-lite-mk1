using System;
using System.Collections.Generic;
using System.Text;
using SupplyManagementLiteMk1.Misc;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.Datasource
{
    public class StoryOutputGoods
    {
        public const String TABLE_NAME = "HISTORIQUE_SORTIES";

        private int m_Id;
        public const String ID_COL = "IDHISTORIQUE_SORTIES";
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        private DateTime m_DateCreation;
        public const String DATECREATION_COL = "DATECREATION";
        public DateTime DateCreation
        {
            get { return m_DateCreation; }
            set { m_DateCreation = value; }
        }

        private String m_Numero;
        public const String NUMERO_COL = "NUMERO";
        public String Numero
        {
            get { return m_Numero; }
            set { m_Numero = value; }
        }

        private int m_IdTiers;
        public const String ID_TIERS_COL = "IDTIERS";
        public int IdTiers
        {
            get { return m_IdTiers; }
            set { m_IdTiers = value; }
        }

        private float m_RemainingAmount = 0F;
        public const String REMAINING_AMOUNT_COL = "RESTEMONTANT";
        public float RemainingAmount
        {
            get { return m_RemainingAmount; }
            set { m_RemainingAmount = value; }
        }

        private List<OutputGoods> m_Goods = new List<OutputGoods>();
        public List<OutputGoods> Goods
        {
            get
            {
                m_Goods.Clear();

                List<object> res = EntityLoader.Instance.LoadData(typeof(OutputGoods), OutputGoods.ID_STORYOUTPUTGOODS + "='" + Id + "'");
                foreach (object o in res)
                    m_Goods.Add((OutputGoods)o);

                return m_Goods;
            }
        }

        public List<OutputPay> m_OutputPaiments = new List<OutputPay>();
        public List<OutputPay> OutputPaiments
        {
            get
            {
                m_OutputPaiments.Clear();

                List<object> res = EntityLoader.Instance.LoadData(typeof(OutputPay), OutputPay.IDSTORYOUTPUTGOODS_COL + "='" + Id + "'");
                foreach (object o in res)
                    m_OutputPaiments.Add((OutputPay)o);

                return m_OutputPaiments;
            }
        }

        public float MontantTotal
        {
            get
            {
                float sum = 0;

                foreach (OutputGoods output in Goods)
                    sum += output.Montant;

                return sum;
            }
        }

        public String szDateCreation
        {
            get
            {
                return DateParser.Instance.ParseDate(DateCreation);
            }
        }

        public String SzDateEdition
        {
            get
            {
                return DateParser.Instance.ParseDate(DateTime.Now);
            }
        }        

        public Tiers ParentTiers
        {
            get
            {
                Tiers res = null;

                res = (Tiers)EntityLoader.Instance.LoadData(typeof(Tiers), m_IdTiers);

                return res;
            }
        }

        public String SzTiersFullName
        {
            get
            {
                String res = "";

                if (ParentTiers != null) res = ParentTiers.FullName;

                return res;
            }
        }

        private float m_LastVersedAmount;
        public float LastVersedAmount
        {
            get { return m_LastVersedAmount; }
            set { m_LastVersedAmount = value; }
        }

        private List<OutputPay> m_Paiments = new List<OutputPay>();
        public List<OutputPay> Paiments
        {
            get
            {
                m_Paiments.Clear();

                List<object> res = EntityLoader.Instance.LoadData(typeof(OutputPay), OutputPay.IDSTORYOUTPUTGOODS_COL + " = '" + m_Id + "'");
                foreach(object o in res)
                    m_Paiments.Add((OutputPay) o);

                return m_Paiments;
            }
        }

        public float TotalPaiments
        {
            get
            {
                float res = 0f;

                foreach (OutputPay pay in Paiments)
                    res += pay.Amount;

                return res;
            }
        }
    }
}
