﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.Datasource
{
    public class Profile
    {
        public const String TABLE_NAME = "PROFILE";

        private int m_Id;
        public const String ID_COL = "IDPROFILE";
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        private String m_FirstName;
        public const String FIRSTNAME_COL = "FIRSTNAME";
        public String FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }

        private String m_LastName;
        public const String LASTNAME_COL = "LASTNAME";
        public String LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }

        private String m_Login;
        public const String LOGIN_COL = "LOGIN";
        public String Login
        {
            get { return m_Login; }
            set { m_Login = value; }
        }

        private String m_Pwd;
        public const String PWD_COL = "PWD";
        public String Pwd
        {
            get { return m_Pwd; }
            set { m_Pwd = value; }
        }

        private int m_Privilege;
        public const String PRIVILEGE_COL = "PRIVILEGE";
        public int Privilege
        {
            get { return m_Privilege; }
            set { m_Privilege = value; }
        }

        public String SzPrivilege
        {
            get
            {
                switch (m_Privilege)
                {
                    case 0: return "ADMIN";
                    case 1: return "MAGASINIER";
                    case 2: return "GERANT";
                }

                return "UNDEFINED";
            }
        }
    }
}
