using System;
using System.Collections.Generic;
using System.Text;
using SupplyManagementLiteMk1.Misc;

namespace SupplyManagementLiteMk1.Datasource
{
    public class InputPay
    {
        public const String TABLE_NAME = "HIST_PAIE_ENTREES";

        private int m_Id;
        public const String ID_COL = "IDHIST_PAIE_ENTREES";
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        private int m_IdStoryInputGoods;
        public const String IDSTORYINPUTGOODS_COL = "IDHISTORIQUE_ENTREES";
        public int IdStoryInputGoods
        {
            get { return m_IdStoryInputGoods; }
            set { m_IdStoryInputGoods = value; }
        }

        private DateTime m_DateOfPay;
        public const String DATEOFPAY_COL = "DATEPAIEMENT";
        public DateTime DateOfPay
        {
            get { return m_DateOfPay; }
            set { m_DateOfPay = value; }
        }

        private float m_Amount;
        public const String AMOUNT = "MONTANT";
        public float Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }

        public String SzDateOfPay
        {
            get
            {
                return DateParser.Instance.ParseDate(m_DateOfPay);
            }
        }
    }
}
