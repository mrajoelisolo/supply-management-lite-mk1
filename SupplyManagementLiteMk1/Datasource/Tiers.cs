using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.Datasource
{
    public class Tiers
    {
        public const String TABLE_NAME = "TIERS";

        private int m_Id;
        public const String ID_COL = "IDTIERS";
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        private String m_Ref;
        public const String REF_COL = "REF";
        public String Ref
        {
            get { return m_Ref; }
            set { m_Ref = value; }
        }

        private String m_Nom;
        public const String NOM_COL = "NOM";
        public String Nom
        {
            get { return m_Nom; }
            set { m_Nom = value; }
        }

        private String m_Sexe;
        public const String SEXE_COL = "SEXE";
        public String Sexe
        {
            get { return m_Sexe; }
            set { m_Sexe = value; }
        }

        private String m_Adresse;
        public const String ADRESSE_COL = "ADRESSE";
        public String Adresse
        {
            get { return m_Adresse; }
            set { m_Adresse = value; }
        }

        private String m_EMail;
        public const String EMAIL_COL = "EMAIL";
        public String EMail
        {
            get { return m_EMail; }
            set { m_EMail = value; }
        }

        private String m_Tel;
        public const String TEL_COL = "TEL";
        public String Tel
        {
            get { return m_Tel; }
            set { m_Tel = value; }
        }

        public String FullName
        {
            get
            {
                return m_Ref + " " + m_Nom;
            }
        }

        public bool IsMale
        {
            get
            {
                if (Sexe == null) return true;
                return Sexe.Equals("M");
            }
            set
            {
                if (value) Sexe = "M";
                else Sexe = "F";
            }
        }

        public bool IsNotMale
        {
            get
            {
                return !IsMale;
            }
        }

        private List<StoryOutputGoods> m_Stories = new List<StoryOutputGoods>();
        public List<StoryOutputGoods> Stories
        {
            get
            {
                m_Stories.Clear();

                List<object> res = EntityLoader.Instance.LoadData(typeof(StoryOutputGoods), StoryOutputGoods.ID_TIERS_COL + "='" + m_Id + "'");
                foreach (object o in res)
                    m_Stories.Add((StoryOutputGoods)o);

                return m_Stories;
            }
        }
    }
}
