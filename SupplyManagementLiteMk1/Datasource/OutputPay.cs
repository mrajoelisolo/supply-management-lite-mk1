using System;
using System.Collections.Generic;
using System.Text;
using SupplyManagementLiteMk1.Misc;

namespace SupplyManagementLiteMk1.Datasource
{
    public class OutputPay
    {
        public const String TABLE_NAME = "HIST_PAIE_SORTIES";

        private int m_Id;
        public const String ID_COL = "IDHIST_PAIE_SORTIES";
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        private int m_IdStoryOutputGoods;
        public const String IDSTORYOUTPUTGOODS_COL = "IDHISTORIQUE_SORTIES";
        public int IdStoryOutputGoods
        {
            get { return m_IdStoryOutputGoods; }
            set { m_IdStoryOutputGoods = value; }
        }

        private DateTime m_DateOfPay;
        public const String DATEOFPAY_COL = "DATEPAIEMENT";
        public DateTime DateOfPay
        {
            get { return m_DateOfPay; }
            set { m_DateOfPay = value; }
        }

        private float m_Amount;
        public const String AMOUNT = "MONTANT";
        public float Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }

        public String SzDateOfPay
        {
            get
            {
                return DateParser.Instance.ParseDate(m_DateOfPay);
            }
        }
    }
}
