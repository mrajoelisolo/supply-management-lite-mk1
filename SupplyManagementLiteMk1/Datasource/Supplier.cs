using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.Datasource
{
    public class Supplier
    {
        public const String TABLE_NAME = "Fournisseur";

        private int m_Id;
        public const String ID_COL = "IDFOURNISSEUR";
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        private String m_Ref;
        public const String REF_COL = "REF";
        public String Ref
        {
            get { return m_Ref; }
            set { m_Ref = value; }
        }

        private String m_Designation;
        public const String DESIGNATION = "DESIGNATION";
        public String Designation
        {
            get { return m_Designation; }
            set { m_Designation = value; }
        }

        private String m_NStat;
        public const String NSTAT_COL = "NSTAT";
        public String NStat
        {
            get { return m_NStat; }
            set { m_NStat = value; }
        }

        private String m_Adresse;
        public const String ADRESSE_COL = "ADRESSE";
        public String Adresse
        {
            get { return m_Adresse; }
            set { m_Adresse = value; }
        }

        private String m_Telephone;
        public const String TELEPHONE_COL = "TELEPHONE";
        public String Telephone
        {
            get { return m_Telephone; }
            set { m_Telephone = value; }
        }

        private String m_EMail;
        public const String EMAIL_COL = "EMAIL";
        public String EMail
        {
            get { return m_EMail; }
            set { m_EMail = value; }
        }

        private List<StoryInputGoods> m_Stories = new List<StoryInputGoods>();
        public List<StoryInputGoods> Stories
        {
            get
            {
                m_Stories.Clear();

                List<object> res = EntityLoader.Instance.LoadData(typeof(StoryInputGoods), StoryInputGoods.ID_TIERS_COL + "='" + m_Id + "'");
                foreach (object o in res)
                    m_Stories.Add((StoryInputGoods)o);

                return m_Stories;
            }
        }
    }
}
