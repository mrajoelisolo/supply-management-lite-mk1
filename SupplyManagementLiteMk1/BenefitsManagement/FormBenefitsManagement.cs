using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.BenefitsManagement.Cmd;

namespace SupplyManagementLiteMk1.BenefitsManagement
{
    public partial class FormBenefitsManagement : Form
    {
        public FormBenefitsManagement()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEvaluate_Click(object sender, EventArgs e)
        {
            //ICommand cmd = new CmdEvaluateBenefits(txtResult, dateTimePicker1.Value, outputBindingSource);
            ICommand cmd = new CmdEvaluateRawBenefits(txtResult, dateTimePicker1.Value, outputBindingSource);
            Invoker.Instance.Invoke(cmd);            
        }
    }
}