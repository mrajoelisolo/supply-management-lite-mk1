using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using TFramework.Utilities;
using ORMFramework.DbUtil.Entity.Query;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.BenefitsManagement.Cmd
{
    public class CmdEvaluateRawBenefits : ICommand
    {
        private TextBox m_TxtResult;
        private DateTime m_DateCreation;
        private BindingSource m_StoryOutputGoodsBindingSource;

        public CmdEvaluateRawBenefits(TextBox txtResult, DateTime dateCreation, BindingSource storyOutputGoodsBindingSource)
        {
            m_TxtResult = txtResult;
            m_DateCreation = dateCreation;
            m_StoryOutputGoodsBindingSource = storyOutputGoodsBindingSource;
        }

        public void Doing()
        {
            float benefit = 0f;

            List<QueryParameter> param = new List<QueryParameter>();
            param.Add(new QueryParameter("DATECREATION", m_DateCreation.Date));

            String whereClause = OutputPay.DATEOFPAY_COL + "=@DATECREATION";
            List<object> res = EntityLoader.Instance.LoadData(typeof(OutputPay), whereClause, param);
            foreach (object r in res)
            {
                OutputPay outputPay = (OutputPay)r;
                benefit += outputPay.Amount;
            }

            whereClause = "EXISTS (SELECT * FROM " + StoryOutputGoods.TABLE_NAME
                + " s WHERE s." + StoryOutputGoods.ID_COL + "=" + OutputGoods.TABLE_NAME + "." + OutputGoods.ID_STORYOUTPUTGOODS
                + " AND s." + StoryOutputGoods.DATECREATION_COL + "=@DATECREATION AND " + StoryOutputGoods.REMAINING_AMOUNT_COL + "='0')";
            res = EntityLoader.Instance.LoadData(typeof(OutputGoods), whereClause, param);
            foreach (object r in res)
            {
                OutputGoods outputGoods = (OutputGoods)r;
                benefit += outputGoods.Montant;
            }

            whereClause = StoryOutputGoods.DATECREATION_COL + "=@DATECREATION";
            m_StoryOutputGoodsBindingSource.DataSource = EntityLoader.Instance.LoadData(typeof(StoryOutputGoods), whereClause, param, StoryOutputGoods.DATECREATION_COL);
            m_TxtResult.Text = DigitToLetters.Instance.ToNbAmount(benefit, "");
        }
    }
}
