using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementMk1Sharp.DbUtil.Entity;
using SupplyManagementLiteMk1.Datasource;
using SupplyManagementMk1Sharp.DbUtil.Query;

namespace SupplyManagementLiteMk1.BenefitsManagement.Cmd
{
    public class CmdEvaluateBenefits : ICommand
    {
        private TextBox m_TxtResult;
        private DateTime m_DateCreation;
        private BindingSource m_IoStoryBindingSource;

        public CmdEvaluateBenefits(TextBox txtResult, DateTime dateCreation, BindingSource ioStoryBindingSource)
        {
            m_TxtResult = txtResult;
            m_DateCreation = dateCreation;
            m_IoStoryBindingSource = ioStoryBindingSource;
        }

        public void Doing()
        {            
            List<QueryParameter> param = new List<QueryParameter>();
            param.Add(new QueryParameter("DATECREATION", m_DateCreation.Date));

            String whereClause = "EXISTS (SELECT * FROM " + StoryOutputGoods.TABLE_NAME + " o WHERE o." + StoryOutputGoods.ID_COL + " = " + OutputGoods.TABLE_NAME + "." + OutputGoods.ID_STORYOUTPUTGOODS + " AND ";
            whereClause += StoryOutputGoods.DATECREATION_COL + "=@DATECREATION)";

            List<object> results = EntityLoader.Instance.LoadData(typeof(OutputGoods), whereClause, param);
            float benefit = 0f;
            foreach (object o in results)
            {
                OutputGoods stock = (OutputGoods)o;
                benefit += stock.Qte * stock.Pu * stock.Factor; //Factor added since 11/04/11
            }

            results = EntityLoader.Instance.LoadData(typeof(StoryOutputGoods), StoryOutputGoods.DATECREATION_COL + "=@DATECREATION", param);
            foreach(object o in results)
            {
                StoryOutputGoods story = (StoryOutputGoods) o;
                benefit -= story.RemainingAmount;
            }

            //whereClause = IOStory.MOVEMENTTYPE_COL + " = '1' AND ";
            //whereClause = " AND ";
            //whereClause += StoryOutputGoods.DATECREATION_COL + "=@DATECREATION";
            //m_IoStoryBindingSource.DataSource = EntityLoader.Instance.LoadData(typeof(StoryOutputGoods), whereClause, param, OutputGoods.LIBELLE_COL);
            whereClause = StoryOutputGoods.DATECREATION_COL + "=@DATECREATION";
            m_IoStoryBindingSource.DataSource = EntityLoader.Instance.LoadData(typeof(StoryOutputGoods), whereClause, param, StoryOutputGoods.DATECREATION_COL);

            m_TxtResult.Text = "" + benefit;
        }
    }
}
