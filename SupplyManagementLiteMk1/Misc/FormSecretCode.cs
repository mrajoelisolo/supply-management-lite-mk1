using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SupplyManagementLiteMk1.Misc
{
    public partial class FormSecretCode : Form
    {
        public FormSecretCode()
        {
            InitializeComponent();

            CenterToParent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public String GetPassword()
        {
            return txtPwd.Text;
        }

        private void txtPwd_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnOK.PerformClick();
            else if (e.KeyCode == Keys.Escape)
                btnCancel.PerformClick();
        }
    }
}