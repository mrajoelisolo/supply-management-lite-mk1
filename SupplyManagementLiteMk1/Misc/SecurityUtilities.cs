using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace SupplyManagementLiteMk1.Misc
{
    public class SecurityUtilities
    {
        private static SecurityUtilities m_Instance = new SecurityUtilities();

        private SecurityUtilities()
        {
        }

        public static SecurityUtilities Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public Boolean Authentify()
        {
            Boolean res = false;

            FormSecretCode f = new FormSecretCode();
            if (f.ShowDialog() == DialogResult.OK)
                if (f.GetPassword() == "superadmin") res = true;

            return res;
        }
    }
}
