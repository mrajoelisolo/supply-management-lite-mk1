using System;
using System.Collections.Generic;
using System.Text;
using SupplyManagementLiteMk1.Datasource;

namespace SupplyManagementLiteMk1.Misc
{
    public class DataChecker
    {
        private static DataChecker m_Instance = new DataChecker();

        private DataChecker() { }

        public static DataChecker Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public Boolean Check(object o)
        {
            if (typeof(Stock).Equals(o.GetType()))
            {
                Stock stock = (Stock)o;

                if (String.IsNullOrEmpty(stock.Libelle))
                {
                    ShowErrorMessage("Tsy maintsy asiana anarana ny 'karazana'");
                    return false;
                }

                if (stock.PuAchat <= 0)
                {
                    ShowErrorMessage("Tsy mety ny voasoratra eo amin'ny 'PUAchat'");
                    return false;
                }

                if (stock.Pu <= 0)
                {
                    ShowErrorMessage("Tsy mety ny voasoratra eo amin'ny 'Vidiny'");
                    return false;
                }

                if (stock.Qte < 0)
                {
                    ShowErrorMessage("Tsy mety ny voasoratra eo amin'ny isan'ny 'Fatrany'");
                    return false;
                }

                //if (String.IsNullOrEmpty(stock.Unite))
                //{
                //    ShowErrorMessage("Tsy maintsy asiana soratra ao amin'ny 'Unite'");
                //    return false;
                //}
            }

            return true;
        }

        private void ShowErrorMessage(String message)
        {
            System.Windows.Forms.MessageBox.Show(message, "Tsy mety", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
        }
    }
}
