using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;

namespace SupplyManagementLiteMk1.Misc
{
    public partial class FormBrowseUnits : Form
    {
        public FormBrowseUnits(Stock stock, Boolean buffered)
        {
            InitializeComponent();

            StockUnit stockUnit = new StockUnit();
            stockUnit.Factor = 1f;
            stockUnit.Name = stock.Unite;
            unitsBindingSource.Add(stockUnit);

            List<StockUnit> units = stock.Units;

            if (buffered)
                units = stock.BufUnits;

            foreach (StockUnit unit in units)
                unitsBindingSource.Add(unit);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public StockUnit SelectedUnit
        {
            get
            {
                return (StockUnit)unitsBindingSource.Current;
            }
        }
    }
}