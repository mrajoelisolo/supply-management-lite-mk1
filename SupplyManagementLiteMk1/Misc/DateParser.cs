using System;
using System.Collections.Generic;
using System.Text;

namespace SupplyManagementLiteMk1.Misc
{
    public class DateParser
    {
        private static DateParser m_Instance = new DateParser();

        private DateParser() { }

        public static DateParser Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public String ParseDate(DateTime date)
        {
            String res = "" + date.Day + " ";

            switch (date.Month)
            {
                case 1: res += "Janvier"; break;
                case 2: res += "F�vrier"; break;
                case 3: res += "Mars"; break;
                case 4: res += "Avril"; break;
                case 5: res += "Mai"; break;
                case 6: res += "Juin"; break;
                case 7: res += "Juillet"; break;
                case 8: res += "Ao�t"; break;
                case 9: res += "Septembre"; break;
                case 10: res += "Octobre"; break;
                case 11: res += "Novembre"; break;
                case 12: res += "D�cembre"; break;
            }

            res += " " + date.Year;

            return res;
        }
    }
}
