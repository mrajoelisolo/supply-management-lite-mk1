using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.Misc
{
    public class MDIManager
    {
        private static MDIManager m_Instance = new MDIManager();

        private MDIManager() { }

        public static MDIManager Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public void ShowMDIChild(Form parent, Type childType, FormWindowState ws)
        {
            if (parent == null) return;

            foreach (Form child in parent.MdiChildren)
                if (child.GetType().Equals(childType))
                {
                    child.Activate();
                    return;
                }

            Form childForm = (Form) Reflector.Instance.CreateInstance(childType);
            childForm.WindowState = ws;
            childForm.MdiParent = parent;
            childForm.Show();
        }
    }
}
