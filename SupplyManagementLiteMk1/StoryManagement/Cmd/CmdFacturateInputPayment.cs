using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using System.IO;
using TFramework.Reporting;
using TFramework.Utilities;
using SupplyManagementLiteMk1.MainApp.Modules;

namespace SupplyManagementLiteMk1.StoryManagement.Cmd
{
    public class CmdFacturateInputPayment : ICommand
    {
        private BindingSource m_StoryInputGoodsBindingSource;

        public CmdFacturateInputPayment(BindingSource storyInputGoodsBindingSource)
        {
            m_StoryInputGoodsBindingSource = storyInputGoodsBindingSource;
        }

        public void Doing()
        {
            StoryInputGoods story = (StoryInputGoods)m_StoryInputGoodsBindingSource.Current;
            if (story == null) return;

            DialogResult res = MessageBox.Show("Avoaka ve ny Bon d'Entr�e ?", "Entana input", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.No) return;

            String src = SystemManager.Instance.GetSystemDirectory() + "\\ModeleBE2.rep";

            SaveFileDialog sf = new SaveFileDialog();
            sf.Filter = SystemManager.Instance.ReportFilter;

            sf.FileName = "BE_" + story.Numero + ".doc";
            if (sf.ShowDialog() == DialogResult.Cancel) return;

            String dest = sf.FileName;

            try
            {
                File.Copy(src, dest);
            }
            catch (Exception ex)
            {
                File.Delete(dest);
                File.Copy(src, dest);
                System.Console.WriteLine(ex.Message);
            }

            MSWordUtilities.Instance.Initialize(dest);

            MSWordUtilities.Instance.ReplaceField("NomFNRS", story.SzSupplierFullName);
            MSWordUtilities.Instance.ReplaceField("NoBE", story.Numero);
            MSWordUtilities.Instance.ReplaceField("CreationDate", story.szDateCreation);
            MSWordUtilities.Instance.ReplaceField("EditionDate", story.SzDateEdition);

            //Tableau des articles
            List<List<String>> rows = new List<List<String>>();
            foreach (InputGoods goods in story.Goods)
            {
                List<String> row = new List<String>();

                row.Add(goods.Libelle);
                row.Add(DigitToLetters.Instance.ToNbAmount(goods.Pu, ""));
                row.Add("" + goods.Qte);
                row.Add(goods.Unite);
                row.Add(DigitToLetters.Instance.ToNbAmount(goods.Montant, ""));

                rows.Add(row);
            }
            MSWordUtilities.Instance.FillTable("TableauBE", rows);

            MSWordUtilities.Instance.ReplaceField("TotalMontant", DigitToLetters.Instance.ToNbAmount(story.MontantTotal, "Ar"));

            //Liste des paiements
            rows = new List<List<String>>();
            foreach (InputPay pay in story.InputPaiments)
            {
                List<String> row = new List<String>();

                row.Add(pay.SzDateOfPay);
                row.Add(DigitToLetters.Instance.ToNbAmount(pay.Amount, ""));

                rows.Add(row);
            }
            MSWordUtilities.Instance.FillTable("TableauPaiement", rows);

            MSWordUtilities.Instance.ReplaceField("TotalPaiement", DigitToLetters.Instance.ToNbAmount(story.TotalPaiments, "Ar"));

            if (story.RemainingAmount > 0)
                MSWordUtilities.Instance.ReplaceField("MontantRestant", DigitToLetters.Instance.ToNbAmount(story.RemainingAmount, "Ar"));
            else
                MSWordUtilities.Instance.ReplaceField("MontantRestant", "0 Ar");

            MSWordUtilities.Instance.Visible = true;
            MSWordUtilities.Instance.SaveDocument();
        }
    }
}
