using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using System.IO;
using SupplyManagementLiteMk1.MainApp.Modules;
using TFramework.Reporting;
using TFramework.Utilities;

namespace SupplyManagementLiteMk1.StoryManagement.Cmd
{
    public class CmdFacturateOutputPayment : ICommand
    {
        private BindingSource m_StoryOutputGoodsBindingSource;

        public CmdFacturateOutputPayment(BindingSource storyOutputGoodsBindingSource)
        {
            m_StoryOutputGoodsBindingSource = storyOutputGoodsBindingSource;
        }

        public void Doing()
        {
            StoryOutputGoods story = (StoryOutputGoods)m_StoryOutputGoodsBindingSource.Current;
            if (story == null) return;

            DialogResult res = MessageBox.Show("Avoaka ve ny faktiora ?", "Entana mivoaka", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.No) return;

            String src = SystemManager.Instance.GetSystemDirectory() + "\\ModeleFacture2.rep";

            SaveFileDialog sf = new SaveFileDialog();
            sf.Filter = SystemManager.Instance.ReportFilter;

            sf.FileName = "Facture_" + story.Numero + ".doc";
            if (sf.ShowDialog() == DialogResult.Cancel) return;

            String dest = sf.FileName;

            try
            {
                File.Copy(src, dest);
            }
            catch (Exception ex)
            {
                File.Delete(dest);
                File.Copy(src, dest);
                System.Console.WriteLine(ex.Message);
            }

            MSWordUtilities.Instance.Initialize(dest);

            MSWordUtilities.Instance.ReplaceField("NomClient", story.SzTiersFullName);
            MSWordUtilities.Instance.ReplaceField("NoFacture", story.Numero);
            MSWordUtilities.Instance.ReplaceField("CreationDate", story.szDateCreation);
            MSWordUtilities.Instance.ReplaceField("EditionDate", story.SzDateEdition);

            //Tableau des articles
            List<List<String>> rows = new List<List<String>>();
            foreach (OutputGoods goods in story.Goods)
            {
                List<String> row = new List<String>();

                row.Add(goods.Libelle);
                row.Add(DigitToLetters.Instance.ToNbAmount(goods.Pu, ""));
                row.Add("" + goods.Qte);
                row.Add(goods.Unite);
                row.Add(DigitToLetters.Instance.ToNbAmount(goods.Montant, ""));

                rows.Add(row);
            }
            MSWordUtilities.Instance.FillTable("TableauFacture", rows);

            MSWordUtilities.Instance.ReplaceField("TotalMontant", DigitToLetters.Instance.ToNbAmount(story.MontantTotal, "Ar"));

            //Liste des paiements
            rows = new List<List<String>>();
            foreach (OutputPay pay in story.OutputPaiments)
            {
                List<String> row = new List<String>();

                row.Add(pay.SzDateOfPay);
                row.Add(DigitToLetters.Instance.ToNbAmount(pay.Amount, ""));

                rows.Add(row);
            }
            MSWordUtilities.Instance.FillTable("TableauPaiement", rows);

            MSWordUtilities.Instance.ReplaceField("TotalPaiement", DigitToLetters.Instance.ToNbAmount(story.TotalPaiments, "Ar"));

            if (story.RemainingAmount > 0)
                MSWordUtilities.Instance.ReplaceField("MontantRestant", DigitToLetters.Instance.ToNbAmount(story.RemainingAmount, "Ar"));
            else
                MSWordUtilities.Instance.ReplaceField("MontantRestant", "0 Ar");

            MSWordUtilities.Instance.Visible = true;
            MSWordUtilities.Instance.SaveDocument();
        }
    }
}
