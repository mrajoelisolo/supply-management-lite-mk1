using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.StoryManagement.Cmd
{
    public class CmdPayInputGoods : ICommand
    {
        private BindingSource m_StoryInputGoodsBindingSource;
        private IObserver m_Observer;

        public CmdPayInputGoods(BindingSource storyInputGoodsBindingSource, IObserver observer)
        {
            m_StoryInputGoodsBindingSource = storyInputGoodsBindingSource;
            m_Observer = observer;
        }

        public void Doing()
        {
            StoryInputGoods story = (StoryInputGoods)m_StoryInputGoodsBindingSource.Current;
            if (story == null) return;

            FormPayInput f = new FormPayInput(story);
            if (f.ShowDialog() == DialogResult.OK)
            {
                DialogResult res = MessageBox.Show("Aloha io vola io ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (res == DialogResult.No) return;

                InputPay inputPay = new InputPay();
                inputPay.IdStoryInputGoods = story.Id;
                inputPay.DateOfPay = DateTime.Now;
                inputPay.Amount = story.LastVersedAmount;

                story.RemainingAmount = story.RemainingAmount - story.LastVersedAmount;
                story.LastVersedAmount = 0f;

                EntityLoader.Instance.Save(inputPay);
                EntityLoader.Instance.Update(story);

                //On imprime le BE
                ICommand cmd = new CmdFacturateInputPayment(m_StoryInputGoodsBindingSource);
                Invoker.Instance.Invoke(cmd);

                m_Observer.Notify("1");
            }
        }
    }
}
