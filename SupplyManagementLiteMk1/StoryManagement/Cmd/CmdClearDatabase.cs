using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.StoryManagement.Cmd
{
    public class CmdClearDatabase : ICommand
    {
        private IObserver m_Observer;

        public CmdClearDatabase(IObserver observer)
        {
            m_Observer = observer;
        }

        public void Doing()
        {
            FormClearDatabase f = new FormClearDatabase();
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                return;

            try {
                StringBuilder str = new StringBuilder();
                str.Append("TRUNCATE TABLE element_entrees;");
                str.Append("TRUNCATE TABLE element_sorties;");
                str.Append("TRUNCATE TABLE fournisseur;");
                str.Append("TRUNCATE TABLE hist_paie_entrees;");
                str.Append("TRUNCATE TABLE hist_paie_sorties;");
                str.Append("TRUNCATE TABLE historique_entrees;");
                str.Append("TRUNCATE TABLE historique_sorties;");
                str.Append("TRUNCATE TABLE stock;");
                str.Append("TRUNCATE TABLE tiers;");
                str.Append("TRUNCATE TABLE unitestock;");
                EntityLoader.Instance.ExecuteNonQuery(str.ToString());
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message, "Erreur", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }
    }
}
