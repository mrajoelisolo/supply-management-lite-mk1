using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.StoryManagement.Cmd
{
    public class CmdClearStory : ICommand
    {
        private IObserver m_Observer;

        public CmdClearStory(IObserver observer)
        {
            m_Observer = observer;
        }

        public void Doing()
        {
            FormClearStory f = new FormClearStory();
            if (f.ShowDialog() == DialogResult.Cancel) return;

            if (f.Result == 2 || f.Result == 1)
            {
                EntityLoader.Instance.ExecuteNonQuery("TRUNCATE TABLE " + StoryInputGoods.TABLE_NAME);
                EntityLoader.Instance.ExecuteNonQuery("TRUNCATE TABLE " + InputGoods.TABLE_NAME);
                EntityLoader.Instance.ExecuteNonQuery("TRUNCATE TABLE " + InputPay.TABLE_NAME);

                m_Observer.Notify("1");
            }

            if (f.Result == 3 || f.Result == 1)
            {
                EntityLoader.Instance.ExecuteNonQuery("TRUNCATE TABLE " + StoryOutputGoods.TABLE_NAME);
                EntityLoader.Instance.ExecuteNonQuery("TRUNCATE TABLE " + OutputGoods.TABLE_NAME);
                EntityLoader.Instance.ExecuteNonQuery("TRUNCATE TABLE " + OutputPay.TABLE_NAME);

                m_Observer.Notify("2");
            }                       
        }
    }
}
