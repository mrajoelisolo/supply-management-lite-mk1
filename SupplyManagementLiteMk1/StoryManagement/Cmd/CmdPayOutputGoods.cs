using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.StoryManagement.Cmd
{
    public class CmdPayOutputGoods : ICommand
    {
        private BindingSource m_StoryOutputGoodsBindingSource;
        private IObserver m_Observer;

        public CmdPayOutputGoods(BindingSource storyOutputGoodsBindingSource, IObserver observer)
        {
            m_StoryOutputGoodsBindingSource = storyOutputGoodsBindingSource;
            m_Observer = observer;
        }

        public void Doing()
        {
            StoryOutputGoods story = (StoryOutputGoods)m_StoryOutputGoodsBindingSource.Current;
            if (story == null) return;

            FormPayOutput f = new FormPayOutput(story);
            if (f.ShowDialog() == DialogResult.OK)
            {               
                DialogResult res = MessageBox.Show("Aloha io vola io ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (res == DialogResult.No) return;

                OutputPay outputPay = new OutputPay();
                outputPay.IdStoryOutputGoods = story.Id;
                outputPay.DateOfPay = DateTime.Now;
                outputPay.Amount = story.LastVersedAmount;

                story.RemainingAmount = story.RemainingAmount - story.LastVersedAmount;
                story.LastVersedAmount = 0f;                

                EntityLoader.Instance.Save(outputPay);
                EntityLoader.Instance.Update(story);

                //On imprime la facture
                ICommand cmd = new CmdFacturateOutputPayment(m_StoryOutputGoodsBindingSource);
                Invoker.Instance.Invoke(cmd);

                m_Observer.Notify("2");
            }
        }
    }
}
