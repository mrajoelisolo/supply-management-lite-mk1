using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.StoryManagement.Cmd;
using SupplyManagementLiteMk1.StoryManagement.Util;

namespace SupplyManagementLiteMk1.StoryManagement
{
    public partial class CtrlStoryInputGoods : UserControl, IObserver
    {
        public CtrlStoryInputGoods()
        {
            InitializeComponent();
        }        

        private void btnSearch1_Click(object sender, EventArgs e)
        {
            Notify("");
        }        

        private void btnPay1_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdPayInputGoods(storyInputGoodsBindingSource, this);
            Invoker.Instance.Invoke(cmd);
        }        

        private void btnFacturate1_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdFacturateInputPayment(storyInputGoodsBindingSource);
            Invoker.Instance.Invoke(cmd);
        }

        public void Notify(string msg)
        {
            bool bShowAll = false;
            DateTime fromDate = fromDateTimePicker1.Value;
            DateTime toDate = toDateTimePicker1.Value;

            storyInputGoodsBindingSource.DataSource = QueryBuilder.Instance.getInputStories(chkDate1.Checked, fromDate, toDate, txtSupplierName1.Text, txtNumSupplier1.Text, bShowAll);
        }
    }
}
