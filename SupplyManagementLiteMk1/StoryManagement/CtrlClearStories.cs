using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ORMFramework.AppUtil;
using TFramework.Properties;
using SupplyManagementLiteMk1.Properties;
using SupplyManagementLiteMk1.StoryManagement.Cmd;

namespace SupplyManagementLiteMk1.StoryManagement
{
    public partial class CtrlClearStories : UserControl, IObserver
    {
        private List<IObserver> m_Observers = new List<IObserver>();

        public CtrlClearStories()
        {
            InitializeComponent();
        }

        private void btnClearStory3_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdClearStory(this);
            Invoker.Instance.Invoke(cmd);
            btnClearDatabase3.Enabled = false;
            btnClearStory3.Enabled = false;
            pictureBoxLock3.Image = Resources.Lock_Locked;
        }

        private void btnOK3_Click(object sender, EventArgs e)
        {
            String pwd = txtPwd3.Text;
            if (pwd.Equals("admin"))
            {
                txtPwd3.Text = "";
                btnClearStory3.Enabled = true;
                btnClearDatabase3.Enabled = true;
                
                pictureBoxLock3.Image = Resources.Lock_Open;
            }
            else
            {
                MessageBox.Show("Diso ny teny miafina voasoratra", "Tsy mety", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnClearStory3.Enabled = false;
                btnClearDatabase3.Enabled = false;
                pictureBoxLock3.Image = Resources.Lock_Locked;
            }
        }                

        private void button1_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdClearDatabase(this);
            Invoker.Instance.Invoke(cmd);
            btnClearDatabase3.Enabled = false;
            btnClearStory3.Enabled = false;
            pictureBoxLock3.Image = Resources.Lock_Locked;
        }

        public List<IObserver> Observers
        {
            get
            {
                return m_Observers;
            }
        }

        public void Notify(string msg)
        {
            foreach (IObserver observer in m_Observers)
                observer.Notify("");
        }
    }
}
