﻿namespace SupplyManagementLiteMk1.StoryManagement
{
    partial class CtrlStoryOutputGoods
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.chkDate2 = new System.Windows.Forms.CheckBox();
            this.lblFromDate2 = new System.Windows.Forms.Label();
            this.lblToDate2 = new System.Windows.Forms.Label();
            this.fromDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.toDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.btnSearch2 = new System.Windows.Forms.Button();
            this.lblTiersName2 = new System.Windows.Forms.Label();
            this.txtTiersName2 = new System.Windows.Forms.TextBox();
            this.txtTiersNum2 = new System.Windows.Forms.TextBox();
            this.lblNumSupplier2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnPay2 = new System.Windows.Forms.Button();
            this.btnFacturate2 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.storyOutputGoodsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.outputGoodsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.szTiersFullNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateCreationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remainingAmountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montantTotalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.libelleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.puDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uniteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montantDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storyOutputGoodsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.outputGoodsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView2, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(778, 484);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.Silver;
            this.tableLayoutPanel3.ColumnCount = 7;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.Controls.Add(this.chkDate2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblFromDate2, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblToDate2, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.fromDateTimePicker2, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.toDateTimePicker2, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnSearch2, 6, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblTiersName2, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtTiersName2, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtTiersNum2, 4, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblNumSupplier2, 3, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(772, 74);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // chkDate2
            // 
            this.chkDate2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.chkDate2.AutoSize = true;
            this.chkDate2.Location = new System.Drawing.Point(3, 11);
            this.chkDate2.Name = "chkDate2";
            this.chkDate2.Size = new System.Drawing.Size(15, 14);
            this.chkDate2.TabIndex = 0;
            this.chkDate2.UseVisualStyleBackColor = true;
            // 
            // lblFromDate2
            // 
            this.lblFromDate2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblFromDate2.AutoSize = true;
            this.lblFromDate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFromDate2.Location = new System.Drawing.Point(24, 5);
            this.lblFromDate2.Name = "lblFromDate2";
            this.lblFromDate2.Size = new System.Drawing.Size(79, 26);
            this.lblFromDate2.TabIndex = 1;
            this.lblFromDate2.Text = "Manomboka amin\'ny";
            // 
            // lblToDate2
            // 
            this.lblToDate2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblToDate2.AutoSize = true;
            this.lblToDate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToDate2.Location = new System.Drawing.Point(304, 5);
            this.lblToDate2.Name = "lblToDate2";
            this.lblToDate2.Size = new System.Drawing.Size(71, 26);
            this.lblToDate2.TabIndex = 2;
            this.lblToDate2.Text = "ka hatramin\'ny";
            // 
            // fromDateTimePicker2
            // 
            this.fromDateTimePicker2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.fromDateTimePicker2.Location = new System.Drawing.Point(118, 8);
            this.fromDateTimePicker2.Name = "fromDateTimePicker2";
            this.fromDateTimePicker2.Size = new System.Drawing.Size(180, 20);
            this.fromDateTimePicker2.TabIndex = 3;
            // 
            // toDateTimePicker2
            // 
            this.toDateTimePicker2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.toDateTimePicker2.Location = new System.Drawing.Point(398, 8);
            this.toDateTimePicker2.Name = "toDateTimePicker2";
            this.toDateTimePicker2.Size = new System.Drawing.Size(180, 20);
            this.toDateTimePicker2.TabIndex = 4;
            // 
            // btnSearch2
            // 
            this.btnSearch2.Image = global::SupplyManagementLiteMk1.Properties.Resources.iconGreen_tiny;
            this.btnSearch2.Location = new System.Drawing.Point(678, 3);
            this.btnSearch2.Name = "btnSearch2";
            this.btnSearch2.Size = new System.Drawing.Size(34, 31);
            this.btnSearch2.TabIndex = 5;
            this.btnSearch2.UseVisualStyleBackColor = true;
            this.btnSearch2.Click += new System.EventHandler(this.btnSearch2_Click);
            // 
            // lblTiersName2
            // 
            this.lblTiersName2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTiersName2.AutoSize = true;
            this.lblTiersName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTiersName2.Location = new System.Drawing.Point(24, 42);
            this.lblTiersName2.Name = "lblTiersName2";
            this.lblTiersName2.Size = new System.Drawing.Size(67, 26);
            this.lblTiersName2.TabIndex = 6;
            this.lblTiersName2.Text = "Anaran\'ny client";
            // 
            // txtTiersName2
            // 
            this.txtTiersName2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtTiersName2.Location = new System.Drawing.Point(118, 45);
            this.txtTiersName2.Name = "txtTiersName2";
            this.txtTiersName2.Size = new System.Drawing.Size(180, 20);
            this.txtTiersName2.TabIndex = 7;
            // 
            // txtTiersNum2
            // 
            this.txtTiersNum2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtTiersNum2.Location = new System.Drawing.Point(398, 45);
            this.txtTiersNum2.Name = "txtTiersNum2";
            this.txtTiersNum2.Size = new System.Drawing.Size(180, 20);
            this.txtTiersNum2.TabIndex = 8;
            // 
            // lblNumSupplier2
            // 
            this.lblNumSupplier2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblNumSupplier2.AutoSize = true;
            this.lblNumSupplier2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumSupplier2.Location = new System.Drawing.Point(304, 49);
            this.lblNumSupplier2.Name = "lblNumSupplier2";
            this.lblNumSupplier2.Size = new System.Drawing.Size(50, 13);
            this.lblNumSupplier2.TabIndex = 9;
            this.lblNumSupplier2.Text = "Numero";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Silver;
            this.flowLayoutPanel1.Controls.Add(this.btnPay2);
            this.flowLayoutPanel1.Controls.Add(this.btnFacturate2);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 444);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(778, 40);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // btnPay2
            // 
            this.btnPay2.BackColor = System.Drawing.Color.Gray;
            this.btnPay2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPay2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPay2.Image = global::SupplyManagementLiteMk1.Properties.Resources.iconGreen_tiny;
            this.btnPay2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPay2.Location = new System.Drawing.Point(3, 3);
            this.btnPay2.Name = "btnPay2";
            this.btnPay2.Size = new System.Drawing.Size(150, 30);
            this.btnPay2.TabIndex = 0;
            this.btnPay2.Text = "Handoa";
            this.btnPay2.UseVisualStyleBackColor = false;
            this.btnPay2.Click += new System.EventHandler(this.btnPay2_Click);
            // 
            // btnFacturate2
            // 
            this.btnFacturate2.BackColor = System.Drawing.Color.Gray;
            this.btnFacturate2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFacturate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFacturate2.Image = global::SupplyManagementLiteMk1.Properties.Resources.iconYellow_tiny;
            this.btnFacturate2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFacturate2.Location = new System.Drawing.Point(159, 3);
            this.btnFacturate2.Name = "btnFacturate2";
            this.btnFacturate2.Size = new System.Drawing.Size(150, 30);
            this.btnFacturate2.TabIndex = 1;
            this.btnFacturate2.Text = "Faktiora";
            this.btnFacturate2.UseVisualStyleBackColor = false;
            this.btnFacturate2.Click += new System.EventHandler(this.btnFacturate2_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.szTiersFullNameDataGridViewTextBoxColumn,
            this.dateCreationDataGridViewTextBoxColumn,
            this.numeroDataGridViewTextBoxColumn,
            this.remainingAmountDataGridViewTextBoxColumn,
            this.montantTotalDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.storyOutputGoodsBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 83);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(772, 176);
            this.dataGridView1.TabIndex = 3;
            // 
            // storyOutputGoodsBindingSource
            // 
            this.storyOutputGoodsBindingSource.DataSource = typeof(SupplyManagementLiteMk1.Datasource.StoryOutputGoods);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.libelleDataGridViewTextBoxColumn,
            this.qteDataGridViewTextBoxColumn,
            this.puDataGridViewTextBoxColumn,
            this.uniteDataGridViewTextBoxColumn,
            this.montantDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.outputGoodsBindingSource;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 265);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.Size = new System.Drawing.Size(772, 176);
            this.dataGridView2.TabIndex = 4;
            // 
            // outputGoodsBindingSource
            // 
            this.outputGoodsBindingSource.DataMember = "Goods";
            this.outputGoodsBindingSource.DataSource = this.storyOutputGoodsBindingSource;
            // 
            // szTiersFullNameDataGridViewTextBoxColumn
            // 
            this.szTiersFullNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.szTiersFullNameDataGridViewTextBoxColumn.DataPropertyName = "SzTiersFullName";
            this.szTiersFullNameDataGridViewTextBoxColumn.HeaderText = "Client";
            this.szTiersFullNameDataGridViewTextBoxColumn.Name = "szTiersFullNameDataGridViewTextBoxColumn";
            this.szTiersFullNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateCreationDataGridViewTextBoxColumn
            // 
            this.dateCreationDataGridViewTextBoxColumn.DataPropertyName = "DateCreation";
            this.dateCreationDataGridViewTextBoxColumn.HeaderText = "Date de création";
            this.dateCreationDataGridViewTextBoxColumn.Name = "dateCreationDataGridViewTextBoxColumn";
            this.dateCreationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // numeroDataGridViewTextBoxColumn
            // 
            this.numeroDataGridViewTextBoxColumn.DataPropertyName = "Numero";
            this.numeroDataGridViewTextBoxColumn.HeaderText = "Numero facture";
            this.numeroDataGridViewTextBoxColumn.Name = "numeroDataGridViewTextBoxColumn";
            this.numeroDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // remainingAmountDataGridViewTextBoxColumn
            // 
            this.remainingAmountDataGridViewTextBoxColumn.DataPropertyName = "RemainingAmount";
            this.remainingAmountDataGridViewTextBoxColumn.HeaderText = "Reste à payer (Ar)";
            this.remainingAmountDataGridViewTextBoxColumn.Name = "remainingAmountDataGridViewTextBoxColumn";
            this.remainingAmountDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // montantTotalDataGridViewTextBoxColumn
            // 
            this.montantTotalDataGridViewTextBoxColumn.DataPropertyName = "MontantTotal";
            this.montantTotalDataGridViewTextBoxColumn.HeaderText = "Montant total (Ar)";
            this.montantTotalDataGridViewTextBoxColumn.Name = "montantTotalDataGridViewTextBoxColumn";
            this.montantTotalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // libelleDataGridViewTextBoxColumn
            // 
            this.libelleDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.libelleDataGridViewTextBoxColumn.DataPropertyName = "Libelle";
            this.libelleDataGridViewTextBoxColumn.HeaderText = "Libelle";
            this.libelleDataGridViewTextBoxColumn.Name = "libelleDataGridViewTextBoxColumn";
            this.libelleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // qteDataGridViewTextBoxColumn
            // 
            this.qteDataGridViewTextBoxColumn.DataPropertyName = "Qte";
            this.qteDataGridViewTextBoxColumn.HeaderText = "Fatrany";
            this.qteDataGridViewTextBoxColumn.Name = "qteDataGridViewTextBoxColumn";
            this.qteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // puDataGridViewTextBoxColumn
            // 
            this.puDataGridViewTextBoxColumn.DataPropertyName = "Pu";
            this.puDataGridViewTextBoxColumn.HeaderText = "PU Vente (Ar)";
            this.puDataGridViewTextBoxColumn.Name = "puDataGridViewTextBoxColumn";
            this.puDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // uniteDataGridViewTextBoxColumn
            // 
            this.uniteDataGridViewTextBoxColumn.DataPropertyName = "Unite";
            this.uniteDataGridViewTextBoxColumn.HeaderText = "Unité";
            this.uniteDataGridViewTextBoxColumn.Name = "uniteDataGridViewTextBoxColumn";
            this.uniteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // montantDataGridViewTextBoxColumn
            // 
            this.montantDataGridViewTextBoxColumn.DataPropertyName = "Montant";
            this.montantDataGridViewTextBoxColumn.HeaderText = "Montant (Ar)";
            this.montantDataGridViewTextBoxColumn.Name = "montantDataGridViewTextBoxColumn";
            this.montantDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // CtrlStoryOutputGoods
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "CtrlStoryOutputGoods";
            this.Size = new System.Drawing.Size(778, 484);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storyOutputGoodsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.outputGoodsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.CheckBox chkDate2;
        private System.Windows.Forms.Label lblFromDate2;
        private System.Windows.Forms.Label lblToDate2;
        private System.Windows.Forms.DateTimePicker fromDateTimePicker2;
        private System.Windows.Forms.DateTimePicker toDateTimePicker2;
        private System.Windows.Forms.Button btnSearch2;
        private System.Windows.Forms.Label lblTiersName2;
        private System.Windows.Forms.TextBox txtTiersName2;
        private System.Windows.Forms.TextBox txtTiersNum2;
        private System.Windows.Forms.Label lblNumSupplier2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnPay2;
        private System.Windows.Forms.Button btnFacturate2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.BindingSource storyOutputGoodsBindingSource;
        private System.Windows.Forms.BindingSource outputGoodsBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn szTiersFullNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateCreationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn remainingAmountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn montantTotalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn libelleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn qteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn puDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uniteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn montantDataGridViewTextBoxColumn;
    }
}
