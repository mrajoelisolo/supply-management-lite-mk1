﻿namespace SupplyManagementLiteMk1.StoryManagement
{
    partial class CtrlAllStoryInputGoods
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFormDate1 = new System.Windows.Forms.Label();
            this.lblToDate1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFromDate3 = new System.Windows.Forms.Label();
            this.lblToDate3 = new System.Windows.Forms.Label();
            this.lblSupplierName3 = new System.Windows.Forms.Label();
            this.lblNumSupplier3 = new System.Windows.Forms.Label();
            this.txtSupplierName3 = new System.Windows.Forms.TextBox();
            this.txtNumSupplier3 = new System.Windows.Forms.TextBox();
            this.fromDateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.toDateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.btnSearch3 = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnView3 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.szSupplierFullNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateCreationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montantTotalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.allStoryInputGoodsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.allInputGoodsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.libelleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.puDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uniteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montantDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allStoryInputGoodsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allInputGoodsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.BackColor = System.Drawing.Color.Silver;
            this.tableLayoutPanel11.ColumnCount = 7;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.Controls.Add(this.lblFormDate1, 1, 0);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel11.TabIndex = 0;
            // 
            // lblFormDate1
            // 
            this.lblFormDate1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblFormDate1.AutoSize = true;
            this.lblFormDate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFormDate1.Location = new System.Drawing.Point(3, 24);
            this.lblFormDate1.Name = "lblFormDate1";
            this.lblFormDate1.Size = new System.Drawing.Size(44, 52);
            this.lblFormDate1.TabIndex = 0;
            this.lblFormDate1.Text = "Manomboka amin\'ny";
            // 
            // lblToDate1
            // 
            this.lblToDate1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblToDate1.AutoSize = true;
            this.lblToDate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToDate1.Location = new System.Drawing.Point(306, 114);
            this.lblToDate1.Name = "lblToDate1";
            this.lblToDate1.Size = new System.Drawing.Size(89, 13);
            this.lblToDate1.TabIndex = 1;
            this.lblToDate1.Text = "ka hatramin\'ny";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView2, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(778, 484);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Silver;
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.lblFromDate3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblToDate3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblSupplierName3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblNumSupplier3, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtSupplierName3, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtNumSupplier3, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.fromDateTimePicker3, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.toDateTimePicker3, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnSearch3, 5, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(772, 74);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // lblFromDate3
            // 
            this.lblFromDate3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblFromDate3.AutoSize = true;
            this.lblFromDate3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFromDate3.Location = new System.Drawing.Point(3, 5);
            this.lblFromDate3.Name = "lblFromDate3";
            this.lblFromDate3.Size = new System.Drawing.Size(79, 26);
            this.lblFromDate3.TabIndex = 0;
            this.lblFromDate3.Text = "Manomboka amin\'ny";
            // 
            // lblToDate3
            // 
            this.lblToDate3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblToDate3.AutoSize = true;
            this.lblToDate3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToDate3.Location = new System.Drawing.Point(289, 12);
            this.lblToDate3.Name = "lblToDate3";
            this.lblToDate3.Size = new System.Drawing.Size(89, 13);
            this.lblToDate3.TabIndex = 1;
            this.lblToDate3.Text = "ka hatramin\'ny";
            // 
            // lblSupplierName3
            // 
            this.lblSupplierName3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSupplierName3.AutoSize = true;
            this.lblSupplierName3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSupplierName3.Location = new System.Drawing.Point(3, 49);
            this.lblSupplierName3.Name = "lblSupplierName3";
            this.lblSupplierName3.Size = new System.Drawing.Size(72, 13);
            this.lblSupplierName3.TabIndex = 2;
            this.lblSupplierName3.Text = "Fournisseur";
            // 
            // lblNumSupplier3
            // 
            this.lblNumSupplier3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblNumSupplier3.AutoSize = true;
            this.lblNumSupplier3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumSupplier3.Location = new System.Drawing.Point(289, 49);
            this.lblNumSupplier3.Name = "lblNumSupplier3";
            this.lblNumSupplier3.Size = new System.Drawing.Size(50, 13);
            this.lblNumSupplier3.TabIndex = 3;
            this.lblNumSupplier3.Text = "Numero";
            // 
            // txtSupplierName3
            // 
            this.txtSupplierName3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtSupplierName3.Location = new System.Drawing.Point(103, 45);
            this.txtSupplierName3.Name = "txtSupplierName3";
            this.txtSupplierName3.Size = new System.Drawing.Size(180, 20);
            this.txtSupplierName3.TabIndex = 5;
            // 
            // txtNumSupplier3
            // 
            this.txtNumSupplier3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtNumSupplier3.Location = new System.Drawing.Point(389, 45);
            this.txtNumSupplier3.Name = "txtNumSupplier3";
            this.txtNumSupplier3.Size = new System.Drawing.Size(180, 20);
            this.txtNumSupplier3.TabIndex = 4;
            // 
            // fromDateTimePicker3
            // 
            this.fromDateTimePicker3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.fromDateTimePicker3.Location = new System.Drawing.Point(103, 8);
            this.fromDateTimePicker3.Name = "fromDateTimePicker3";
            this.fromDateTimePicker3.Size = new System.Drawing.Size(180, 20);
            this.fromDateTimePicker3.TabIndex = 6;
            // 
            // toDateTimePicker3
            // 
            this.toDateTimePicker3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.toDateTimePicker3.Location = new System.Drawing.Point(389, 8);
            this.toDateTimePicker3.Name = "toDateTimePicker3";
            this.toDateTimePicker3.Size = new System.Drawing.Size(180, 20);
            this.toDateTimePicker3.TabIndex = 7;
            // 
            // btnSearch3
            // 
            this.btnSearch3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSearch3.Image = global::SupplyManagementLiteMk1.Properties.Resources.iconGreen_tiny;
            this.btnSearch3.Location = new System.Drawing.Point(675, 3);
            this.btnSearch3.Name = "btnSearch3";
            this.btnSearch3.Size = new System.Drawing.Size(34, 31);
            this.btnSearch3.TabIndex = 8;
            this.btnSearch3.UseVisualStyleBackColor = true;
            this.btnSearch3.Click += new System.EventHandler(this.btnSearch3_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Silver;
            this.flowLayoutPanel1.Controls.Add(this.btnView3);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 444);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(778, 40);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // btnView3
            // 
            this.btnView3.BackColor = System.Drawing.Color.Gray;
            this.btnView3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnView3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView3.Image = global::SupplyManagementLiteMk1.Properties.Resources.iconGreen_tiny;
            this.btnView3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnView3.Location = new System.Drawing.Point(3, 3);
            this.btnView3.Name = "btnView3";
            this.btnView3.Size = new System.Drawing.Size(150, 30);
            this.btnView3.TabIndex = 3;
            this.btnView3.Text = "Bon d\'Entrée (BE)";
            this.btnView3.UseVisualStyleBackColor = false;
            this.btnView3.Click += new System.EventHandler(this.btnView3_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.szSupplierFullNameDataGridViewTextBoxColumn,
            this.dateCreationDataGridViewTextBoxColumn,
            this.numeroDataGridViewTextBoxColumn,
            this.montantTotalDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.allStoryInputGoodsBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 83);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(772, 176);
            this.dataGridView1.TabIndex = 5;
            // 
            // szSupplierFullNameDataGridViewTextBoxColumn
            // 
            this.szSupplierFullNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.szSupplierFullNameDataGridViewTextBoxColumn.DataPropertyName = "SzSupplierFullName";
            this.szSupplierFullNameDataGridViewTextBoxColumn.HeaderText = "Fournisseur";
            this.szSupplierFullNameDataGridViewTextBoxColumn.Name = "szSupplierFullNameDataGridViewTextBoxColumn";
            this.szSupplierFullNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateCreationDataGridViewTextBoxColumn
            // 
            this.dateCreationDataGridViewTextBoxColumn.DataPropertyName = "DateCreation";
            this.dateCreationDataGridViewTextBoxColumn.HeaderText = "Date de création";
            this.dateCreationDataGridViewTextBoxColumn.Name = "dateCreationDataGridViewTextBoxColumn";
            this.dateCreationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // numeroDataGridViewTextBoxColumn
            // 
            this.numeroDataGridViewTextBoxColumn.DataPropertyName = "Numero";
            this.numeroDataGridViewTextBoxColumn.HeaderText = "Numero BE";
            this.numeroDataGridViewTextBoxColumn.Name = "numeroDataGridViewTextBoxColumn";
            this.numeroDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // montantTotalDataGridViewTextBoxColumn
            // 
            this.montantTotalDataGridViewTextBoxColumn.DataPropertyName = "MontantTotal";
            this.montantTotalDataGridViewTextBoxColumn.HeaderText = "Montant total (Ar)";
            this.montantTotalDataGridViewTextBoxColumn.Name = "montantTotalDataGridViewTextBoxColumn";
            this.montantTotalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // allStoryInputGoodsBindingSource
            // 
            this.allStoryInputGoodsBindingSource.DataSource = typeof(SupplyManagementLiteMk1.Datasource.StoryInputGoods);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.libelleDataGridViewTextBoxColumn,
            this.qteDataGridViewTextBoxColumn,
            this.puDataGridViewTextBoxColumn,
            this.uniteDataGridViewTextBoxColumn,
            this.montantDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.allInputGoodsBindingSource;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 265);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.Size = new System.Drawing.Size(772, 176);
            this.dataGridView2.TabIndex = 6;
            // 
            // allInputGoodsBindingSource
            // 
            this.allInputGoodsBindingSource.DataMember = "Goods";
            this.allInputGoodsBindingSource.DataSource = this.allStoryInputGoodsBindingSource;
            // 
            // libelleDataGridViewTextBoxColumn
            // 
            this.libelleDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.libelleDataGridViewTextBoxColumn.DataPropertyName = "Libelle";
            this.libelleDataGridViewTextBoxColumn.HeaderText = "Karazana";
            this.libelleDataGridViewTextBoxColumn.Name = "libelleDataGridViewTextBoxColumn";
            this.libelleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // qteDataGridViewTextBoxColumn
            // 
            this.qteDataGridViewTextBoxColumn.DataPropertyName = "Qte";
            this.qteDataGridViewTextBoxColumn.HeaderText = "Fatrany";
            this.qteDataGridViewTextBoxColumn.Name = "qteDataGridViewTextBoxColumn";
            this.qteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // puDataGridViewTextBoxColumn
            // 
            this.puDataGridViewTextBoxColumn.DataPropertyName = "Pu";
            this.puDataGridViewTextBoxColumn.HeaderText = "PU Achat (Ar)";
            this.puDataGridViewTextBoxColumn.Name = "puDataGridViewTextBoxColumn";
            this.puDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // uniteDataGridViewTextBoxColumn
            // 
            this.uniteDataGridViewTextBoxColumn.DataPropertyName = "Unite";
            this.uniteDataGridViewTextBoxColumn.HeaderText = "Unité";
            this.uniteDataGridViewTextBoxColumn.Name = "uniteDataGridViewTextBoxColumn";
            this.uniteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // montantDataGridViewTextBoxColumn
            // 
            this.montantDataGridViewTextBoxColumn.DataPropertyName = "Montant";
            this.montantDataGridViewTextBoxColumn.HeaderText = "Montant (Ar)";
            this.montantDataGridViewTextBoxColumn.Name = "montantDataGridViewTextBoxColumn";
            this.montantDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // CtrlAllStoryInputGoods
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "CtrlAllStoryInputGoods";
            this.Size = new System.Drawing.Size(778, 484);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allStoryInputGoodsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allInputGoodsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label lblFormDate1;
        private System.Windows.Forms.Label lblToDate1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblFromDate3;
        private System.Windows.Forms.Label lblToDate3;
        private System.Windows.Forms.Label lblSupplierName3;
        private System.Windows.Forms.Label lblNumSupplier3;
        private System.Windows.Forms.TextBox txtSupplierName3;
        private System.Windows.Forms.TextBox txtNumSupplier3;
        private System.Windows.Forms.DateTimePicker fromDateTimePicker3;
        private System.Windows.Forms.DateTimePicker toDateTimePicker3;
        private System.Windows.Forms.Button btnSearch3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnView3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.BindingSource allStoryInputGoodsBindingSource;
        private System.Windows.Forms.BindingSource allInputGoodsBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn szSupplierFullNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateCreationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn montantTotalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn libelleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn qteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn puDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uniteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn montantDataGridViewTextBoxColumn;
    }
}
