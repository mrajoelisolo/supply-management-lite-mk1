using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ORMFramework.AppUtil;

namespace SupplyManagementLiteMk1.StoryManagement
{
    public partial class FormStoryManagement : Form
    {
        public FormStoryManagement()
        {
            InitializeComponent();

            ctrlClearStories1.Observers.Add(ctrlAllStoryInputGoods1);
            ctrlClearStories1.Observers.Add(ctrlAllStoryOutputGoods1);
            ctrlClearStories1.Observers.Add(ctrlStoryInputGoods1);
            ctrlClearStories1.Observers.Add(ctrlStoryOutputGoods1);
        }
    }
}