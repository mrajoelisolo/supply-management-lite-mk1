using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.StoryManagement.Util;
using SupplyManagementLiteMk1.OutputManagement.Cmd;

namespace SupplyManagementLiteMk1.StoryManagement
{
    public partial class CtrlAllStoryOutputGoods : UserControl, IObserver
    {
        public CtrlAllStoryOutputGoods()
        {
            InitializeComponent();
        }        

        private void btnSearch4_Click(object sender, EventArgs e)
        {
            Notify("");
        }        

        private void btnView4_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdFacturateOutput(allStoryOutputGoodsBindingSource);
            Invoker.Instance.Invoke(cmd);
        }

        public void Notify(string msg)
        {
            bool bShowAll = false;
            DateTime fromDate = fromDateTimePicker4.Value;
            DateTime toDate = toDateTimePicker4.Value;

            allStoryOutputGoodsBindingSource.DataSource = QueryBuilder.Instance.getCompletedOutputStories(true, fromDate, toDate, txtTiersName4.Text, txtNumTiers4.Text, bShowAll);
        }
    }
}
