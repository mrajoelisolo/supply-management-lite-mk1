using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.StoryManagement.Cmd;
using SupplyManagementLiteMk1.StoryManagement.Util;

namespace SupplyManagementLiteMk1.StoryManagement
{
    public partial class CtrlStoryOutputGoods : UserControl, IObserver
    {
        public CtrlStoryOutputGoods()
        {
            InitializeComponent();
        }

        private void btnSearch2_Click(object sender, EventArgs e)
        {
            Notify("");
        }        

        private void btnPay2_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdPayOutputGoods(storyOutputGoodsBindingSource, this);
            Invoker.Instance.Invoke(cmd);
        }        

        private void btnFacturate2_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdFacturateOutputPayment(storyOutputGoodsBindingSource);
            Invoker.Instance.Invoke(cmd);
        }

        public void Notify(string msg)
        {
            bool bShowAll = false;
            DateTime fromDate = fromDateTimePicker2.Value;
            DateTime toDate = toDateTimePicker2.Value;

            storyOutputGoodsBindingSource.DataSource = QueryBuilder.Instance.getOutputStories(chkDate2.Checked, fromDate, toDate, txtTiersName2.Text, txtTiersNum2.Text, bShowAll);
        }
    }
}
