﻿namespace SupplyManagementLiteMk1.StoryManagement
{
    partial class CtrlAllStoryOutputGoods
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnView4 = new System.Windows.Forms.Button();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFromDate4 = new System.Windows.Forms.Label();
            this.lblToDate4 = new System.Windows.Forms.Label();
            this.fromDateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.toDateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.lblTiersName4 = new System.Windows.Forms.Label();
            this.lblNumTiers4 = new System.Windows.Forms.Label();
            this.txtTiersName4 = new System.Windows.Forms.TextBox();
            this.txtNumTiers4 = new System.Windows.Forms.TextBox();
            this.btnSearch4 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.allStoryOutputGoodsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.allOutputGoodsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.szTiersFullNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateCreationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montantTotalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.libelleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.puDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uniteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montantDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allStoryOutputGoodsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allOutputGoodsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel15, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView2, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(778, 484);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Silver;
            this.flowLayoutPanel1.Controls.Add(this.btnView4);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 444);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(778, 40);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // btnView4
            // 
            this.btnView4.BackColor = System.Drawing.Color.Gray;
            this.btnView4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnView4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView4.Image = global::SupplyManagementLiteMk1.Properties.Resources.iconGreen_tiny;
            this.btnView4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnView4.Location = new System.Drawing.Point(3, 3);
            this.btnView4.Name = "btnView4";
            this.btnView4.Size = new System.Drawing.Size(150, 30);
            this.btnView4.TabIndex = 3;
            this.btnView4.Text = "Faktiora";
            this.btnView4.UseVisualStyleBackColor = false;
            this.btnView4.Click += new System.EventHandler(this.btnView4_Click);
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.BackColor = System.Drawing.Color.Silver;
            this.tableLayoutPanel15.ColumnCount = 6;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.Controls.Add(this.lblFromDate4, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.lblToDate4, 2, 0);
            this.tableLayoutPanel15.Controls.Add(this.fromDateTimePicker4, 1, 0);
            this.tableLayoutPanel15.Controls.Add(this.toDateTimePicker4, 3, 0);
            this.tableLayoutPanel15.Controls.Add(this.lblTiersName4, 0, 1);
            this.tableLayoutPanel15.Controls.Add(this.lblNumTiers4, 2, 1);
            this.tableLayoutPanel15.Controls.Add(this.txtTiersName4, 1, 1);
            this.tableLayoutPanel15.Controls.Add(this.txtNumTiers4, 3, 1);
            this.tableLayoutPanel15.Controls.Add(this.btnSearch4, 5, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 2;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(772, 74);
            this.tableLayoutPanel15.TabIndex = 1;
            // 
            // lblFromDate4
            // 
            this.lblFromDate4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblFromDate4.AutoSize = true;
            this.lblFromDate4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFromDate4.Location = new System.Drawing.Point(3, 5);
            this.lblFromDate4.Name = "lblFromDate4";
            this.lblFromDate4.Size = new System.Drawing.Size(79, 26);
            this.lblFromDate4.TabIndex = 1;
            this.lblFromDate4.Text = "Manomboka amin\'ny";
            // 
            // lblToDate4
            // 
            this.lblToDate4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblToDate4.AutoSize = true;
            this.lblToDate4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToDate4.Location = new System.Drawing.Point(289, 12);
            this.lblToDate4.Name = "lblToDate4";
            this.lblToDate4.Size = new System.Drawing.Size(89, 13);
            this.lblToDate4.TabIndex = 2;
            this.lblToDate4.Text = "ka hatramin\'ny";
            // 
            // fromDateTimePicker4
            // 
            this.fromDateTimePicker4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.fromDateTimePicker4.Location = new System.Drawing.Point(103, 8);
            this.fromDateTimePicker4.Name = "fromDateTimePicker4";
            this.fromDateTimePicker4.Size = new System.Drawing.Size(180, 20);
            this.fromDateTimePicker4.TabIndex = 3;
            // 
            // toDateTimePicker4
            // 
            this.toDateTimePicker4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.toDateTimePicker4.Location = new System.Drawing.Point(389, 8);
            this.toDateTimePicker4.Name = "toDateTimePicker4";
            this.toDateTimePicker4.Size = new System.Drawing.Size(180, 20);
            this.toDateTimePicker4.TabIndex = 4;
            // 
            // lblTiersName4
            // 
            this.lblTiersName4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTiersName4.AutoSize = true;
            this.lblTiersName4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTiersName4.Location = new System.Drawing.Point(3, 42);
            this.lblTiersName4.Name = "lblTiersName4";
            this.lblTiersName4.Size = new System.Drawing.Size(67, 26);
            this.lblTiersName4.TabIndex = 5;
            this.lblTiersName4.Text = "Anaran\'ny client";
            // 
            // lblNumTiers4
            // 
            this.lblNumTiers4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblNumTiers4.AutoSize = true;
            this.lblNumTiers4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumTiers4.Location = new System.Drawing.Point(289, 49);
            this.lblNumTiers4.Name = "lblNumTiers4";
            this.lblNumTiers4.Size = new System.Drawing.Size(50, 13);
            this.lblNumTiers4.TabIndex = 6;
            this.lblNumTiers4.Text = "Numero";
            // 
            // txtTiersName4
            // 
            this.txtTiersName4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtTiersName4.Location = new System.Drawing.Point(103, 45);
            this.txtTiersName4.Name = "txtTiersName4";
            this.txtTiersName4.Size = new System.Drawing.Size(180, 20);
            this.txtTiersName4.TabIndex = 7;
            // 
            // txtNumTiers4
            // 
            this.txtNumTiers4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtNumTiers4.Location = new System.Drawing.Point(389, 45);
            this.txtNumTiers4.Name = "txtNumTiers4";
            this.txtNumTiers4.Size = new System.Drawing.Size(180, 20);
            this.txtNumTiers4.TabIndex = 8;
            // 
            // btnSearch4
            // 
            this.btnSearch4.Image = global::SupplyManagementLiteMk1.Properties.Resources.iconGreen_tiny;
            this.btnSearch4.Location = new System.Drawing.Point(675, 3);
            this.btnSearch4.Name = "btnSearch4";
            this.btnSearch4.Size = new System.Drawing.Size(34, 31);
            this.btnSearch4.TabIndex = 9;
            this.btnSearch4.UseVisualStyleBackColor = true;
            this.btnSearch4.Click += new System.EventHandler(this.btnSearch4_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.szTiersFullNameDataGridViewTextBoxColumn,
            this.dateCreationDataGridViewTextBoxColumn,
            this.numeroDataGridViewTextBoxColumn,
            this.montantTotalDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.allStoryOutputGoodsBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 83);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(772, 176);
            this.dataGridView1.TabIndex = 6;
            // 
            // allStoryOutputGoodsBindingSource
            // 
            this.allStoryOutputGoodsBindingSource.DataSource = typeof(SupplyManagementLiteMk1.Datasource.StoryOutputGoods);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.libelleDataGridViewTextBoxColumn,
            this.qteDataGridViewTextBoxColumn,
            this.puDataGridViewTextBoxColumn,
            this.uniteDataGridViewTextBoxColumn,
            this.montantDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.allOutputGoodsBindingSource;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 265);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.Size = new System.Drawing.Size(772, 176);
            this.dataGridView2.TabIndex = 7;
            // 
            // allOutputGoodsBindingSource
            // 
            this.allOutputGoodsBindingSource.DataMember = "Goods";
            this.allOutputGoodsBindingSource.DataSource = this.allStoryOutputGoodsBindingSource;
            // 
            // szTiersFullNameDataGridViewTextBoxColumn
            // 
            this.szTiersFullNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.szTiersFullNameDataGridViewTextBoxColumn.DataPropertyName = "SzTiersFullName";
            this.szTiersFullNameDataGridViewTextBoxColumn.HeaderText = "Fournisseur";
            this.szTiersFullNameDataGridViewTextBoxColumn.Name = "szTiersFullNameDataGridViewTextBoxColumn";
            this.szTiersFullNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateCreationDataGridViewTextBoxColumn
            // 
            this.dateCreationDataGridViewTextBoxColumn.DataPropertyName = "DateCreation";
            this.dateCreationDataGridViewTextBoxColumn.HeaderText = "Date de création";
            this.dateCreationDataGridViewTextBoxColumn.Name = "dateCreationDataGridViewTextBoxColumn";
            this.dateCreationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // numeroDataGridViewTextBoxColumn
            // 
            this.numeroDataGridViewTextBoxColumn.DataPropertyName = "Numero";
            this.numeroDataGridViewTextBoxColumn.HeaderText = "Numero facture";
            this.numeroDataGridViewTextBoxColumn.Name = "numeroDataGridViewTextBoxColumn";
            this.numeroDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // montantTotalDataGridViewTextBoxColumn
            // 
            this.montantTotalDataGridViewTextBoxColumn.DataPropertyName = "MontantTotal";
            this.montantTotalDataGridViewTextBoxColumn.HeaderText = "Montant total (Ar)";
            this.montantTotalDataGridViewTextBoxColumn.Name = "montantTotalDataGridViewTextBoxColumn";
            this.montantTotalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // libelleDataGridViewTextBoxColumn
            // 
            this.libelleDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.libelleDataGridViewTextBoxColumn.DataPropertyName = "Libelle";
            this.libelleDataGridViewTextBoxColumn.HeaderText = "Karazana";
            this.libelleDataGridViewTextBoxColumn.Name = "libelleDataGridViewTextBoxColumn";
            this.libelleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // qteDataGridViewTextBoxColumn
            // 
            this.qteDataGridViewTextBoxColumn.DataPropertyName = "Qte";
            this.qteDataGridViewTextBoxColumn.HeaderText = "Qte";
            this.qteDataGridViewTextBoxColumn.Name = "qteDataGridViewTextBoxColumn";
            this.qteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // puDataGridViewTextBoxColumn
            // 
            this.puDataGridViewTextBoxColumn.DataPropertyName = "Pu";
            this.puDataGridViewTextBoxColumn.HeaderText = "PU Vente (Ar)";
            this.puDataGridViewTextBoxColumn.Name = "puDataGridViewTextBoxColumn";
            this.puDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // uniteDataGridViewTextBoxColumn
            // 
            this.uniteDataGridViewTextBoxColumn.DataPropertyName = "Unite";
            this.uniteDataGridViewTextBoxColumn.HeaderText = "Unité";
            this.uniteDataGridViewTextBoxColumn.Name = "uniteDataGridViewTextBoxColumn";
            this.uniteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // montantDataGridViewTextBoxColumn
            // 
            this.montantDataGridViewTextBoxColumn.DataPropertyName = "Montant";
            this.montantDataGridViewTextBoxColumn.HeaderText = "Montant (Ar)";
            this.montantDataGridViewTextBoxColumn.Name = "montantDataGridViewTextBoxColumn";
            this.montantDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // CtrlAllStoryOutputGoods
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "CtrlAllStoryOutputGoods";
            this.Size = new System.Drawing.Size(778, 484);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allStoryOutputGoodsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allOutputGoodsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Label lblFromDate4;
        private System.Windows.Forms.Label lblToDate4;
        private System.Windows.Forms.DateTimePicker fromDateTimePicker4;
        private System.Windows.Forms.DateTimePicker toDateTimePicker4;
        private System.Windows.Forms.Label lblTiersName4;
        private System.Windows.Forms.Label lblNumTiers4;
        private System.Windows.Forms.TextBox txtTiersName4;
        private System.Windows.Forms.TextBox txtNumTiers4;
        private System.Windows.Forms.Button btnSearch4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnView4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.BindingSource allStoryOutputGoodsBindingSource;
        private System.Windows.Forms.BindingSource allOutputGoodsBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn szTiersFullNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateCreationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn montantTotalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn libelleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn qteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn puDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uniteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn montantDataGridViewTextBoxColumn;
    }
}
