﻿namespace SupplyManagementLiteMk1.StoryManagement
{
    partial class CtrlClearStories
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.lblSecretCode = new System.Windows.Forms.Label();
            this.txtPwd3 = new System.Windows.Forms.TextBox();
            this.btnOK3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.btnClearDatabase3 = new System.Windows.Forms.Button();
            this.btnClearStory3 = new System.Windows.Forms.Button();
            this.pictureBoxLock3 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLock3)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel9, 0, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(778, 484);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.BackColor = System.Drawing.Color.LightGray;
            this.tableLayoutPanel8.ColumnCount = 4;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 402F));
            this.tableLayoutPanel8.Controls.Add(this.lblSecretCode, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.txtPwd3, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.btnOK3, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.label1, 3, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(778, 40);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // lblSecretCode
            // 
            this.lblSecretCode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSecretCode.AutoSize = true;
            this.lblSecretCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecretCode.Location = new System.Drawing.Point(3, 13);
            this.lblSecretCode.Name = "lblSecretCode";
            this.lblSecretCode.Size = new System.Drawing.Size(79, 13);
            this.lblSecretCode.TabIndex = 0;
            this.lblSecretCode.Text = "Teny miafina";
            // 
            // txtPwd3
            // 
            this.txtPwd3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtPwd3.Location = new System.Drawing.Point(88, 10);
            this.txtPwd3.Name = "txtPwd3";
            this.txtPwd3.PasswordChar = '*';
            this.txtPwd3.Size = new System.Drawing.Size(219, 20);
            this.txtPwd3.TabIndex = 1;
            // 
            // btnOK3
            // 
            this.btnOK3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnOK3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnOK3.Location = new System.Drawing.Point(313, 8);
            this.btnOK3.Name = "btnOK3";
            this.btnOK3.Size = new System.Drawing.Size(68, 23);
            this.btnOK3.TabIndex = 2;
            this.btnOK3.Text = "Vahana";
            this.btnOK3.UseVisualStyleBackColor = true;
            this.btnOK3.Click += new System.EventHandler(this.btnOK3_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(387, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(370, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Tandremo : Raha voafafa ny lisitra dia tsy afaka averina intsony";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.btnClearDatabase3, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.btnClearStory3, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.pictureBoxLock3, 0, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 40);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 3;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(778, 444);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // btnClearDatabase3
            // 
            this.btnClearDatabase3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClearDatabase3.BackColor = System.Drawing.Color.Silver;
            this.btnClearDatabase3.Enabled = false;
            this.btnClearDatabase3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearDatabase3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearDatabase3.Image = global::SupplyManagementLiteMk1.Properties.Resources.iconRed_tiny;
            this.btnClearDatabase3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClearDatabase3.Location = new System.Drawing.Point(189, 355);
            this.btnClearDatabase3.Name = "btnClearDatabase3";
            this.btnClearDatabase3.Size = new System.Drawing.Size(400, 30);
            this.btnClearDatabase3.TabIndex = 3;
            this.btnClearDatabase3.Text = "Fafana ny lisitra rehetra";
            this.btnClearDatabase3.UseVisualStyleBackColor = false;
            this.btnClearDatabase3.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnClearStory3
            // 
            this.btnClearStory3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClearStory3.BackColor = System.Drawing.Color.Silver;
            this.btnClearStory3.Enabled = false;
            this.btnClearStory3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearStory3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearStory3.Image = global::SupplyManagementLiteMk1.Properties.Resources.iconRed_tiny;
            this.btnClearStory3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClearStory3.Location = new System.Drawing.Point(189, 207);
            this.btnClearStory3.Name = "btnClearStory3";
            this.btnClearStory3.Size = new System.Drawing.Size(400, 30);
            this.btnClearStory3.TabIndex = 1;
            this.btnClearStory3.Text = "Fafana ny lisitrin\'ny faktiora/bon d\'entrée";
            this.btnClearStory3.UseVisualStyleBackColor = false;
            this.btnClearStory3.Click += new System.EventHandler(this.btnClearStory3_Click);
            // 
            // pictureBoxLock3
            // 
            this.pictureBoxLock3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBoxLock3.Image = global::SupplyManagementLiteMk1.Properties.Resources.Lock_Locked;
            this.pictureBoxLock3.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxLock3.Name = "pictureBoxLock3";
            this.pictureBoxLock3.Size = new System.Drawing.Size(155, 142);
            this.pictureBoxLock3.TabIndex = 2;
            this.pictureBoxLock3.TabStop = false;
            // 
            // CtrlClearStories
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel7);
            this.Name = "CtrlClearStories";
            this.Size = new System.Drawing.Size(778, 484);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLock3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label lblSecretCode;
        private System.Windows.Forms.TextBox txtPwd3;
        private System.Windows.Forms.Button btnOK3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button btnClearStory3;
        private System.Windows.Forms.PictureBox pictureBoxLock3;
        private System.Windows.Forms.Button btnClearDatabase3;
    }
}
