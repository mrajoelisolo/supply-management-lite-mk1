using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SupplyManagementLiteMk1.StoryManagement
{
    public partial class FormClearDatabase : Form
    {
        public FormClearDatabase()
        {
            InitializeComponent();

            CenterToParent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Tena tianao fafana ve ?", "Fafana", MessageBoxButtons.YesNo, MessageBoxIcon.Hand);
            if (res == DialogResult.No) return;

            DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            btnOK.Enabled = checkBox1.Checked;
        }
    }
}