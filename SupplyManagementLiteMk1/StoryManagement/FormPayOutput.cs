using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;

namespace SupplyManagementLiteMk1.StoryManagement
{
    public partial class FormPayOutput : Form
    {
        private StoryOutputGoods m_Story;

        public FormPayOutput(StoryOutputGoods story)
        {
            InitializeComponent();

            CenterToParent();

            m_Story = story;
            outputPayBindingSource.DataSource = story.OutputPaiments;

            txtRemainingAmount.Text = "" + story.RemainingAmount;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnValidate_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
                m_Story.LastVersedAmount = m_Story.RemainingAmount;
            else if (radioButton2.Checked)
            {
                float amount = GetSpecifiedAmount();

                if ((amount <= 0) || (amount > GetTotalAmount()))
                {
                    MessageBox.Show("Tsy mety ny voasoratra", "Tsy mety", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                m_Story.LastVersedAmount = amount;
            }

            DialogResult = DialogResult.OK;
        }

        private float GetTotalAmount()
        {
            float res = 0f;

            try
            {
                res = float.Parse(txtRemainingAmount.Text);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }

            return res;
        }

        private float GetSpecifiedAmount()
        {
            float res = 0f;

            try
            {
                res = float.Parse(txtSpecifiedAmount.Text);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }

            return res;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            txtSpecifiedAmount.Enabled = radioButton2.Checked;
        }
    }
}