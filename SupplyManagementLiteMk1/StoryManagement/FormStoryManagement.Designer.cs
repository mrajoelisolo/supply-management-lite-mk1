﻿namespace SupplyManagementLiteMk1.StoryManagement
{
    partial class FormStoryManagement
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormStoryManagement));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageInputStory1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTitle1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.ctrlStoryInputGoods1 = new SupplyManagementLiteMk1.StoryManagement.CtrlStoryInputGoods();
            this.tabPageOutputStory2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTitle2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.ctrlStoryOutputGoods1 = new SupplyManagementLiteMk1.StoryManagement.CtrlStoryOutputGoods();
            this.tabPageAllInputStory3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.LblTitle3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.ctrlAllStoryInputGoods1 = new SupplyManagementLiteMk1.StoryManagement.CtrlAllStoryInputGoods();
            this.tabPageAllOutputStory4 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTitle4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.ctrlAllStoryOutputGoods1 = new SupplyManagementLiteMk1.StoryManagement.CtrlAllStoryOutputGoods();
            this.tabPageClearStories5 = new System.Windows.Forms.TabPage();
            this.ctrlClearStories1 = new SupplyManagementLiteMk1.StoryManagement.CtrlClearStories();
            this.tabControl1.SuspendLayout();
            this.tabPageInputStory1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tabPageOutputStory2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tabPageAllInputStory3.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tabPageAllOutputStory4.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tabPageClearStories5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageInputStory1);
            this.tabControl1.Controls.Add(this.tabPageOutputStory2);
            this.tabControl1.Controls.Add(this.tabPageAllInputStory3);
            this.tabControl1.Controls.Add(this.tabPageAllOutputStory4);
            this.tabControl1.Controls.Add(this.tabPageClearStories5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(792, 566);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPageInputStory1
            // 
            this.tabPageInputStory1.Controls.Add(this.tableLayoutPanel1);
            this.tabPageInputStory1.Location = new System.Drawing.Point(4, 22);
            this.tabPageInputStory1.Name = "tabPageInputStory1";
            this.tabPageInputStory1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageInputStory1.Size = new System.Drawing.Size(784, 540);
            this.tabPageInputStory1.TabIndex = 0;
            this.tabPageInputStory1.Text = "Bon d\'Entrée mbola tsy voaloa tanteraka";
            this.tabPageInputStory1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Navy;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lblTitle1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(778, 534);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblTitle1
            // 
            this.lblTitle1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTitle1.AutoSize = true;
            this.lblTitle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lblTitle1.Location = new System.Drawing.Point(3, 8);
            this.lblTitle1.Name = "lblTitle1";
            this.lblTitle1.Size = new System.Drawing.Size(554, 33);
            this.lblTitle1.TabIndex = 0;
            this.lblTitle1.Text = "Bon d\'Entrée mbola tsy voaloha tanteraka";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.ctrlStoryInputGoods1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 50);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 484F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(778, 484);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // ctrlStoryInputGoods1
            // 
            this.ctrlStoryInputGoods1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlStoryInputGoods1.Location = new System.Drawing.Point(3, 3);
            this.ctrlStoryInputGoods1.Name = "ctrlStoryInputGoods1";
            this.ctrlStoryInputGoods1.Size = new System.Drawing.Size(772, 478);
            this.ctrlStoryInputGoods1.TabIndex = 0;
            // 
            // tabPageOutputStory2
            // 
            this.tabPageOutputStory2.Controls.Add(this.tableLayoutPanel5);
            this.tabPageOutputStory2.Location = new System.Drawing.Point(4, 22);
            this.tabPageOutputStory2.Name = "tabPageOutputStory2";
            this.tabPageOutputStory2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOutputStory2.Size = new System.Drawing.Size(784, 540);
            this.tabPageOutputStory2.TabIndex = 1;
            this.tabPageOutputStory2.Text = "Faktiora mbola tsy voaloa tanteraka";
            this.tabPageOutputStory2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.BackColor = System.Drawing.Color.Navy;
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.lblTitle2, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(778, 534);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // lblTitle2
            // 
            this.lblTitle2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTitle2.AutoSize = true;
            this.lblTitle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lblTitle2.Location = new System.Drawing.Point(3, 8);
            this.lblTitle2.Name = "lblTitle2";
            this.lblTitle2.Size = new System.Drawing.Size(478, 33);
            this.lblTitle2.TabIndex = 1;
            this.lblTitle2.Text = "Faktiora mbola tsy voaloa tanteraka";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.ctrlStoryOutputGoods1, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 50);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 484F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(778, 484);
            this.tableLayoutPanel6.TabIndex = 2;
            // 
            // ctrlStoryOutputGoods1
            // 
            this.ctrlStoryOutputGoods1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlStoryOutputGoods1.Location = new System.Drawing.Point(3, 3);
            this.ctrlStoryOutputGoods1.Name = "ctrlStoryOutputGoods1";
            this.ctrlStoryOutputGoods1.Size = new System.Drawing.Size(772, 478);
            this.ctrlStoryOutputGoods1.TabIndex = 0;
            // 
            // tabPageAllInputStory3
            // 
            this.tabPageAllInputStory3.Controls.Add(this.tableLayoutPanel9);
            this.tabPageAllInputStory3.Location = new System.Drawing.Point(4, 22);
            this.tabPageAllInputStory3.Name = "tabPageAllInputStory3";
            this.tabPageAllInputStory3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAllInputStory3.Size = new System.Drawing.Size(784, 540);
            this.tabPageAllInputStory3.TabIndex = 2;
            this.tabPageAllInputStory3.Text = "Bon d\'entrée efa voaloa tanteraka";
            this.tabPageAllInputStory3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.BackColor = System.Drawing.Color.Navy;
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.LblTitle3, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel10, 0, 1);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(778, 534);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // LblTitle3
            // 
            this.LblTitle3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LblTitle3.AutoSize = true;
            this.LblTitle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTitle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.LblTitle3.Location = new System.Drawing.Point(3, 8);
            this.LblTitle3.Name = "LblTitle3";
            this.LblTitle3.Size = new System.Drawing.Size(468, 33);
            this.LblTitle3.TabIndex = 2;
            this.LblTitle3.Text = "Bon d\'Entrée efa voaloha tanteraka";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.ctrlAllStoryInputGoods1, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 50);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 484F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(778, 484);
            this.tableLayoutPanel10.TabIndex = 3;
            // 
            // ctrlAllStoryInputGoods1
            // 
            this.ctrlAllStoryInputGoods1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlAllStoryInputGoods1.Location = new System.Drawing.Point(3, 3);
            this.ctrlAllStoryInputGoods1.Name = "ctrlAllStoryInputGoods1";
            this.ctrlAllStoryInputGoods1.Size = new System.Drawing.Size(772, 478);
            this.ctrlAllStoryInputGoods1.TabIndex = 0;
            // 
            // tabPageAllOutputStory4
            // 
            this.tabPageAllOutputStory4.Controls.Add(this.tableLayoutPanel13);
            this.tabPageAllOutputStory4.Location = new System.Drawing.Point(4, 22);
            this.tabPageAllOutputStory4.Name = "tabPageAllOutputStory4";
            this.tabPageAllOutputStory4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAllOutputStory4.Size = new System.Drawing.Size(784, 540);
            this.tabPageAllOutputStory4.TabIndex = 3;
            this.tabPageAllOutputStory4.Text = "Faktiora efa voaloa tanteraka";
            this.tabPageAllOutputStory4.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.BackColor = System.Drawing.Color.Navy;
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Controls.Add(this.lblTitle4, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel14, 0, 1);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(778, 534);
            this.tableLayoutPanel13.TabIndex = 0;
            // 
            // lblTitle4
            // 
            this.lblTitle4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTitle4.AutoSize = true;
            this.lblTitle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lblTitle4.Location = new System.Drawing.Point(3, 8);
            this.lblTitle4.Name = "lblTitle4";
            this.lblTitle4.Size = new System.Drawing.Size(408, 33);
            this.lblTitle4.TabIndex = 3;
            this.lblTitle4.Text = "Faktiora efa voaloha tanteraka";
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Controls.Add(this.ctrlAllStoryOutputGoods1, 0, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(0, 50);
            this.tableLayoutPanel14.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 484F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(778, 484);
            this.tableLayoutPanel14.TabIndex = 4;
            // 
            // ctrlAllStoryOutputGoods1
            // 
            this.ctrlAllStoryOutputGoods1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlAllStoryOutputGoods1.Location = new System.Drawing.Point(3, 3);
            this.ctrlAllStoryOutputGoods1.Name = "ctrlAllStoryOutputGoods1";
            this.ctrlAllStoryOutputGoods1.Size = new System.Drawing.Size(772, 478);
            this.ctrlAllStoryOutputGoods1.TabIndex = 0;
            // 
            // tabPageClearStories5
            // 
            this.tabPageClearStories5.Controls.Add(this.ctrlClearStories1);
            this.tabPageClearStories5.Location = new System.Drawing.Point(4, 22);
            this.tabPageClearStories5.Name = "tabPageClearStories5";
            this.tabPageClearStories5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageClearStories5.Size = new System.Drawing.Size(784, 540);
            this.tabPageClearStories5.TabIndex = 4;
            this.tabPageClearStories5.Text = "Fafana ny faktiora sy ny bon d\'entrée rehetra";
            this.tabPageClearStories5.UseVisualStyleBackColor = true;
            // 
            // ctrlClearStories1
            // 
            this.ctrlClearStories1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlClearStories1.Location = new System.Drawing.Point(3, 3);
            this.ctrlClearStories1.Name = "ctrlClearStories1";
            this.ctrlClearStories1.Size = new System.Drawing.Size(778, 534);
            this.ctrlClearStories1.TabIndex = 0;
            // 
            // FormStoryManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 566);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormStoryManagement";
            this.Text = "Fandehanam-bola miditra/mivoaka";
            this.tabControl1.ResumeLayout(false);
            this.tabPageInputStory1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tabPageOutputStory2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tabPageAllInputStory3.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tabPageAllOutputStory4.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tabPageClearStories5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageInputStory1;
        private System.Windows.Forms.TabPage tabPageOutputStory2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblTitle1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label lblTitle2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TabPage tabPageAllInputStory3;
        private System.Windows.Forms.TabPage tabPageAllOutputStory4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label LblTitle3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label lblTitle4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private CtrlStoryInputGoods ctrlStoryInputGoods1;
        private CtrlStoryOutputGoods ctrlStoryOutputGoods1;
        private CtrlAllStoryInputGoods ctrlAllStoryInputGoods1;
        private CtrlAllStoryOutputGoods ctrlAllStoryOutputGoods1;
        private System.Windows.Forms.TabPage tabPageClearStories5;
        private CtrlClearStories ctrlClearStories1;

    }
}