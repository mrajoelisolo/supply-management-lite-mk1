using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.StoryManagement.Util;
using SupplyManagementLiteMk1.InputManagement.Cmd;

namespace SupplyManagementLiteMk1.StoryManagement
{
    public partial class CtrlAllStoryInputGoods : UserControl, IObserver
    {
        public CtrlAllStoryInputGoods()
        {
            InitializeComponent();
        }

        private void btnSearch3_Click(object sender, EventArgs e)
        {
            Notify("");
        }        

        private void btnView3_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdFacturateInput(allStoryInputGoodsBindingSource);
            Invoker.Instance.Invoke(cmd);
        }

        public void Notify(string msg)
        {
            bool bShowAll = false;
            DateTime fromDate = fromDateTimePicker3.Value;
            DateTime toDate = toDateTimePicker3.Value;

            allStoryInputGoodsBindingSource.DataSource = QueryBuilder.Instance.getCompletedInputStories(true, fromDate, toDate, txtSupplierName3.Text, txtNumSupplier3.Text, bShowAll);
        }
    }
}
