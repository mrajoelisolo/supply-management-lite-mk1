using System;
using System.Collections.Generic;
using System.Text;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.DbUtil.Entity.Query;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.StoryManagement.Util
{
    public class QueryBuilder
    {
        private static QueryBuilder m_Instance = new QueryBuilder();

        private QueryBuilder() {}

        public static QueryBuilder Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public List<StoryInputGoods> getInputStories(bool chkDate, DateTime fromDate, DateTime toDate, String fnrsName, String numero, bool showAll)
        {
            List<StoryInputGoods> res = new List<StoryInputGoods>();

            String whereClause = "";

            List<QueryParameter> param = new List<QueryParameter>();
            if (chkDate)
            {
                whereClause += StoryInputGoods.DATECREATION_COL + " BETWEEN @DateFrom AND @DateTo";
                
                param.Add(new QueryParameter("DateFrom", fromDate.Date));
                param.Add(new QueryParameter("DateTo", toDate.Date));
            }

            if (!String.IsNullOrEmpty(numero))
            {
                if (!String.IsNullOrEmpty(whereClause))
                    whereClause += " AND ";

                whereClause += StoryInputGoods.NUMERO_COL + " LIKE '" + numero + "%'";
            }

            if (!String.IsNullOrEmpty(fnrsName))
            {
                if (!String.IsNullOrEmpty(whereClause))
                    whereClause += " AND ";

                whereClause += "EXISTS(SELECT * FROM " + Supplier.TABLE_NAME + " s WHERE s." + Supplier.ID_COL + " = "
                    + StoryInputGoods.TABLE_NAME + "." + StoryInputGoods.ID_TIERS_COL + " AND " + Supplier.REF_COL + " LIKE '" + fnrsName + "%' )";
            }

            if (!showAll)
            {
                if (!String.IsNullOrEmpty(whereClause))
                    whereClause += " AND ";

                whereClause += StoryInputGoods.REMAINING_AMOUNT_COL + " > 0";
            }

            List<object> rs = EntityLoader.Instance.LoadData(typeof(StoryInputGoods), whereClause, param, StoryInputGoods.DATECREATION_COL + ", " + StoryInputGoods.REMAINING_AMOUNT_COL + " DESC");
            foreach (object o in rs)
                res.Add((StoryInputGoods)o);

            return res;
        }

        public List<StoryOutputGoods> getOutputStories(bool chkDate, DateTime fromDate, DateTime toDate, String tiersName, String numero, bool showAll)
        {
            List<StoryOutputGoods> res = new List<StoryOutputGoods>();

            String whereClause = "";
            
            List<QueryParameter> param = new List<QueryParameter>();
            if (chkDate)
            {
                whereClause += StoryOutputGoods.DATECREATION_COL + " BETWEEN @DateFrom AND @DateTo";

                param.Add(new QueryParameter("DateFrom", fromDate.Date));
                param.Add(new QueryParameter("DateTo", toDate.Date));
            }


            if (!String.IsNullOrEmpty(numero))
            {
                if (!String.IsNullOrEmpty(whereClause))
                    whereClause += " AND ";

                whereClause += StoryOutputGoods.NUMERO_COL + " LIKE '" + numero + "%'";
            }

            if (!String.IsNullOrEmpty(tiersName))
            {
                if (!String.IsNullOrEmpty(whereClause))
                    whereClause += " AND ";

                whereClause += "EXISTS(SELECT * FROM " + Tiers.TABLE_NAME + " s WHERE s." + Tiers.ID_COL + " = "
                    + StoryOutputGoods.TABLE_NAME + "." + StoryOutputGoods.ID_TIERS_COL + " AND " + Tiers.REF_COL + " LIKE '" + tiersName + "%' )";
            }

            if (!showAll)
            {
                if (!String.IsNullOrEmpty(whereClause))
                    whereClause += " AND ";

                whereClause += StoryOutputGoods.REMAINING_AMOUNT_COL + " > 0";
            }

            List<object> rs = EntityLoader.Instance.LoadData(typeof(StoryOutputGoods), whereClause, param, StoryOutputGoods.DATECREATION_COL + ", " + StoryOutputGoods.REMAINING_AMOUNT_COL + " DESC");
            foreach (Object o in rs)
                res.Add((StoryOutputGoods)o);

            return res;
        }

        public List<StoryInputGoods> getCompletedInputStories(bool chkDate, DateTime fromDate, DateTime toDate, String fnrsName, String numero, bool showAll)
        {
            List<StoryInputGoods> res = new List<StoryInputGoods>();

            String whereClause = "";

            List<QueryParameter> param = new List<QueryParameter>();
            if (chkDate)
            {
                whereClause += StoryInputGoods.DATECREATION_COL + " BETWEEN @DateFrom AND @DateTo";

                param.Add(new QueryParameter("DateFrom", fromDate.Date));
                param.Add(new QueryParameter("DateTo", toDate.Date));
            }

            if (!String.IsNullOrEmpty(numero))
            {
                if (!String.IsNullOrEmpty(whereClause))
                    whereClause += " AND ";

                whereClause += StoryInputGoods.NUMERO_COL + " LIKE '" + numero + "%'";
            }

            if (!String.IsNullOrEmpty(fnrsName))
            {
                if (!String.IsNullOrEmpty(whereClause))
                    whereClause += " AND ";

                whereClause += "EXISTS(SELECT * FROM " + Supplier.TABLE_NAME + " s WHERE s." + Supplier.ID_COL + " = "
                    + StoryInputGoods.TABLE_NAME + "." + StoryInputGoods.ID_TIERS_COL + " AND " + Supplier.REF_COL + " LIKE '" + fnrsName + "%' )";
            }

            if (!showAll)
            {
                if (!String.IsNullOrEmpty(whereClause))
                    whereClause += " AND ";

                whereClause += StoryInputGoods.REMAINING_AMOUNT_COL + " = 0";
            }

            List<object> rs = EntityLoader.Instance.LoadData(typeof(StoryInputGoods), whereClause, param, StoryInputGoods.DATECREATION_COL + ", " + StoryInputGoods.REMAINING_AMOUNT_COL + " DESC");
            foreach (object o in rs)
                res.Add((StoryInputGoods)o);

            return res;
        }

        public List<StoryOutputGoods> getCompletedOutputStories(bool chkDate, DateTime fromDate, DateTime toDate, String tiersName, String numero, bool showAll)
        {
            List<StoryOutputGoods> res = new List<StoryOutputGoods>();

            String whereClause = "";

            List<QueryParameter> param = new List<QueryParameter>();
            if (chkDate)
            {
                whereClause += StoryOutputGoods.DATECREATION_COL + " BETWEEN @DateFrom AND @DateTo";

                param.Add(new QueryParameter("DateFrom", fromDate.Date));
                param.Add(new QueryParameter("DateTo", toDate.Date));
            }


            if (!String.IsNullOrEmpty(numero))
            {
                if (!String.IsNullOrEmpty(whereClause))
                    whereClause += " AND ";

                whereClause += StoryOutputGoods.NUMERO_COL + " LIKE '" + numero + "%'";
            }

            if (!String.IsNullOrEmpty(tiersName))
            {
                if (!String.IsNullOrEmpty(whereClause))
                    whereClause += " AND ";

                whereClause += "EXISTS(SELECT * FROM " + Tiers.TABLE_NAME + " s WHERE s." + Tiers.ID_COL + " = "
                    + StoryOutputGoods.TABLE_NAME + "." + StoryOutputGoods.ID_TIERS_COL + " AND " + Tiers.REF_COL + " LIKE '" + tiersName + "%' )";
            }

            if (!showAll)
            {
                if (!String.IsNullOrEmpty(whereClause))
                    whereClause += " AND ";

                whereClause += StoryOutputGoods.REMAINING_AMOUNT_COL + " = 0";
            }

            List<object> rs = EntityLoader.Instance.LoadData(typeof(StoryOutputGoods), whereClause, param, StoryOutputGoods.DATECREATION_COL + ", " + StoryOutputGoods.REMAINING_AMOUNT_COL + " DESC");
            foreach (Object o in rs)
                res.Add((StoryOutputGoods)o);

            return res;
        }
    }
}
