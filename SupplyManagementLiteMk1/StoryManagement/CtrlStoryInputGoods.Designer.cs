﻿namespace SupplyManagementLiteMk1.StoryManagement
{
    partial class CtrlStoryInputGoods
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFormDate1 = new System.Windows.Forms.Label();
            this.lblToDate1 = new System.Windows.Forms.Label();
            this.lblSupplierName1 = new System.Windows.Forms.Label();
            this.lblNumSupplier1 = new System.Windows.Forms.Label();
            this.txtSupplierName1 = new System.Windows.Forms.TextBox();
            this.txtNumSupplier1 = new System.Windows.Forms.TextBox();
            this.fromDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.toDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnSearch1 = new System.Windows.Forms.Button();
            this.chkDate1 = new System.Windows.Forms.CheckBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.storyInputGoodsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.inputGoodsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnPay1 = new System.Windows.Forms.Button();
            this.btnFacturate1 = new System.Windows.Forms.Button();
            this.libelleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.puDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uniteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montantDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.szSupplierFullNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateCreationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remainingAmountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montantTotalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storyInputGoodsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputGoodsBindingSource)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel11, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(778, 484);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.BackColor = System.Drawing.Color.Silver;
            this.tableLayoutPanel11.ColumnCount = 7;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.Controls.Add(this.lblFormDate1, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.lblToDate1, 3, 0);
            this.tableLayoutPanel11.Controls.Add(this.lblSupplierName1, 1, 1);
            this.tableLayoutPanel11.Controls.Add(this.lblNumSupplier1, 3, 1);
            this.tableLayoutPanel11.Controls.Add(this.txtSupplierName1, 2, 1);
            this.tableLayoutPanel11.Controls.Add(this.txtNumSupplier1, 4, 1);
            this.tableLayoutPanel11.Controls.Add(this.fromDateTimePicker1, 2, 0);
            this.tableLayoutPanel11.Controls.Add(this.toDateTimePicker1, 4, 0);
            this.tableLayoutPanel11.Controls.Add(this.btnSearch1, 6, 0);
            this.tableLayoutPanel11.Controls.Add(this.chkDate1, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(772, 74);
            this.tableLayoutPanel11.TabIndex = 2;
            // 
            // lblFormDate1
            // 
            this.lblFormDate1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblFormDate1.AutoSize = true;
            this.lblFormDate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFormDate1.Location = new System.Drawing.Point(24, 5);
            this.lblFormDate1.Name = "lblFormDate1";
            this.lblFormDate1.Size = new System.Drawing.Size(79, 26);
            this.lblFormDate1.TabIndex = 0;
            this.lblFormDate1.Text = "Manomboka amin\'ny";
            // 
            // lblToDate1
            // 
            this.lblToDate1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblToDate1.AutoSize = true;
            this.lblToDate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToDate1.Location = new System.Drawing.Point(304, 5);
            this.lblToDate1.Name = "lblToDate1";
            this.lblToDate1.Size = new System.Drawing.Size(71, 26);
            this.lblToDate1.TabIndex = 1;
            this.lblToDate1.Text = "ka hatramin\'ny";
            // 
            // lblSupplierName1
            // 
            this.lblSupplierName1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSupplierName1.AutoSize = true;
            this.lblSupplierName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSupplierName1.Location = new System.Drawing.Point(24, 49);
            this.lblSupplierName1.Name = "lblSupplierName1";
            this.lblSupplierName1.Size = new System.Drawing.Size(72, 13);
            this.lblSupplierName1.TabIndex = 2;
            this.lblSupplierName1.Text = "Fournisseur";
            // 
            // lblNumSupplier1
            // 
            this.lblNumSupplier1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblNumSupplier1.AutoSize = true;
            this.lblNumSupplier1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumSupplier1.Location = new System.Drawing.Point(304, 49);
            this.lblNumSupplier1.Name = "lblNumSupplier1";
            this.lblNumSupplier1.Size = new System.Drawing.Size(50, 13);
            this.lblNumSupplier1.TabIndex = 3;
            this.lblNumSupplier1.Text = "Numero";
            // 
            // txtSupplierName1
            // 
            this.txtSupplierName1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtSupplierName1.Location = new System.Drawing.Point(118, 45);
            this.txtSupplierName1.Name = "txtSupplierName1";
            this.txtSupplierName1.Size = new System.Drawing.Size(180, 20);
            this.txtSupplierName1.TabIndex = 5;
            // 
            // txtNumSupplier1
            // 
            this.txtNumSupplier1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtNumSupplier1.Location = new System.Drawing.Point(398, 45);
            this.txtNumSupplier1.Name = "txtNumSupplier1";
            this.txtNumSupplier1.Size = new System.Drawing.Size(180, 20);
            this.txtNumSupplier1.TabIndex = 4;
            // 
            // fromDateTimePicker1
            // 
            this.fromDateTimePicker1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.fromDateTimePicker1.Location = new System.Drawing.Point(118, 8);
            this.fromDateTimePicker1.Name = "fromDateTimePicker1";
            this.fromDateTimePicker1.Size = new System.Drawing.Size(180, 20);
            this.fromDateTimePicker1.TabIndex = 6;
            // 
            // toDateTimePicker1
            // 
            this.toDateTimePicker1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.toDateTimePicker1.Location = new System.Drawing.Point(398, 8);
            this.toDateTimePicker1.Name = "toDateTimePicker1";
            this.toDateTimePicker1.Size = new System.Drawing.Size(180, 20);
            this.toDateTimePicker1.TabIndex = 7;
            // 
            // btnSearch1
            // 
            this.btnSearch1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSearch1.Image = global::SupplyManagementLiteMk1.Properties.Resources.iconGreen_tiny;
            this.btnSearch1.Location = new System.Drawing.Point(678, 3);
            this.btnSearch1.Name = "btnSearch1";
            this.btnSearch1.Size = new System.Drawing.Size(34, 31);
            this.btnSearch1.TabIndex = 8;
            this.btnSearch1.UseVisualStyleBackColor = true;
            this.btnSearch1.Click += new System.EventHandler(this.btnSearch1_Click);
            // 
            // chkDate1
            // 
            this.chkDate1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.chkDate1.AutoSize = true;
            this.chkDate1.Location = new System.Drawing.Point(3, 11);
            this.chkDate1.Name = "chkDate1";
            this.chkDate1.Size = new System.Drawing.Size(15, 14);
            this.chkDate1.TabIndex = 9;
            this.chkDate1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.szSupplierFullNameDataGridViewTextBoxColumn,
            this.dateCreationDataGridViewTextBoxColumn,
            this.numeroDataGridViewTextBoxColumn,
            this.remainingAmountDataGridViewTextBoxColumn,
            this.montantTotalDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.storyInputGoodsBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 83);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(772, 176);
            this.dataGridView1.TabIndex = 0;
            // 
            // storyInputGoodsBindingSource
            // 
            this.storyInputGoodsBindingSource.DataSource = typeof(SupplyManagementLiteMk1.Datasource.StoryInputGoods);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.libelleDataGridViewTextBoxColumn,
            this.qteDataGridViewTextBoxColumn,
            this.puDataGridViewTextBoxColumn,
            this.uniteDataGridViewTextBoxColumn,
            this.montantDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.inputGoodsBindingSource;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 265);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.Size = new System.Drawing.Size(772, 176);
            this.dataGridView2.TabIndex = 1;
            // 
            // inputGoodsBindingSource
            // 
            this.inputGoodsBindingSource.DataMember = "Goods";
            this.inputGoodsBindingSource.DataSource = this.storyInputGoodsBindingSource;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Silver;
            this.flowLayoutPanel1.Controls.Add(this.btnPay1);
            this.flowLayoutPanel1.Controls.Add(this.btnFacturate1);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 444);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(778, 40);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // btnPay1
            // 
            this.btnPay1.BackColor = System.Drawing.Color.Gray;
            this.btnPay1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPay1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPay1.Image = global::SupplyManagementLiteMk1.Properties.Resources.iconGreen_tiny;
            this.btnPay1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPay1.Location = new System.Drawing.Point(3, 3);
            this.btnPay1.Name = "btnPay1";
            this.btnPay1.Size = new System.Drawing.Size(150, 30);
            this.btnPay1.TabIndex = 2;
            this.btnPay1.Text = "Handoa";
            this.btnPay1.UseVisualStyleBackColor = false;
            this.btnPay1.Click += new System.EventHandler(this.btnPay1_Click);
            // 
            // btnFacturate1
            // 
            this.btnFacturate1.BackColor = System.Drawing.Color.Gray;
            this.btnFacturate1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFacturate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFacturate1.Image = global::SupplyManagementLiteMk1.Properties.Resources.iconYellow_tiny;
            this.btnFacturate1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFacturate1.Location = new System.Drawing.Point(159, 3);
            this.btnFacturate1.Name = "btnFacturate1";
            this.btnFacturate1.Size = new System.Drawing.Size(150, 30);
            this.btnFacturate1.TabIndex = 3;
            this.btnFacturate1.Text = "Bon d\'Entrée (BE)";
            this.btnFacturate1.UseVisualStyleBackColor = false;
            this.btnFacturate1.Click += new System.EventHandler(this.btnFacturate1_Click);
            // 
            // libelleDataGridViewTextBoxColumn
            // 
            this.libelleDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.libelleDataGridViewTextBoxColumn.DataPropertyName = "Libelle";
            this.libelleDataGridViewTextBoxColumn.HeaderText = "Karazana";
            this.libelleDataGridViewTextBoxColumn.Name = "libelleDataGridViewTextBoxColumn";
            this.libelleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // qteDataGridViewTextBoxColumn
            // 
            this.qteDataGridViewTextBoxColumn.DataPropertyName = "Qte";
            this.qteDataGridViewTextBoxColumn.HeaderText = "Fatrany";
            this.qteDataGridViewTextBoxColumn.Name = "qteDataGridViewTextBoxColumn";
            this.qteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // puDataGridViewTextBoxColumn
            // 
            this.puDataGridViewTextBoxColumn.DataPropertyName = "Pu";
            this.puDataGridViewTextBoxColumn.HeaderText = "PU Vente (Ar)";
            this.puDataGridViewTextBoxColumn.Name = "puDataGridViewTextBoxColumn";
            this.puDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // uniteDataGridViewTextBoxColumn
            // 
            this.uniteDataGridViewTextBoxColumn.DataPropertyName = "Unite";
            this.uniteDataGridViewTextBoxColumn.HeaderText = "Unité";
            this.uniteDataGridViewTextBoxColumn.Name = "uniteDataGridViewTextBoxColumn";
            this.uniteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // montantDataGridViewTextBoxColumn
            // 
            this.montantDataGridViewTextBoxColumn.DataPropertyName = "Montant";
            this.montantDataGridViewTextBoxColumn.HeaderText = "Montant (Ar)";
            this.montantDataGridViewTextBoxColumn.Name = "montantDataGridViewTextBoxColumn";
            this.montantDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // szSupplierFullNameDataGridViewTextBoxColumn
            // 
            this.szSupplierFullNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.szSupplierFullNameDataGridViewTextBoxColumn.DataPropertyName = "SzSupplierFullName";
            this.szSupplierFullNameDataGridViewTextBoxColumn.HeaderText = "Fournisseur";
            this.szSupplierFullNameDataGridViewTextBoxColumn.Name = "szSupplierFullNameDataGridViewTextBoxColumn";
            this.szSupplierFullNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateCreationDataGridViewTextBoxColumn
            // 
            this.dateCreationDataGridViewTextBoxColumn.DataPropertyName = "DateCreation";
            this.dateCreationDataGridViewTextBoxColumn.HeaderText = "Date de création";
            this.dateCreationDataGridViewTextBoxColumn.Name = "dateCreationDataGridViewTextBoxColumn";
            this.dateCreationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // numeroDataGridViewTextBoxColumn
            // 
            this.numeroDataGridViewTextBoxColumn.DataPropertyName = "Numero";
            this.numeroDataGridViewTextBoxColumn.HeaderText = "Numero BE";
            this.numeroDataGridViewTextBoxColumn.Name = "numeroDataGridViewTextBoxColumn";
            this.numeroDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // remainingAmountDataGridViewTextBoxColumn
            // 
            this.remainingAmountDataGridViewTextBoxColumn.DataPropertyName = "RemainingAmount";
            this.remainingAmountDataGridViewTextBoxColumn.HeaderText = "Reste à payer (Ar)";
            this.remainingAmountDataGridViewTextBoxColumn.Name = "remainingAmountDataGridViewTextBoxColumn";
            this.remainingAmountDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // montantTotalDataGridViewTextBoxColumn
            // 
            this.montantTotalDataGridViewTextBoxColumn.DataPropertyName = "MontantTotal";
            this.montantTotalDataGridViewTextBoxColumn.HeaderText = "Montant total (Ar)";
            this.montantTotalDataGridViewTextBoxColumn.Name = "montantTotalDataGridViewTextBoxColumn";
            this.montantTotalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // CtrlStoryInputGoods
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "CtrlStoryInputGoods";
            this.Size = new System.Drawing.Size(778, 484);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storyInputGoodsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputGoodsBindingSource)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label lblFormDate1;
        private System.Windows.Forms.Label lblToDate1;
        private System.Windows.Forms.Label lblSupplierName1;
        private System.Windows.Forms.Label lblNumSupplier1;
        private System.Windows.Forms.TextBox txtSupplierName1;
        private System.Windows.Forms.TextBox txtNumSupplier1;
        private System.Windows.Forms.DateTimePicker fromDateTimePicker1;
        private System.Windows.Forms.DateTimePicker toDateTimePicker1;
        private System.Windows.Forms.Button btnSearch1;
        private System.Windows.Forms.CheckBox chkDate1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnPay1;
        private System.Windows.Forms.Button btnFacturate1;
        private System.Windows.Forms.BindingSource storyInputGoodsBindingSource;
        private System.Windows.Forms.BindingSource inputGoodsBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn libelleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn qteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn puDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uniteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn montantDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn szSupplierFullNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateCreationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn remainingAmountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn montantTotalDataGridViewTextBoxColumn;
    }
}
