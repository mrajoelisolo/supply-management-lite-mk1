﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SupplyManagementLiteMk1.MainApp;
using SupplyManagementMk1Sharp.AppUtil;
using ORMFramework.DbUtil;

namespace SupplyManagementLiteMk1
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            SchemaBuilder.Instance.Initialize();

            ConnectionDatasource cda = ConnectionDataSerializer.Instance.Deserialize();
            ConnectionManager.EnumConnectionResult res = ConnectionManager.Instance.Connect(cda);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormSplash());
        }
    }
}