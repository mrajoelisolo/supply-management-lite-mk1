using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using ORMFramework.AppUtil;
using ORMFramework.DbUtil;
using SupplyManagementLiteMk1.Properties;

namespace SupplyManagementLiteMk1.ConnectionManagement.Cmd
{
    public class CmdTestConnection : ICommand
    {
        private PictureBox m_PictureBox;
        private ConnectionDatasource m_Cda;

        public CmdTestConnection(PictureBox pictureBox, ConnectionDatasource cda)
        {
            m_Cda = cda;
            m_PictureBox = pictureBox;
        }

        public void Doing()
        {
            ConnectionManager.EnumConnectionResult res = ConnectionManager.Instance.Connect(m_Cda);
            if (res == ConnectionManager.EnumConnectionResult.LOGGED)
                m_PictureBox.Image = Resources.ConnectionOK;
            else
                m_PictureBox.Image = Resources.ConnectionFailed;
        }
    }
}
