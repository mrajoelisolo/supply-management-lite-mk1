using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.ConnectionManagement.Cmd;
using ORMFramework.DbUtil;

namespace SupplyManagementLiteMk1.ConnectionManagement
{
    public partial class FormConnectionSetup : Form
    {
        public FormConnectionSetup()
        {
            InitializeComponent();

            connectionBindingSource.DataSource = ConnectionDataSerializer.Instance.Deserialize();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            ConnectionDatasource cda = (ConnectionDatasource)connectionBindingSource.DataSource;
            ConnectionDataSerializer.Instance.SerializeWithPassword(cda);

            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            ConnectionDatasource cda = (ConnectionDatasource)connectionBindingSource.DataSource;
            ICommand cmd = new CmdTestConnection(pictureBox1, cda);
            Invoker.Instance.Invoke(cmd);
        }
    }
}