using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.InputManagement.Cmd;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.InputManagement
{
    public partial class FormInputManagement : Form, IObserver
    {
        private Boolean m_Dirty = false;

        public FormInputManagement()
        {
            InitializeComponent();
            inputBindingSource.DataSource = new List<InputGoods>();
            storyInputBindingSource.DataSource = new StoryInputGoods();
        }        

        private void SearchInput()
        {            
            if (!String.IsNullOrEmpty(txtLibelle.Text))
            {
                String whereClause = "";
                whereClause += Stock.LIBELLE_COL + " LIKE '" + txtLibelle.Text + "%'";
                stockBindingSource.DataSource = EntityLoader.Instance.LoadData(typeof(Stock), whereClause, null, Stock.LIBELLE_COL);
            }
            else 
                ClearResult();
        }

        private void ClearResult()
        {
            stockBindingSource.Clear();
            txtLibelle.Text = "";
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchInput();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdAddStock(stockBindingSource, inputBindingSource, txtLibelle.Text, this);
            Invoker.Instance.Invoke(cmd);
        }       

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
            //DialogResult = DialogResult.Cancel;
        }

        private void btnValidate_Click(object sender, EventArgs e)
        {
            String str = txtTiers.Text;
            if (String.IsNullOrEmpty(str))
            {
                MessageBox.Show("Tsy voasoratra ny anaran'ny fournisseur", "Tsy mety", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            m_Dirty = false;

            ICommand cmd = new CmdValidateInput(storyInputBindingSource, inputBindingSource, dateTimePicker1.Value, this);
            Invoker.Instance.Invoke(cmd);            
        }        

        private void txtLibelle_KeyUp(object sender, KeyEventArgs e)
        {
            SearchInput();
        }       

        private void btnRemoveItem_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdRemoveItemInput(inputBindingSource, this);
            Invoker.Instance.Invoke(cmd);            
        }

        private void btnTiers_Click(object sender, EventArgs e)
        {
            ICommand cmd = new CmdBrowseSupplier(storyInputBindingSource, txtTiers);
            Invoker.Instance.Invoke(cmd);
        }

        private float GetMontantTotal()
        {
            float sum = 0f;

            List<InputGoods> objs = (List<InputGoods>)inputBindingSource.DataSource;
            foreach(InputGoods iGoods in objs)
                sum += iGoods.Montant;

            return sum;
        }

        private void FormInputManagement_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (m_Dirty)
            {
                DialogResult res = MessageBox.Show("Hidina ve ?", "Entana miditra", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (res == DialogResult.No)
                    e.Cancel = true;
            }
        }

        public void Notify(String value)
        {
            ClearResult();
            txtMontantTotal.Text = "" + GetMontantTotal();

            if (value != null)
                if (value.Equals("dirty"))
                    m_Dirty = true;
        }
    }
}