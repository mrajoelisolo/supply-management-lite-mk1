using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using SupplyManagementLiteMk1.Misc;

namespace SupplyManagementLiteMk1.InputManagement
{
    public partial class FormEditInputStock : Form
    {
        private Stock m_Stock;
        private StockUnit m_StockUnit;
        private Boolean m_Buffered;

        public FormEditInputStock(Stock stock, Boolean buffered)
        {
            InitializeComponent();

            CenterToParent();

            m_Stock = stock;
            m_Buffered = buffered;
            
            StockUnit stockUnit = new StockUnit(stock.Unite, 1f);
            m_StockUnit = stockUnit;

            txtLibelle.Text = stock.Libelle;
            txtPUAchat.Text = "" + stock.PuAchat;
            lblUnit.Text = "[" + stock.Unite + "]";
            //txtQte.Text = "" + stock.Qte;
        }

        public float Qte
        {
            get
            {
                try
                {
                    float res = float.Parse(txtQte.Text);
                    return res;
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                    return 0f;
                }
            }
        }

        public float PUAchat
        {
            get
            {
                try
                {
                    float res = float.Parse(txtPUAchat.Text);
                    return res;
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                    return 0f;
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (Qte <= 0 || PUAchat <= 0)
                return;

            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void txtQte_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnOk.PerformClick();
            else if (e.KeyCode == Keys.Escape)
                btnCancel.PerformClick();
        }

        private void btnBrowseUnits_Click(object sender, EventArgs e)
        {
            FormBrowseUnits f = new FormBrowseUnits(m_Stock, m_Buffered);
            if (f.ShowDialog() == DialogResult.OK)
            {
                lblUnit.Text = "[" + f.SelectedUnit.Name + "]";
                m_StockUnit = f.SelectedUnit;
            }
        }

        public StockUnit SelectedUnit
        {
            get
            {
                return m_StockUnit;
            }
        }
    }    
}