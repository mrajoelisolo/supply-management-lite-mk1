using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using System.IO;
using SupplyManagementLiteMk1.Datasource;
using TFramework.Reporting;
using TFramework.Utilities;
using SupplyManagementLiteMk1.MainApp.Modules;

namespace SupplyManagementLiteMk1.InputManagement.Cmd
{
    public class CmdFacturateInput : ICommand
    {
        private BindingSource m_StoryOutputGoodsBindingSource;

        public CmdFacturateInput(BindingSource storyOutputGoodsBindingSource)
        {
            m_StoryOutputGoodsBindingSource = storyOutputGoodsBindingSource;
        }

        public void Doing()
        {
            StoryInputGoods story = (StoryInputGoods)m_StoryOutputGoodsBindingSource.Current;
            if (story == null) return;

            if (story.RemainingAmount > 0)
            {
                MessageBox.Show("Mbola misy vola tsy voaloa, tsy afaka avoaka amin'ny Bon d'Entr�e tsotra ?", "Tsy mety", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            DialogResult res = MessageBox.Show("Avoaka ve ny Bon d'Entr�e ?", "Entana miditra", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.No) return;

            String src = SystemManager.Instance.GetSystemDirectory() + "\\ModeleBE.rep";

            SaveFileDialog sf = new SaveFileDialog();
            sf.Filter = SystemManager.Instance.ReportFilter;
            
            sf.FileName = "BE_" + story.Numero + ".doc";
            if (sf.ShowDialog() == DialogResult.Cancel) return;

            String dest = sf.FileName;

            try
            {
                File.Copy(src, dest);
            }
            catch (Exception ex)
            {
                File.Delete(dest);
                File.Copy(src, dest);
                System.Console.WriteLine(ex.Message);
            }

            MSWordUtilities.Instance.Initialize(dest);

            MSWordUtilities.Instance.ReplaceField("NomFNRS", story.SzSupplierFullName);
            MSWordUtilities.Instance.ReplaceField("NoBE", story.Numero);
            MSWordUtilities.Instance.ReplaceField("CreationDate", story.szDateCreation);
            MSWordUtilities.Instance.ReplaceField("EditionDate", story.SzDateEdition);

            List<List<String>> rows = new List<List<String>>();
            foreach (InputGoods goods in story.Goods)
            {
                List<String> row = new List<String>();

                row.Add(goods.Libelle);
                row.Add(DigitToLetters.Instance.ToNbAmount(goods.Pu, ""));
                row.Add("" + goods.Qte);
                row.Add(goods.Unite);
                row.Add(DigitToLetters.Instance.ToNbAmount(goods.Montant, ""));

                rows.Add(row);
            }
            MSWordUtilities.Instance.FillTable("TableauBE", rows);

            MSWordUtilities.Instance.ReplaceField("TotalMontant", DigitToLetters.Instance.ToNbAmount(story.MontantTotal, ""));

            MSWordUtilities.Instance.Visible = true;
            MSWordUtilities.Instance.SaveDocument();
        }
    }
}
