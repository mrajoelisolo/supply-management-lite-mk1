using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using ORMFramework.AppUtil;
using SupplyManagementLiteMk1.StockManagement;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.InputManagement.Cmd
{
    public class CmdAddStock : ICommand
    {
        private BindingSource m_StockBindingSource;
        private BindingSource m_ioStockBindingSource;
        private String m_TxtLibelle;
        private IObserver m_Observer;

        public CmdAddStock(BindingSource stockBindingSource, BindingSource ioStockBingingSource, String txtLibelle, IObserver observer)
        {
            m_StockBindingSource = stockBindingSource;

            m_ioStockBindingSource = ioStockBingingSource;
            m_TxtLibelle = txtLibelle;
            m_Observer = observer;
        }

        public void Doing()
        {            
            Stock stock = (Stock)m_StockBindingSource.Current;

            if (stock == null && String.IsNullOrEmpty(m_TxtLibelle)) return;

            //V�rifier d'abord si l'article n'�xiste pas d�ja dans le panier
            List<InputGoods> goods = (List<InputGoods>)m_ioStockBindingSource.DataSource;
            foreach (InputGoods inputGoods in goods)
            {
                if (stock == null) break; //Solution for E1
                if (inputGoods.IdStock == stock.Id) //There is an error when I try to add several new stocks : Resolved since 06/04/2011 E1
                {
                    DialogResult res = MessageBox.Show("Efa misy ao anaty lisitrin'ny entana ampidirina io entana io. Alefa ihany ve ?", "", MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question);
                    if(res == DialogResult.No) return;
                }
            }
            //Added since 30/03/2011 : Gestion d'erreurs et d'intergit�

            bool isRecent = false;
            float inputQty = 0f;
            StockUnit stockUnit;
            float PUAchat = 0f;
            if (stock == null)            
            {
                DialogResult res = MessageBox.Show("Tsy mbola misy ao amin'ny 'stock' ilay entana, ampidirina ao anaty lisitra ve ?", "Entana vaovao", MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Information);
                if (res == DialogResult.No) return;

                Stock newStock = new Stock();
                newStock.Libelle = m_TxtLibelle;
                FormEditStock2 f1 = new FormEditStock2(newStock);                
                if (f1.ShowDialog() == DialogResult.Cancel) return;
                //On enregistre le stock � une qt� �gale a z�ro, au cas o� l'utilisateur anulle l'ES, sinon il y a erreur sur la gestion de stock

                FormEditInputStock f2 = new FormEditInputStock(newStock, true);
                if (f2.ShowDialog() == DialogResult.Cancel) return;

                inputQty = f2.Qte;
                stockUnit = f2.SelectedUnit; //Since 10/04/2011
                PUAchat = newStock.PuAchat;
                newStock.Qte = 0;

                EntityLoader.Instance.Save(newStock);

                foreach (StockUnit newStockUnit in newStock.BufUnits)
                {
                    newStockUnit.IdStock = newStock.Id;
                    EntityLoader.Instance.Save(newStockUnit);
                }

                stock = newStock;
                //inputQty = newStock.Qte;
                isRecent = true;
            } else {                
                FormEditInputStock f = new FormEditInputStock(stock, false);
                if (f.ShowDialog() == DialogResult.Cancel) return;
                inputQty = f.Qte;
                stockUnit = f.SelectedUnit; //Since 10/04/2011            
                PUAchat = f.PUAchat; //Le prix d'achat peut �tre chang� : 06/04/2011
            }            

            InputGoods inputStock = new InputGoods();
            inputStock.Libelle = stock.Libelle;
            //inputStock.Pu = stock.Pu; //Erreur : on n'entre pas le PU pour les entr�es mais le PU_Achat : 06/04/2011
            inputStock.Pu = PUAchat;
            inputStock.Qte = inputQty;
            inputStock.Unite = stockUnit.Name; //Since 10/04/2011  
            inputStock.Factor = stockUnit.Factor; //Since 10/04/2011  
            inputStock.IdStock = stock.Id;
            inputStock.IsRecent = isRecent;
            m_ioStockBindingSource.Add(inputStock);
            m_Observer.Notify("dirty");
        }
    }
}