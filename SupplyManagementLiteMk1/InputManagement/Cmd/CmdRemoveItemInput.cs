using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;

namespace SupplyManagementLiteMk1.InputManagement.Cmd
{
    public class CmdRemoveItemInput : ICommand
    {
        private BindingSource m_IoStockBindingSource;
        private IObserver m_Observer;

        public CmdRemoveItemInput(BindingSource ioStockBindingSource, IObserver observer)
        {
            m_IoStockBindingSource = ioStockBindingSource;
            m_Observer = observer;
        }

        public void Doing()
        {
            DialogResult res = MessageBox.Show("Esorina ve ?", "Esorina", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.No) return;

            InputGoods story = (InputGoods)m_IoStockBindingSource.Current;
            m_IoStockBindingSource.Remove(story);

            m_Observer.Notify(null);
        }
    }
}
