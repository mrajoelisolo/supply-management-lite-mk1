using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using SupplyManagementLiteMk1.StoryManagement.Cmd;
using ORMFramework.DbUtil.Entity;

namespace SupplyManagementLiteMk1.InputManagement.Cmd
{
    public class CmdValidateInput : ICommand
    {
        private BindingSource m_StoryInputBindingSource;
        private BindingSource m_InputBindingSource;
        private DateTime m_InputDate;
        private Form m_Parent;

        public CmdValidateInput(BindingSource storyInputBindingSource, BindingSource inputBindingSource, DateTime inputDate, Form parent)
        {
            m_StoryInputBindingSource = storyInputBindingSource;
            m_InputBindingSource = inputBindingSource;
            m_InputDate = inputDate;
            m_Parent = parent;
        }

        public void Doing()
        {
            //List<IOStock> inputs = (List<IOStock>) m_InputBindingSource.DataSource;
            List<InputGoods> inputs = (List<InputGoods>)m_InputBindingSource.DataSource;
            if (inputs == null || inputs.Count == 0)
            {
                MessageBox.Show("Tsy misy entana ao anaty lisitra", "Tsy mety", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            DialogResult res = MessageBox.Show("Ekena ve ireo lisitra ireo ireo ?", "Entana ampidirina", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.No) return;            

            StoryInputGoods story = (StoryInputGoods)m_StoryInputBindingSource.Current;//new StoryInputGoods();

            //Anontaniana ve aloha daholo sa ampahiny
            FormEditInputAmount f = new FormEditInputAmount(m_StoryInputBindingSource, m_InputBindingSource);
            if (f.ShowDialog() == DialogResult.Cancel) return;

            story.DateCreation = m_InputDate;
            EntityLoader.Instance.Save(story);
            //Mise � jour du num�ro de BE
            //String szId = "BE" + story.Id;
            //story.Numero = "" + Math.Abs(szId.GetHashCode());
            story.Numero = "" + story.Id;
            EntityLoader.Instance.Update(story);

            foreach (InputGoods input in inputs)
            {
                Stock stock = (Stock)EntityLoader.Instance.LoadData(typeof(Stock), input.IdStock);
                //if (!input.IsRecent) // Depuis que E1 est r�solu, on en a pas besoin
                //{
                    stock.Qte = stock.Qte + input.Qte * input.Factor; //Added factor since 11/04/11
                    EntityLoader.Instance.Update(stock);
                //}

                input.IdStoryInputGoods = story.Id;
                EntityLoader.Instance.Save(input);       
            }

            //On enregistre les paiements partiels
            //S'il n'ya eu aucun paiement, alors on n'enregistre rien dans l'historique
            if (story.RemainingAmount > 0 && story.LastVersedAmount > 0)
            {
                InputPay inputPay = new InputPay();
                inputPay.Amount = story.LastVersedAmount;
                inputPay.DateOfPay = DateTime.Now;
                inputPay.IdStoryInputGoods = story.Id;
                story.LastVersedAmount = 0f;
                
                EntityLoader.Instance.Save(inputPay);
            }

            //On sort le BE
            ICommand cmd = null;
            if (story.RemainingAmount > 0)
                cmd = new CmdFacturateInputPayment(m_StoryInputBindingSource);
            else if (story.RemainingAmount == 0)
                cmd = new CmdFacturateInput(m_StoryInputBindingSource);
            Invoker.Instance.Invoke(cmd);

            //m_Parent.DialogResult = DialogResult.OK;
            m_Parent.Close();
        }
    }
}
