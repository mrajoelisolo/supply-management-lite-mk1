using System;
using System.Collections.Generic;
using System.Text;
using ORMFramework.AppUtil;
using System.Windows.Forms;
using SupplyManagementLiteMk1.Datasource;
using SupplyManagementLiteMk1.SupplierManagement;

namespace SupplyManagementLiteMk1.InputManagement.Cmd
{
    public class CmdBrowseSupplier : ICommand
    {
        private BindingSource m_StoryInputBindingSource;
        private TextBox m_TxtTiers;

        public CmdBrowseSupplier(BindingSource storyInputBindingSource, TextBox txtTiers) 
        {
            m_StoryInputBindingSource = storyInputBindingSource;
            m_TxtTiers = txtTiers;
        }

        public void Doing()
        {
            StoryInputGoods story = (StoryInputGoods)m_StoryInputBindingSource.Current;

            FormSupplierManagement f = new FormSupplierManagement();
            if (f.ShowDialog() == DialogResult.OK)
            {
                Supplier supplier = f.SelectedSupplier;
                story.IdTiers = supplier.Id;

                m_TxtTiers.Text = supplier.Ref;
                //EntityLoader.Instante.Update(story);
            }
        }
    }
}
