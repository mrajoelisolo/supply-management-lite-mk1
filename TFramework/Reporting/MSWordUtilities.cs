using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Office.Interop.Word;
using System.Runtime.InteropServices;
using System.Reflection;
using System.IO;

namespace TFramework.Reporting
{
    public class MSWordUtilities
    {
        private static MSWordUtilities m_Instance = new MSWordUtilities();

        private MSWordUtilities() {}

        public static MSWordUtilities Instance
        {
            get
            {
                return m_Instance;
            }
        }

        private Application m_Application;
        private Document m_Document;
        private Selection m_Selection;
        private object missing = System.Reflection.Missing.Value;

        public void Initialize(String path)
        {
            Dispose();
            m_Application = new Application();

            m_Application.Visible = false;
            object szPath = @path; 
            bool fileExists = System.IO.File.Exists(path);
            if (fileExists)
            {
                m_Document = m_Application.Documents.Open(ref szPath, ref missing, ref missing,
				    ref missing, ref missing, ref missing, ref missing, ref missing,
				    ref missing, ref missing, ref missing, ref missing, ref missing,
				    ref missing, ref missing, ref missing);
            }
            else
            {
                m_Document = m_Application.Documents.Add(ref missing, ref missing, ref missing, ref missing);
            }

            m_Selection = m_Application.Selection;
        }

        public void Dispose()
        {
            if (m_Application != null)
            {
                try
                {
                    m_Application.Quit(ref missing, ref missing, ref missing);
                }
                catch (COMException ex) {
                    //System.Windows.Forms.MessageBox.Show(ex.Message);
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        public bool Visible
        {
            get { return m_Application.Visible; }
            set { m_Application.Visible = value; }
        }

        public void ReplaceField(String field, List<String> rows)
        {
            object o1 = @Microsoft.Office.Interop.Word.WdGoToItem.wdGoToLine;
            object o2 = @Microsoft.Office.Interop.Word.WdGoToDirection.wdGoToFirst;
            object o3 = 1;
            object o4 = "";
            m_Selection.GoTo(ref o1, ref o2, ref o3, ref o4);
            m_Selection.Find.ClearFormatting();
            m_Selection.Find.Replacement.ClearFormatting();

            Find f = m_Selection.Find;
            f.Text = "<%" + field + "%>";
            f.Forward = true;
            f.Format = false;
            f.MatchCase = false;
            f.MatchWholeWord = false;
            f.MatchKashida = false;
            f.MatchDiacritics = false;
            f.MatchAlefHamza = false;
            f.MatchControl = false;
            f.MatchWildcards = false;
            f.MatchSoundsLike = false;
            f.MatchAllWordForms = false;

            if(m_Selection.Find.Execute(ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing, 
                    ref missing, ref missing, ref missing, ref missing, ref missing))
            {
                if(rows == null) 
                {
                    m_Selection.TypeText(" ");
                    return;
                }
                if(rows.Count == 0) 
                {
                    m_Selection.TypeText(" ");
                    return;
                }
                foreach(String row in rows)
                {
                    m_Selection.TypeText(row);
                    m_Selection.TypeParagraph();
                }
                return;
            }
        }

        public bool GoTo(String field)
        {
            Find f = m_Selection.Find;
            f.Text = "<%" + field + "%>";
            f.Forward = true;
            f.Format = false;
            f.MatchCase = false;
            f.MatchWholeWord = false;
            f.MatchKashida = false;
            f.MatchDiacritics = false;
            f.MatchAlefHamza = false;
            f.MatchControl = false;
            f.MatchWildcards = false;
            f.MatchSoundsLike = false;
            f.MatchAllWordForms = false;

            return f.Execute(ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing);
        }

        public void TypeParagraph()
        {
            m_Selection.TypeParagraph();
        }

        public void AppendLine(String value)
        {
            if (String.IsNullOrEmpty(value)) return;
            m_Selection.TypeParagraph();
            m_Selection.TypeText(value);
        }

        public void Append(String value)
        {
            if (String.IsNullOrEmpty(value)) return;
            //if (!String.IsNullOrEmpty(style))
            //{
            //    m_Selection.Style??
            //}
            m_Selection.TypeText(value);
        }

        public void ReplaceField(String field, String value)
        {
            object o1 = @Microsoft.Office.Interop.Word.WdGoToItem.wdGoToLine;
            object o2 = @Microsoft.Office.Interop.Word.WdGoToDirection.wdGoToFirst;
            object o3 = 1;
            object o4 = "";
            m_Selection.GoTo(ref o1, ref o2, ref o3, ref o4);
            m_Selection.Find.ClearFormatting();
            m_Selection.Find.Replacement.ClearFormatting();

            Find f = m_Selection.Find;
            f.Text = "<%" + field + "%>";
            f.Forward = true;
            f.Format = false;
            f.MatchCase = false;
            f.MatchWholeWord = false;
            f.MatchKashida = false;
            f.MatchDiacritics = false;
            f.MatchAlefHamza = false;
            f.MatchControl = false;
            f.MatchWildcards = false;
            f.MatchSoundsLike = false;
            f.MatchAllWordForms = false;

            if(f.Execute(ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing));
            {
                if(value != null)
                    if(value.Length == 0)
                        m_Selection.TypeText(" ");
                m_Selection.TypeText(value);
            }
        }

        //public void CreateTable(int colCount, int rowCount, List<String> headers)
        //{
        //    Microsoft.Office.Interop.Word.Table table = m_Document.Tables.Add(m_Selection.Range, rowCount, colCount, ref missing, ref missing);

        //    table.ApplyStyleHeadingRows = true;
        //    table.ApplyStyleLastRow = true;
        //    table.ApplyStyleFirstColumn = true;
        //    table.ApplyStyleLastColumn = true;

        //    foreach(String header in headers)
        //    {
        //        m_Selection.TypeText(header);
        //        if (headers.IndexOf(header) != headers.Count - 1)
        //            m_Selection.MoveRight(ref missing, ref missing, ref missing);
        //    }
        //}

        //public void FillTable(List<List<String>> rows) 
        //{
        //    if (rows == null) return;
        //    Selection s = m_Selection;
        //    s.TypeText(" ");
        //    foreach (List<String> row in rows)
        //    {
        //        s.InsertRowsBelow(ref missing);
        //        foreach (String col in row)
        //        {
        //            s.TypeText(col);
        //            if (row.IndexOf(col) < row.Count - 1)
        //                s.MoveRight(ref missing, ref missing, ref missing);
        //        }
        //    }
        //    s.MoveDown(ref missing, ref missing, ref missing);
        //}

        //public void FillTable(String tableName, List<List<String>> values)
        //{
        //    if (values == null) return;
        //    if (String.IsNullOrEmpty(tableName)) return;

        //    object o1 = @Microsoft.Office.Interop.Word.WdGoToItem.wdGoToLine;
        //    object o2 = @Microsoft.Office.Interop.Word.WdGoToDirection.wdGoToFirst;
        //    object o3 = 1;
        //    object o4 = "";
        //    m_Selection.GoTo(ref o1, ref o2, ref o3, ref o4);
        //    m_Selection.Find.ClearFormatting();
        //    m_Selection.Find.Replacement.ClearFormatting();

        //    Find f = m_Selection.Find;
        //    f.Text = "<%" + tableName + "%>";
        //    f.Forward = true;
        //    f.Format = false;
        //    f.MatchCase = false;
        //    f.MatchWholeWord = false;
        //    f.MatchKashida = false;
        //    f.MatchDiacritics = false;
        //    f.MatchAlefHamza = false;
        //    f.MatchControl = false;
        //    f.MatchWildcards = false;
        //    f.MatchSoundsLike = false;
        //    f.MatchAllWordForms = false;

        //    if (f.Execute(ref missing, ref missing, ref missing, ref missing, ref missing,
        //            ref missing, ref missing, ref missing, ref missing, ref missing,
        //            ref missing, ref missing, ref missing, ref missing, ref missing)) ;
        //    {
        //        FillTable(values);
        //    }
        //}

        //By Naruto
        public void FillTable(String tableName, List<List<String>> rows)
        {
            if (String.IsNullOrEmpty(tableName)) return;
            if (rows == null) return;

            object o1 = @Microsoft.Office.Interop.Word.WdGoToItem.wdGoToLine;
            object o2 = @Microsoft.Office.Interop.Word.WdGoToDirection.wdGoToFirst;
            object o3 = 1;
            object o4 = "";
            m_Selection.GoTo(ref o1, ref o2, ref o3, ref o4);
            m_Selection.Find.ClearFormatting();
            m_Selection.Find.Replacement.ClearFormatting();

            Find f = m_Selection.Find;
            f.Text = "<%" + tableName + "%>";
            f.Forward = true;
            f.Format = false;
            f.MatchCase = false;
            f.MatchWholeWord = false;
            f.MatchKashida = false;
            f.MatchDiacritics = false;
            f.MatchAlefHamza = false;
            f.MatchControl = false;
            f.MatchWildcards = false;
            f.MatchSoundsLike = false;
            f.MatchAllWordForms = false;

            if (f.Execute(ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing)) ;
            {
                ReplaceField(tableName, "");
                int iCount = 0;
                Selection s = m_Selection;
                foreach (List<String> row in rows)
                {
                    FillRow(row);                    
                    if(iCount < rows.Count - 1)
                        s.InsertRowsBelow(ref missing);
                    iCount++;
                }
            }
        }

        private void FillRow(List<String> cols)
        {
            Selection s = m_Selection;
            foreach (String col in cols)
            {
                s.TypeText(col);
                if (cols.IndexOf(col) < cols.Count - 1)
                    s.MoveRight(ref missing, ref missing, ref missing);
            }
        }

        public void SaveDocument()
        {
            if (m_Document != null)
                m_Document.Save();
        }
    }
}
