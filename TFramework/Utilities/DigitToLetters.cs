using System;
using System.Collections.Generic;
using System.Text;

namespace TFramework.Utilities
{
    public class DigitToLetters
    {
        private static DigitToLetters m_Instance = new DigitToLetters();

        private DigitToLetters() { }

        public static DigitToLetters Instance
        {
            get
            {
                return m_Instance;
            }
        }

        private String ToCentaine(long N100)
        {
            long Q1 = 0;
            String str100 = "";
            double N10 = 0d;
            double Q100 = 0d;
            double Q10 = 0;

            Q100 = Div(N100, 100);
            N10 = N100 % 100;

            if (N10 >= 1 && N10 <= 20)
            {
                Q10 = 0;
                Q1 = (long)N10;
            }
            else if ((N10 >= 21 && N10 <= 69) || (N10 >= 80 && N10 <= 89))
            {
                long _N10 = (long) N10;

                Q10 = _N10 - _N10 % 10;
                Q1 = _N10 % 10;
            }
            else if ((N10 >= 70 && N10 <= 79) || (N10 >= 90 && N10 <= 99))
            {
                long _N10 = (long) N10;

                Q10 = (long)Div(N10, 10) * 10;
                Q1 = 10 + _N10 % 10;
            }

            if (Q100 != 0)
            {
                if (Q100 != 1)
                    str100 = NbToStr(Q100);
                str100 += " " + " CENT";
            }

            if (Q10 != 0)
            {
                str100 += " " + NbToStr(Q10);
            }

            if (Q1 != 0)
            {
                str100 += " " + NbToStr(Q1);
            }

            return str100;
        }

        private const long MILLIARD = 1000000;
        private const long MILLION = 1000000;
        private const long MILLE = 1000;

        private String ToLetter(double n)
        {
            String res = "";

            String phrase = "";            
            long nb = (long)n;
            long quo = 0;

            try
            {
                quo = Div(nb, MILLIARD);

                if (quo != 0)
                {
                    if (quo == 1)
                        phrase += ToCentaine(quo) + " " + "MILLIARD ";
                    else if (quo >= 2 && quo <= 999)
                        phrase += ToCentaine(quo) + " " + "MILLIARDS ";
                }

                nb = nb % MILLIARD;
                quo = Div(nb, MILLION);

                if (quo != 0)
                {
                    if (quo == 1)
                        phrase += ToCentaine(quo) + " " + "MILLION ";
                    else if (quo >= 2 && quo <= 999)
                        phrase += ToCentaine(quo) + " " + "MILLIONS ";
                }

                nb = nb % MILLION;
                quo = Div(nb, MILLE);
                if (quo != 0)
                {
                    if (quo != 1)
                        phrase += ToCentaine(quo);
                    phrase += " " + "MILLE ";
                }

                nb = nb % MILLE;
                phrase += " " + ToCentaine(nb);

                res = phrase;
            }
            catch (Exception ex)
            {
                throw new Exception("Ce nombre n'est pas pr�vu par le convertisseur : " + ex.Message);
            }

            return res;
        }

        private String NbToStr(double nb)
        {
            long n = (long)nb;
            switch(n) {
                case 1 : return "UN";
                case 2 : return "DEUX";
                case 3 : return "TROIS";
                case 4 : return "QUATRE";
                case 5 : return "CINQ";
                case 6 : return "SIX";
                case 7 : return "SEPT";
                case 8 : return "HUIT";
                case 9 : return "NEUF";
                case 10 : return "DIX";
                case 11 : return "ONZE";
                case 12 : return "DOUZE";
                case 13 : return "TREIZE";
                case 14 : return "QUATORZE";
                case 15 : return "QUINZE";
                case 16 : return "SEIZE";
                case 17 : return "DIX SEPT";
                case 18 : return "DIX HUIT";
                case 19 : return "DIX NEUF";
                case 20 : return "VINGT";
                case 30 : return "TRENTE";
                case 40 : return "QUARANTE";
                case 50 : return "CINQUANTE";
                case 60 : return "SOIXANTE";
                case 70 : return "SOIXANTE";
                case 80 : return "QUATRE VINGT";
                case 90 : return "QUATRE VINGT";
                case 100 : return "CENT";
            }
            return "ERROR";
        }

        public long Div(object n, long d)
        {
            long a = (long)n;
            return (long)((a - a%d) / d);
        }

        public String NbToLetter(double nb)
        {
            double nb1 = (int) nb;
            double nb2 = (nb - (int)nb) * 100;

            if (nb2 == 0) return ToLetter(nb1);
            return (ToLetter(nb1) + ", " + ToLetter(nb2));
        }

        public String ToLetterAmount(double a, String unit)
        {
            return NbToLetter(a) + " " + unit + " (" + String.Format("#,##0", a) + " " + unit + ")";
        }

        public String ToNbAmount(double a, String unit)
        {
            String res = String.Format("{0:#,##}", a);
            if (!String.IsNullOrEmpty(unit)) res += " " + unit;

            return res;             
        }
    }
}
