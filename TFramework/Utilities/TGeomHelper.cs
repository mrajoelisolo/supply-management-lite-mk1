using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace TFramework.Utilities
{
    public class TGeomHelper
    {
        private static TGeomHelper m_Instance = new TGeomHelper();

        private TGeomHelper() { }

        public double GetDistance(int x1, int y1, int x2, int y2)
        {
            double res = 0;

            res = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));

            return res;
        }

        public static TGeomHelper Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public double GetAngle(Point o, Point p1, Point p2)
        {
            double res = Math.Atan2(p2.Y - o.Y, p2.X - o.X) - Math.Atan2(p1.Y - o.Y, p1.X - o.X);

            while (res > Math.PI) res -= 2 * Math.PI;
            while (res < -Math.PI) res += 2 * Math.PI;

            return res;
        }

        public double GetAngle(int x1, int y1, int x2, int y2)
        {
            double res = Math.Atan2(y2 - y1, x2 - x1);

            while (res > Math.PI) res -= 2 * Math.PI;
            while (res < -Math.PI) res += 2 * Math.PI;

            return res;
        }
        
        public double ToDeg(double rad)
        {
            return 180 * rad / Math.PI;
        }

        public Boolean Contains(int[] xTab, int[] yTab, int nCount, Point p)
        {
            double angle = 0;
            double theta1, theta2, dTheta;

            for (int i = 0; i < nCount; i++)
            {
                theta1 = Math.Atan2(yTab[i] - p.Y, xTab[i] - p.X);
                theta2 = Math.Atan2(yTab[(i + 1) % nCount] - p.Y, xTab[(i + 1) % nCount] - p.X);
                dTheta = theta2 - theta1;

                while (dTheta > Math.PI) dTheta -= 2 * Math.PI;
                while (dTheta < -Math.PI) dTheta += 2 * Math.PI;
                angle += dTheta;
            }

            return Math.Abs(angle) > Math.PI;
        }
    }
}
