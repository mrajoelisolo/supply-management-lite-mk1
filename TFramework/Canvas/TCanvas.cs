using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using TFramework.Canvas.Shapes;
using System.Drawing.Drawing2D;

namespace TFramework.Canvas
{
    public partial class TCanvas : PictureBox
    {
        private CompositingQuality m_CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
        private SmoothingMode m_SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        private List<IShape> m_Shapes = new List<IShape>();

        public TCanvas()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            pe.Graphics.CompositingQuality = m_CompositingQuality;
            pe.Graphics.SmoothingMode = m_SmoothingMode;

            foreach (IShape sh in m_Shapes)
                sh.Draw(pe.Graphics);
        }

        public void AddShape(IShape sh)
        {
            if (m_Shapes.Contains(sh)) return;
            m_Shapes.Add(sh);
            this.Refresh();
        }

        public void RemoveShape(IShape sh)
        {
            if (!m_Shapes.Contains(sh)) return;
            m_Shapes.Remove(sh);
            this.Refresh();
        }

        public void ClearShapes()
        {
            m_Shapes.Clear();
            this.Refresh();
        }

        public SmoothingMode SmoothingModeValue
        {
            get { return m_SmoothingMode; }
            set { 
                m_SmoothingMode = value; 
            }
        }

        public CompositingQuality CompositingQualityValue {
            get { return m_CompositingQuality; }
            set { 
                m_CompositingQuality = value; 
            }
        }
    }
}
