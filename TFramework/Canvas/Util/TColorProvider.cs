using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace TFramework.Canvas.Util
{
    public class TColorProvider
    {
        private static TColorProvider m_Instance = new TColorProvider();
        private List<Color> m_Colors = new List<Color>();

        private TColorProvider() { }

        public static TColorProvider Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public Color GetColor(int r, int g, int b)
        {
            foreach (Color color in m_Colors)
            {
                byte _r = (byte)r, _g = (byte)g, _b = (byte)b;
                if (color.R == _r && color.G == _g && color.B == _b)
                    return color;
            }

            Color res = Color.FromArgb(r, g, b);
            m_Colors.Add(res);
            return res;
        }
    }
}
