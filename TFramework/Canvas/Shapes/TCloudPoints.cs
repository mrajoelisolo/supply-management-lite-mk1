using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace TFramework.Canvas.Shapes
{
    public class TCloudPoints : TAbstractShape, IShape
    {
        private List<Point> m_Pts = new List<Point>();
        private int m_PdSize = 5;

        public TCloudPoints() { }

        public TCloudPoints(List<Point> pts)
        {
            foreach (Point p in pts)
                m_Pts.Add(p);
        }

        public void Draw(Graphics g)
        {
            foreach(Point p in m_Pts)
                g.DrawEllipse(base.PenStyle, p.X - m_PdSize/2, p.Y - m_PdSize/2, m_PdSize, m_PdSize);
        }

        public int PointSize
        {
            get { return m_PdSize; }
            set {m_PdSize = value; }
        }

        public void Add(Point pt)
        {
            if (pt == null) return;
            m_Pts.Add(pt);
        }

        public List<Point> Nodes
        {
            get 
            {
                return m_Pts;
            }
        }

        public void ClearNodes()
        {
            m_Pts.Clear();
        }
    }
}
