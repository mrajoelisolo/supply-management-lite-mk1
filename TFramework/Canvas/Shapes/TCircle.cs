using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace TFramework.Canvas.Shapes
{
    public class TCircle : TEllipse
    {
        public TCircle(int x, int y, int r):base(x - r, y - r, 2 * r, 2 * r) { }

        public int Radius
        {
            get { return m_Bounds.Width/2; }
            set { 
                m_Bounds.Width = value * 2;
                m_Bounds.Height = value * 2;
            }
        }

        private Point m_PBuf = new Point();
        public Point GetLocation()
        {
            m_PBuf.X = m_Bounds.X + Radius;
            m_PBuf.Y = m_Bounds.Y + Radius;

            return m_PBuf;
        }

        public void SetLocation(int x, int y)
        {
            m_Bounds.X = x - Radius;
            m_Bounds.Y = y - Radius;
        }
    }
}
