using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace TFramework.Canvas.Shapes
{
    public interface IShape
    {
        void Draw(Graphics g);
    }
}
