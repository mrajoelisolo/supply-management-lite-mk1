using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace TFramework.Canvas.Shapes
{
    public class TPolygon : TAbstractShape, IShape
    {
        protected Point[] m_Points;

        public TPolygon() { }

        public TPolygon(Point[] points)
        {
            m_Points = points;
        }

        public TPolygon(List<Point> pts) : this(pts.ToArray()) { }

        public void Draw(System.Drawing.Graphics g)
        {
            if (m_Points == null || m_Points.Length == 0) return;

            if (base.Filled)
                g.FillPolygon(base.BrushStyle, m_Points);

            g.DrawPolygon(base.PenStyle, m_Points);
        }

        public Point GetNodeAt(int i)
        {
            if (i < m_Points.Length)
                return m_Points[i];

            return m_Points[m_Points.Length - 1];
        }

        public void SetNodeAt(int i, Point value)
        {
            if (i < m_Points.Length)
                return;

            m_Points[i] = value;
        }

        public void SetNodes(List<Point> nodes)
        {
            m_Points = null;
            m_Points = nodes.ToArray();
        }

        public void ClearNodes()
        {
            m_Points = null;
        }

        private List<Point> tNodes = new List<Point>();
        public List<Point> Nodes
        {
            get
            {
                tNodes.Clear();

                if (m_Points == null) return tNodes;
                foreach(Point p in m_Points)
                    tNodes.Add(p);

                return tNodes;
            }
        }
        }
}