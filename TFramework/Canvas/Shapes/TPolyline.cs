using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace TFramework.Canvas.Shapes
{
    public class TPolyline : TAbstractShape, IShape
    {
        protected Point[] m_Points;

        public TPolyline(Point[] points)
        {
            m_Points = points;
        }

        public TPolyline(List<Point> pts) : this(pts.ToArray()) { }

        public void Draw(System.Drawing.Graphics g)
        {
            if (m_Points == null) return;

            g.DrawLines(base.PenStyle, m_Points);
        }

        public Point GetNodeAt(int i)
        {
            if (i < m_Points.Length)
                return m_Points[i];

            return m_Points[m_Points.Length - 1];
        }

        public void SetNodeAt(int i, Point value)
        {
            if (i < m_Points.Length)
                return;

            m_Points[i] = value;
        }
    }
}
