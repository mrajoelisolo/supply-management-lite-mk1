using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace TFramework.Canvas.Shapes
{
    public abstract class TAbstractShape
    {        
        private Pen m_PenStyle = new Pen(Color.Black, 1f);
        public Pen PenStyle
        {
            get { return m_PenStyle; }
        }
        public float PenWidth
        {
            get { return m_PenStyle.Width; }
            set { m_PenStyle.Width = value; }
        }
        public Color PenColor
        {
            get { return m_PenStyle.Color; }
            set { m_PenStyle.Color = value; }
        }

        private Brush m_BrushStyle = Brushes.White;
        public Brush BrushStyle
        {
            get { return m_BrushStyle; }
            set { m_BrushStyle = value; }
        }       

        private Boolean m_Filled = true;
        public Boolean Filled
        {
            get { return m_Filled; }
            set { m_Filled = value; }
        }
    }
}
