using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace TFramework.Canvas.Shapes
{
    public class TEllipse : TAbstractShape, IShape
    {
        protected Rectangle m_Bounds = new Rectangle();

        public TEllipse(int x, int y, int wdt, int hgt)
        {
            SetBounds(x, y, wdt, hgt);
        }

        public void Draw(System.Drawing.Graphics g)
        {
            if (base.Filled)
                g.FillEllipse(base.BrushStyle, m_Bounds);

            g.DrawEllipse(base.PenStyle, m_Bounds);
        }

        public void SetBounds(int x, int y, int wdt, int hgt)
        {
            m_Bounds.X = x;
            m_Bounds.Y = y;
            m_Bounds.Width = wdt;
            m_Bounds.Height = hgt;
        }

        public Rectangle GetBounds()
        {
            return m_Bounds;
        }
    }
}
