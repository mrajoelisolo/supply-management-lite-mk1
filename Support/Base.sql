﻿CREATE DATABASE `supply_mg_lite` /*!40100 DEFAULT CHARACTER SET latin1 */;

DROP TABLE IF EXISTS `supply_mg_lite`.`element_entrees`;
CREATE TABLE  `supply_mg_lite`.`element_entrees` (
  `idelement_entrees` bigint(20) unsigned NOT NULL auto_increment,
  `libelle` varchar(255) default NULL,
  `pu` float default NULL,
  `qte` float default NULL,
  `unite` varchar(255) default NULL,
  `idhistorique_entrees` bigint(20) unsigned default NULL,
  `idstock` bigint(20) unsigned default NULL,
  `facteur` float default NULL,
  PRIMARY KEY  (`idelement_entrees`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `supply_mg_lite`.`element_sorties`;
CREATE TABLE  `supply_mg_lite`.`element_sorties` (
  `idelement_sorties` bigint(20) unsigned NOT NULL auto_increment,
  `libelle` varchar(255) default NULL,
  `pu` float default NULL,
  `qte` float default NULL,
  `unite` varchar(255) default NULL,
  `idhistorique_sorties` bigint(20) unsigned default NULL,
  `idstock` bigint(20) unsigned default NULL,
  `facteur` float default NULL,
  PRIMARY KEY  (`idelement_sorties`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `supply_mg_lite`.`fournisseur`;
CREATE TABLE  `supply_mg_lite`.`fournisseur` (
  `idFournisseur` bigint(20) unsigned NOT NULL auto_increment,
  `ref` varchar(255) default NULL,
  `designation` varchar(255) default NULL,
  `nStat` varchar(255) default NULL,
  `adresse` varchar(255) default NULL,
  `telephone` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  PRIMARY KEY  (`idFournisseur`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `supply_mg_lite`.`hist_paie_entrees`;
CREATE TABLE  `supply_mg_lite`.`hist_paie_entrees` (
  `idHist_paie_entrees` bigint(20) unsigned NOT NULL auto_increment,
  `idHistorique_entrees` bigint(20) unsigned default NULL,
  `datePaiement` date default NULL,
  `montant` float default NULL,
  PRIMARY KEY  (`idHist_paie_entrees`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `supply_mg_lite`.`hist_paie_sorties`;
CREATE TABLE  `supply_mg_lite`.`hist_paie_sorties` (
  `idHist_paie_sorties` bigint(20) unsigned NOT NULL auto_increment,
  `idHistorique_sorties` bigint(20) unsigned default NULL,
  `datePaiement` date default NULL,
  `montant` float default NULL,
  PRIMARY KEY  USING BTREE (`idHist_paie_sorties`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `supply_mg_lite`.`historique_entrees`;
CREATE TABLE  `supply_mg_lite`.`historique_entrees` (
  `idhistorique_Entrees` int(10) unsigned NOT NULL auto_increment,
  `dateCreation` date default NULL,
  `numero` varchar(255) default NULL,
  `idTiers` varchar(45) default NULL,
  `resteMontant` float default NULL,
  PRIMARY KEY  USING BTREE (`idhistorique_Entrees`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `supply_mg_lite`.`historique_sorties`;
CREATE TABLE  `supply_mg_lite`.`historique_sorties` (
  `idhistorique_sorties` bigint(20) unsigned NOT NULL auto_increment,
  `dateCreation` date default NULL,
  `numero` varchar(255) default NULL,
  `idTiers` varchar(45) default NULL,
  `resteMontant` float default NULL,
  PRIMARY KEY  (`idhistorique_sorties`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `supply_mg_lite`.`stock`;
CREATE TABLE  `supply_mg_lite`.`stock` (
  `idStock` bigint(20) unsigned NOT NULL auto_increment,
  `libelle` varchar(255) default NULL,
  `pu_achat` float default NULL,
  `pu` float default NULL,
  `qte` float default NULL,
  `unite` varchar(255) default NULL,
  PRIMARY KEY  (`idStock`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `supply_mg_lite`.`tiers`;
CREATE TABLE  `supply_mg_lite`.`tiers` (
  `idTiers` bigint(20) unsigned NOT NULL auto_increment,
  `ref` varchar(255) default NULL,
  `nom` varchar(255) default NULL,
  `sexe` varchar(255) default NULL,
  `adresse` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `tel` varchar(255) default NULL,
  PRIMARY KEY  (`idTiers`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `supply_mg_lite`.`unitestock`;
CREATE TABLE  `supply_mg_lite`.`unitestock` (
  `idUniteStock` bigint(20) unsigned NOT NULL auto_increment,
  `idStock` bigint(20) unsigned NOT NULL,
  `nom` varchar(255) default NULL,
  `facteur` varchar(255) default NULL,
  PRIMARY KEY  (`idUniteStock`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;