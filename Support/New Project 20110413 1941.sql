-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.67-community-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema supply_mg_lite
--

CREATE DATABASE IF NOT EXISTS supply_mg_lite;
USE supply_mg_lite;

--
-- Definition of table `element_entrees`
--

DROP TABLE IF EXISTS `element_entrees`;
CREATE TABLE `element_entrees` (
  `idelement_entrees` bigint(20) unsigned NOT NULL auto_increment,
  `libelle` varchar(255) default NULL,
  `pu` float default NULL,
  `qte` float default NULL,
  `unite` varchar(255) default NULL,
  `facteur` float default NULL,
  `idhistorique_entrees` bigint(20) unsigned default NULL,
  `idstock` bigint(20) unsigned default NULL,
  PRIMARY KEY  (`idelement_entrees`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `element_entrees`
--

/*!40000 ALTER TABLE `element_entrees` DISABLE KEYS */;
INSERT INTO `element_entrees` (`idelement_entrees`,`libelle`,`pu`,`qte`,`unite`,`facteur`,`idhistorique_entrees`,`idstock`) VALUES 
 (14,'Gold 80cl',1800,5,'Cageot de 16',16,5,20),
 (15,'Gouty GM',450,4,'Boite de 20',20,6,18),
 (16,'Pain GM',250,100,'Pièce',1,7,21),
 (17,'Huile tourneol Wasfa',2600,5,'Carton de 10',10,7,22),
 (18,'Pates matsiro',450,30,'Sachet',1,7,24),
 (19,'Pates indomie',450,30,'Sachet',1,7,25),
 (20,'Pates esko',350,40,'Sachet',1,7,26),
 (21,'Chili sauce',1500,10,'Bouteille',1,7,27),
 (22,'Sauce soya',1600,10,'Bouteille',1,7,28),
 (23,'Sucre',2600,20,'Sachet 1kg',1,7,29),
 (24,'Vary gasy',500,6,'Sac de 60kg',60,7,23),
 (25,'Sel',100,10,'Sachet',1,7,31),
 (26,'Oeuf',250,5,'Boite de 20',20,7,30);
/*!40000 ALTER TABLE `element_entrees` ENABLE KEYS */;


--
-- Definition of table `element_sorties`
--

DROP TABLE IF EXISTS `element_sorties`;
CREATE TABLE `element_sorties` (
  `idelement_sorties` bigint(20) unsigned NOT NULL auto_increment,
  `libelle` varchar(255) default NULL,
  `pu` float default NULL,
  `qte` float default NULL,
  `unite` varchar(255) default NULL,
  `facteur` float default NULL,
  `idhistorique_sorties` bigint(20) unsigned default NULL,
  `idstock` bigint(20) unsigned default NULL,
  PRIMARY KEY  (`idelement_sorties`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `element_sorties`
--

/*!40000 ALTER TABLE `element_sorties` DISABLE KEYS */;
INSERT INTO `element_sorties` (`idelement_sorties`,`libelle`,`pu`,`qte`,`unite`,`facteur`,`idhistorique_sorties`,`idstock`) VALUES 
 (10,'Vary gasy',600,1,'Sac de 60kg',60,3,23),
 (11,'Gouty GM',500,25,'Sachet',1,4,18),
 (12,'Pates indomie',500,10,'Sachet',1,4,25),
 (13,'Chili sauce',1600,5,'Bouteille',1,4,27),
 (14,'Pates esko',400,12,'Sachet',1,5,26),
 (15,'Pates indomie',500,13,'Sachet',1,5,25),
 (16,'Pates matsiro',500,14,'Sachet',1,5,24),
 (17,'Sucre',2700,1,'Sachet 1kg',1,6,29),
 (18,'Gouty GM',500,4,'Sachet',1,6,18),
 (19,'Chili sauce',1600,1,'Bouteille',1,6,27),
 (20,'Gold 80cl',1900,1,'Cageot de 16',16,7,20),
 (21,'Gold 80cl',1900,1,'Bouteille 80cl',1,8,20);
/*!40000 ALTER TABLE `element_sorties` ENABLE KEYS */;


--
-- Definition of table `fournisseur`
--

DROP TABLE IF EXISTS `fournisseur`;
CREATE TABLE `fournisseur` (
  `idFournisseur` bigint(20) unsigned NOT NULL auto_increment,
  `ref` varchar(255) default NULL,
  `designation` varchar(255) default NULL,
  `nStat` varchar(255) default NULL,
  `adresse` varchar(255) default NULL,
  `telephone` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  PRIMARY KEY  (`idFournisseur`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fournisseur`
--

/*!40000 ALTER TABLE `fournisseur` DISABLE KEYS */;
INSERT INTO `fournisseur` (`idFournisseur`,`ref`,`designation`,`nStat`,`adresse`,`telephone`,`email`) VALUES 
 (7,'STAR TANJOMBATO','STAR',NULL,NULL,NULL,NULL),
 (8,'JB SARL','JB',NULL,NULL,NULL,NULL),
 (9,'RAKOTO & FILS',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `fournisseur` ENABLE KEYS */;


--
-- Definition of table `hist_paie_entrees`
--

DROP TABLE IF EXISTS `hist_paie_entrees`;
CREATE TABLE `hist_paie_entrees` (
  `idHist_paie_entrees` bigint(20) unsigned NOT NULL auto_increment,
  `idHistorique_entrees` bigint(20) unsigned default NULL,
  `datePaiement` date default NULL,
  `montant` float default NULL,
  PRIMARY KEY  (`idHist_paie_entrees`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hist_paie_entrees`
--

/*!40000 ALTER TABLE `hist_paie_entrees` DISABLE KEYS */;
INSERT INTO `hist_paie_entrees` (`idHist_paie_entrees`,`idHistorique_entrees`,`datePaiement`,`montant`) VALUES 
 (9,5,'2011-04-12',80000),
 (10,7,'2011-04-12',300000),
 (11,7,'2011-04-13',85000);
/*!40000 ALTER TABLE `hist_paie_entrees` ENABLE KEYS */;


--
-- Definition of table `hist_paie_sorties`
--

DROP TABLE IF EXISTS `hist_paie_sorties`;
CREATE TABLE `hist_paie_sorties` (
  `idHist_paie_sorties` bigint(20) unsigned NOT NULL auto_increment,
  `idHistorique_sorties` bigint(20) unsigned default NULL,
  `datePaiement` date default NULL,
  `montant` float default NULL,
  PRIMARY KEY  USING BTREE (`idHist_paie_sorties`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hist_paie_sorties`
--

/*!40000 ALTER TABLE `hist_paie_sorties` DISABLE KEYS */;
INSERT INTO `hist_paie_sorties` (`idHist_paie_sorties`,`idHistorique_sorties`,`datePaiement`,`montant`) VALUES 
 (4,3,'2011-04-12',30000),
 (5,5,'2011-04-12',15000);
/*!40000 ALTER TABLE `hist_paie_sorties` ENABLE KEYS */;


--
-- Definition of table `historique_entrees`
--

DROP TABLE IF EXISTS `historique_entrees`;
CREATE TABLE `historique_entrees` (
  `idhistorique_Entrees` int(10) unsigned NOT NULL auto_increment,
  `dateCreation` date default NULL,
  `numero` varchar(255) default NULL,
  `idTiers` varchar(45) default NULL,
  `resteMontant` float default NULL,
  PRIMARY KEY  USING BTREE (`idhistorique_Entrees`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `historique_entrees`
--

/*!40000 ALTER TABLE `historique_entrees` DISABLE KEYS */;
INSERT INTO `historique_entrees` (`idhistorique_Entrees`,`dateCreation`,`numero`,`idTiers`,`resteMontant`) VALUES 
 (5,'2011-04-12','5','7',64000),
 (6,'2011-04-12','6','8',0),
 (7,'2011-04-12','7','9',100000);
/*!40000 ALTER TABLE `historique_entrees` ENABLE KEYS */;


--
-- Definition of table `historique_sorties`
--

DROP TABLE IF EXISTS `historique_sorties`;
CREATE TABLE `historique_sorties` (
  `idhistorique_sorties` bigint(20) unsigned NOT NULL auto_increment,
  `dateCreation` date default NULL,
  `numero` varchar(255) default NULL,
  `idTiers` varchar(45) default NULL,
  `resteMontant` float default NULL,
  PRIMARY KEY  (`idhistorique_sorties`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `historique_sorties`
--

/*!40000 ALTER TABLE `historique_sorties` DISABLE KEYS */;
INSERT INTO `historique_sorties` (`idhistorique_sorties`,`dateCreation`,`numero`,`idTiers`,`resteMontant`) VALUES 
 (3,'2011-04-12','3','4',6000),
 (4,'2011-04-12','4','5',0),
 (5,'2011-04-12','5','4',3300),
 (6,'2011-04-13','6','6',0),
 (7,'2011-04-13','7','4',0),
 (8,'2011-04-13','8','4',0);
/*!40000 ALTER TABLE `historique_sorties` ENABLE KEYS */;


--
-- Definition of table `stock`
--

DROP TABLE IF EXISTS `stock`;
CREATE TABLE `stock` (
  `idStock` bigint(20) unsigned NOT NULL auto_increment,
  `libelle` varchar(255) default NULL,
  `pu_achat` float default NULL,
  `pu` float default NULL,
  `qte` float default NULL,
  `unite` varchar(255) default NULL,
  PRIMARY KEY  (`idStock`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

/*!40000 ALTER TABLE `stock` DISABLE KEYS */;
INSERT INTO `stock` (`idStock`,`libelle`,`pu_achat`,`pu`,`qte`,`unite`) VALUES 
 (18,'Gouty GM',450,500,61,'Sachet'),
 (20,'Gold 80cl',1800,1900,63,'Bouteille 80cl'),
 (21,'Pain GM',250,300,100,'Pièce'),
 (22,'Huile tourneol Wasfa',2600,2700,50,'Bouteille 1L'),
 (23,'Vary gasy',500,600,300,'Kg'),
 (24,'Pates matsiro',450,500,16,'Sachet'),
 (25,'Pates indomie',450,500,7,'Sachet'),
 (26,'Pates esko',350,400,28,'Sachet'),
 (27,'Chili sauce',1500,1600,4,'Bouteille'),
 (28,'Sauce soya',1600,1700,10,'Bouteille'),
 (29,'Sucre',2600,2700,19,'Sachet 1kg'),
 (30,'Oeuf',250,300,100,'Pièce'),
 (31,'Sel',100,150,10,'Sachet');
/*!40000 ALTER TABLE `stock` ENABLE KEYS */;


--
-- Definition of table `tiers`
--

DROP TABLE IF EXISTS `tiers`;
CREATE TABLE `tiers` (
  `idTiers` bigint(20) unsigned NOT NULL auto_increment,
  `ref` varchar(255) default NULL,
  `nom` varchar(255) default NULL,
  `sexe` varchar(255) default NULL,
  `adresse` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `tel` varchar(255) default NULL,
  PRIMARY KEY  (`idTiers`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tiers`
--

/*!40000 ALTER TABLE `tiers` DISABLE KEYS */;
INSERT INTO `tiers` (`idTiers`,`ref`,`nom`,`sexe`,`adresse`,`email`,`tel`) VALUES 
 (4,'RAJOELISOLO','Mitanjo',NULL,NULL,NULL,NULL),
 (5,'RASOA','Solange',NULL,NULL,NULL,NULL),
 (6,'RANORO','Norine',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tiers` ENABLE KEYS */;


--
-- Definition of table `unitestock`
--

DROP TABLE IF EXISTS `unitestock`;
CREATE TABLE `unitestock` (
  `idUniteStock` bigint(20) unsigned NOT NULL auto_increment,
  `idStock` bigint(20) unsigned NOT NULL,
  `nom` varchar(255) default NULL,
  `facteur` varchar(255) default NULL,
  PRIMARY KEY  (`idUniteStock`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unitestock`
--

/*!40000 ALTER TABLE `unitestock` DISABLE KEYS */;
INSERT INTO `unitestock` (`idUniteStock`,`idStock`,`nom`,`facteur`) VALUES 
 (4,18,'Boite de 20','20'),
 (6,20,'Cageot de 16','16'),
 (7,22,'Carton de 10','10'),
 (8,23,'Sac de 60kg','60'),
 (9,30,'Boite de 20','20');
/*!40000 ALTER TABLE `unitestock` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
