using System;
using System.Collections.Generic;
using System.Text;

namespace TFramework.Utilities
{
    public class DigitToLetters
    {
        private static DigitToLetters m_Instance = new DigitToLetters();

        private DigitToLetters() { }

        public static DigitToLetters Instance
        {
            get
            {
                return m_Instance;
            }
        }

        private String Nb2Str(long number)
        { 
            switch(number)
            {
                case 1 : return "UN";
                case 2 : return "DEUX";
                case 3 : return "TROIS";
                case 4 : return "QUATRE";
                case 5 : return "CINQ";
                case 6 : return "SIX";
                case 7 : return "SEPT";
                case 8 : return "HUIT";
                case 9 : return "NEUF";
                case 10 : return "DIX";
                case 11 : return "ONZE";
                case 12 : return "DOUZE";
                case 13 : return "TREIZE";
                case 14 : return "QUATORZE";
                case 15 : return "QUINZE";
                case 16 : return "SEIZE";
                case 17 : return "DIX SEPT";
                case 18 : return "DIX HUIT";
                case 19 : return "DIX NEUF";
                case 20 : return "VINGT";
                case 30 : return "TRENTE";
                case 40 : return "QUARANTE";
                case 50 : return "CINQUANTE";
                case 60 : return "SOIXANTE";
                case 70 : return "SOIXANTE";
                case 80 : return "QUATRE VINGT";
                case 90 : return "QUATRE VINGT";
                case 100 : return "CENT";
            }

            return "ERROR";
        }

        private String ToCentaine(long N100)
        {
            //Dim Q1 As Long
            long Q1 = 0;
            //Static str100 As String = ""
            String str100 = "";
            //Dim N10 As Double, Q100 As Double, Q10 As Double
            long N10 = 0, Q100 = 0, Q10 = 0;
            //Q100 = Div(N100, 100)
            Q100 = Div(N100, 100);//N100/100;
            //N10 = N100 Mod 100
            N10 = N100%100;
            //Select Case N10
            //    Case 1 To 20
            //        Q10 = 0 : Q1 = N10
            //    Case 21 To 69, 80 To 89
            //        Q10 = N10 - N10 Mod 10 : Q1 = N10 Mod 10
            //    Case 70 To 79, 90 To 99
            //        Q10 = Div(N10, 10) * 10 : Q1 = 10 + N10 Mod 10
            //End Select

            if (N10 >= 0 && N10 <= 20)
            {
                Q10 = 0; Q1 = (long)N10;
            }
            else if ((N10 >= 21 && N10 <= 69) || (N10 >= 80 && N10 <= 89))
            {
                Q10 = N10 - N10%10;
                Q1 = (long)N10%10;
            }
            else if ((N10 >= 70 && N10 <= 79) || (N10 >= 90 && N10 <= 99))
            {
                Q10 = (N10 / 10) * 10;
                Q1 = (long)(10 + N10 % 10);
            }

            //If Q100 <> 0 Then
            //    If Q100 <> 1 Then str100 = Num2Str(Q100)
            //    str100 = str100 & " " & " CENT"
            //End If
            //If Q10 <> 0 Then
            //    str100 = str100 & " " & Num2Str(Q10)
            //End If
            //If Q1 <> 0 Then
            //    str100 = str100 & " " & Num2Str(Q1)
            //End If
            //CENTAINE = str100

            if (Q100 != 0)
            {
                if (Q100 != 1) str100 = Nb2Str(Q100);
                str100 = str100 + " " + " CENT";
            }
            if (Q10 != 0)
            {
                str100 = str100 + " " + Nb2Str(Q10);
            }
            if (Q1 != 0)
            {
                str100 = str100 + " " + Nb2Str(Q1);
            }

            return str100;
        }

        public String ToCommercialAmount(double value, String amountUnit)
        {
            //Return Chiffre2Lettre.NBLT(a).ToUpper + " " + unite + " (" + Format(a, "#,##0") + " " + unite + ")"
            return "";
        }

        private static long Div(long n, long d)
        {
            return (n - n%d) / d;
        }
    }
}
