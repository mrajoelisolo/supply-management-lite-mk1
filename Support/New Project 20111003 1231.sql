-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.67-community-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema supply_mg_lite
--

CREATE DATABASE IF NOT EXISTS supply_mg_lite;
USE supply_mg_lite;

--
-- Definition of table `element_entrees`
--

DROP TABLE IF EXISTS `element_entrees`;
CREATE TABLE `element_entrees` (
  `idelement_entrees` bigint(20) unsigned NOT NULL auto_increment,
  `libelle` varchar(255) default NULL,
  `pu` float default NULL,
  `qte` float default NULL,
  `unite` varchar(255) default NULL,
  `facteur` float default NULL,
  `idhistorique_entrees` bigint(20) unsigned default NULL,
  `idstock` bigint(20) unsigned default NULL,
  PRIMARY KEY  (`idelement_entrees`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `element_entrees`
--

/*!40000 ALTER TABLE `element_entrees` DISABLE KEYS */;
/*!40000 ALTER TABLE `element_entrees` ENABLE KEYS */;


--
-- Definition of table `element_sorties`
--

DROP TABLE IF EXISTS `element_sorties`;
CREATE TABLE `element_sorties` (
  `idelement_sorties` bigint(20) unsigned NOT NULL auto_increment,
  `libelle` varchar(255) default NULL,
  `pu` float default NULL,
  `qte` float default NULL,
  `unite` varchar(255) default NULL,
  `facteur` float default NULL,
  `idhistorique_sorties` bigint(20) unsigned default NULL,
  `idstock` bigint(20) unsigned default NULL,
  PRIMARY KEY  (`idelement_sorties`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `element_sorties`
--

/*!40000 ALTER TABLE `element_sorties` DISABLE KEYS */;
INSERT INTO `element_sorties` (`idelement_sorties`,`libelle`,`pu`,`qte`,`unite`,`facteur`,`idhistorique_sorties`,`idstock`) VALUES 
 (10,'gaufy',2000,5,'sachet',1,3,19),
 (11,'salto GM',3000,10,'Sachet',1,4,20),
 (12,'gaufy',2000,5,'sachet',1,4,19),
 (13,'salto GM',3000,5,'Sachet',1,5,20),
 (14,'gaufy',2000,2,'sachet',1,5,19);
/*!40000 ALTER TABLE `element_sorties` ENABLE KEYS */;


--
-- Definition of table `fournisseur`
--

DROP TABLE IF EXISTS `fournisseur`;
CREATE TABLE `fournisseur` (
  `idFournisseur` bigint(20) unsigned NOT NULL auto_increment,
  `ref` varchar(255) default NULL,
  `designation` varchar(255) default NULL,
  `nStat` varchar(255) default NULL,
  `adresse` varchar(255) default NULL,
  `telephone` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  PRIMARY KEY  (`idFournisseur`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fournisseur`
--

/*!40000 ALTER TABLE `fournisseur` DISABLE KEYS */;
INSERT INTO `fournisseur` (`idFournisseur`,`ref`,`designation`,`nStat`,`adresse`,`telephone`,`email`) VALUES 
 (1,'SAMY','SAMY',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `fournisseur` ENABLE KEYS */;


--
-- Definition of table `hist_paie_entrees`
--

DROP TABLE IF EXISTS `hist_paie_entrees`;
CREATE TABLE `hist_paie_entrees` (
  `idHist_paie_entrees` bigint(20) unsigned NOT NULL auto_increment,
  `idHistorique_entrees` bigint(20) unsigned default NULL,
  `datePaiement` date default NULL,
  `montant` float default NULL,
  PRIMARY KEY  (`idHist_paie_entrees`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hist_paie_entrees`
--

/*!40000 ALTER TABLE `hist_paie_entrees` DISABLE KEYS */;
/*!40000 ALTER TABLE `hist_paie_entrees` ENABLE KEYS */;


--
-- Definition of table `hist_paie_sorties`
--

DROP TABLE IF EXISTS `hist_paie_sorties`;
CREATE TABLE `hist_paie_sorties` (
  `idHist_paie_sorties` bigint(20) unsigned NOT NULL auto_increment,
  `idHistorique_sorties` bigint(20) unsigned default NULL,
  `datePaiement` date default NULL,
  `montant` float default NULL,
  PRIMARY KEY  USING BTREE (`idHist_paie_sorties`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hist_paie_sorties`
--

/*!40000 ALTER TABLE `hist_paie_sorties` DISABLE KEYS */;
INSERT INTO `hist_paie_sorties` (`idHist_paie_sorties`,`idHistorique_sorties`,`datePaiement`,`montant`) VALUES 
 (4,3,'2011-08-16',5000),
 (5,4,'2011-09-25',20000);
/*!40000 ALTER TABLE `hist_paie_sorties` ENABLE KEYS */;


--
-- Definition of table `historique_entrees`
--

DROP TABLE IF EXISTS `historique_entrees`;
CREATE TABLE `historique_entrees` (
  `idhistorique_Entrees` int(10) unsigned NOT NULL auto_increment,
  `dateCreation` date default NULL,
  `numero` varchar(255) default NULL,
  `idTiers` varchar(45) default NULL,
  `resteMontant` float default NULL,
  PRIMARY KEY  USING BTREE (`idhistorique_Entrees`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `historique_entrees`
--

/*!40000 ALTER TABLE `historique_entrees` DISABLE KEYS */;
/*!40000 ALTER TABLE `historique_entrees` ENABLE KEYS */;


--
-- Definition of table `historique_sorties`
--

DROP TABLE IF EXISTS `historique_sorties`;
CREATE TABLE `historique_sorties` (
  `idhistorique_sorties` bigint(20) unsigned NOT NULL auto_increment,
  `dateCreation` date default NULL,
  `numero` varchar(255) default NULL,
  `idTiers` varchar(45) default NULL,
  `resteMontant` float default NULL,
  PRIMARY KEY  (`idhistorique_sorties`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `historique_sorties`
--

/*!40000 ALTER TABLE `historique_sorties` DISABLE KEYS */;
INSERT INTO `historique_sorties` (`idhistorique_sorties`,`dateCreation`,`numero`,`idTiers`,`resteMontant`) VALUES 
 (3,'2011-08-16','3','4',5000),
 (4,'2011-09-25','4','4',20000),
 (5,'2011-09-28','5','5',0);
/*!40000 ALTER TABLE `historique_sorties` ENABLE KEYS */;


--
-- Definition of table `profile`
--

DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `idProfile` int(10) unsigned NOT NULL auto_increment,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `login` varchar(45) NOT NULL,
  `pwd` varchar(45) NOT NULL,
  `privilege` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`idProfile`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile`
--

/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` (`idProfile`,`firstName`,`lastName`,`login`,`pwd`,`privilege`) VALUES 
 (1,'Mitanjo','RAJOELISOLO','mitanjo','hinata',0),
 (3,'RAMONS','MONS','ramone','test',1),
 (4,'LOUZA','LOOZA','VORONTSILOZA','VORONTSILOZA',2);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;


--
-- Definition of table `profile_profilegrant`
--

DROP TABLE IF EXISTS `profile_profilegrant`;
CREATE TABLE `profile_profilegrant` (
  `idprofilegrant` int(10) unsigned NOT NULL,
  `idprofile` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`idprofilegrant`,`idprofile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_profilegrant`
--

/*!40000 ALTER TABLE `profile_profilegrant` DISABLE KEYS */;
INSERT INTO `profile_profilegrant` (`idprofilegrant`,`idprofile`) VALUES 
 (1,1),
 (2,1),
 (3,1);
/*!40000 ALTER TABLE `profile_profilegrant` ENABLE KEYS */;


--
-- Definition of table `profilegrant`
--

DROP TABLE IF EXISTS `profilegrant`;
CREATE TABLE `profilegrant` (
  `idProfileGrant` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY  (`idProfileGrant`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profilegrant`
--

/*!40000 ALTER TABLE `profilegrant` DISABLE KEYS */;
INSERT INTO `profilegrant` (`idProfileGrant`,`name`) VALUES 
 (1,'stock'),
 (2,'entrees'),
 (3,'sorties'),
 (4,'benefices'),
 (5,'suivi');
/*!40000 ALTER TABLE `profilegrant` ENABLE KEYS */;


--
-- Definition of table `stock`
--

DROP TABLE IF EXISTS `stock`;
CREATE TABLE `stock` (
  `idStock` bigint(20) unsigned NOT NULL auto_increment,
  `libelle` varchar(255) default NULL,
  `pu_achat` float default NULL,
  `pu` float default NULL,
  `qte` float default NULL,
  `unite` varchar(255) default NULL,
  PRIMARY KEY  (`idStock`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

/*!40000 ALTER TABLE `stock` DISABLE KEYS */;
INSERT INTO `stock` (`idStock`,`libelle`,`pu_achat`,`pu`,`qte`,`unite`) VALUES 
 (18,'gouty',1200,1500,10,'sachet'),
 (19,'gaufy',1099,2000,2,'sachet'),
 (20,'salto GM',2500,3000,5,'Sachet'),
 (21,'Minichoco Noir',900,1000,100,'Sachet');
/*!40000 ALTER TABLE `stock` ENABLE KEYS */;


--
-- Definition of table `tiers`
--

DROP TABLE IF EXISTS `tiers`;
CREATE TABLE `tiers` (
  `idTiers` bigint(20) unsigned NOT NULL auto_increment,
  `ref` varchar(255) default NULL,
  `nom` varchar(255) default NULL,
  `sexe` varchar(255) default NULL,
  `adresse` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `tel` varchar(255) default NULL,
  PRIMARY KEY  (`idTiers`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tiers`
--

/*!40000 ALTER TABLE `tiers` DISABLE KEYS */;
INSERT INTO `tiers` (`idTiers`,`ref`,`nom`,`sexe`,`adresse`,`email`,`tel`) VALUES 
 (4,'rakoto','rakoto',NULL,NULL,NULL,NULL),
 (5,'mons','mons',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tiers` ENABLE KEYS */;


--
-- Definition of table `unitestock`
--

DROP TABLE IF EXISTS `unitestock`;
CREATE TABLE `unitestock` (
  `idUniteStock` bigint(20) unsigned NOT NULL auto_increment,
  `idStock` bigint(20) unsigned NOT NULL,
  `nom` varchar(255) default NULL,
  `facteur` varchar(255) default NULL,
  PRIMARY KEY  (`idUniteStock`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unitestock`
--

/*!40000 ALTER TABLE `unitestock` DISABLE KEYS */;
INSERT INTO `unitestock` (`idUniteStock`,`idStock`,`nom`,`facteur`) VALUES 
 (1,21,'Paquets de 30','30');
/*!40000 ALTER TABLE `unitestock` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
