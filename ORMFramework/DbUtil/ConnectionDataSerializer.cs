using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace ORMFramework.DbUtil
{
    public class ConnectionDataSerializer
    {
        private static ConnectionDataSerializer m_Instance = new ConnectionDataSerializer();
        private String m_Path = "connection.xml";

        private ConnectionDataSerializer() { }

        public static ConnectionDataSerializer Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public void Serialize(ConnectionDatasource cda)
        {
            if (cda == null)
            {                
                return; 
            }

            ConnectionDatasource _cda = new ConnectionDatasource();
            _cda.Server = cda.Server;
            _cda.Database = cda.Database;
            _cda.User = cda.User;

            XmlSerializer writer = new XmlSerializer(typeof(ConnectionDatasource));
            StreamWriter file = new StreamWriter(m_Path);
            writer.Serialize(file, _cda);
            file.Close();
        }

        public void SerializeWithPassword(ConnectionDatasource cda)
        {
            XmlSerializer writer = new XmlSerializer(typeof(ConnectionDatasource));
            StreamWriter file = new StreamWriter(m_Path);
            writer.Serialize(file, cda);
            file.Close();
        }

        public ConnectionDatasource Deserialize()
        {
            ConnectionDatasource res = null;

            try
            {
                XmlSerializer reader = new XmlSerializer(typeof(ConnectionDatasource));
                StreamReader file = new StreamReader(m_Path);
                res = (ConnectionDatasource)reader.Deserialize(file);
                file.Close();
            }
            catch (Exception ex) 
            {
                res = new ConnectionDatasource();
                System.Console.WriteLine(ex.Message);
            }

            return res;
        }
    }
}
