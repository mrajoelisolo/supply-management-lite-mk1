using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;

namespace ORMFramework.DbUtil
{
    public class ConnectionManager
    {
        private static ConnectionManager m_Instance = new ConnectionManager();
        private ConnectionDatasource m_ConnectionData;

        private ConnectionManager()
        {
        }

        public static ConnectionManager Instance {
            get
            {
                return m_Instance;
            }
        }

        public String GetConnectionString(ConnectionDatasource cda)
        {
            return "SERVER=" + cda.Server + "; DATABASE=" + cda.Database + "; USER=" + cda.User + "; PASSWORD=" + cda.Password + ";";
        }

        public Boolean IsInitialized
        {
            get
            {
                return String.IsNullOrEmpty(m_ConnectionData.Database) &&
                    String.IsNullOrEmpty(m_ConnectionData.Password) &&
                        String.IsNullOrEmpty(m_ConnectionData.Server) &&
                            String.IsNullOrEmpty(m_ConnectionData.User);
            }
        }

        public enum EnumConnectionResult
        {
            LOGGED, LOGON_FAILED
        }

        public EnumConnectionResult Connect(ConnectionDatasource cda)
        {
            try
            {
                if (cda == null) throw new Exception("The connection is not initialized.");
                m_ConnectionData = cda;

                String strCon = GetConnectionString(cda);
                MySqlConnection cnx = new MySqlConnection(strCon);
                cnx.Open();
                cnx.Close();                
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                //throw new Exception(e.Message);
                return EnumConnectionResult.LOGON_FAILED;                
            }
            return EnumConnectionResult.LOGGED;
        }

        public MySqlConnection CurrentConnection
        {
            get
            {
                if (m_ConnectionData == null) throw new Exception("The connection is not initialized.");
                return new MySqlConnection(GetConnectionString(m_ConnectionData));
            }
        }

        public ConnectionDatasource CurrentConnectionDatasource
        {
            get
            {
                return m_ConnectionData;
            }
        }
    }
}
