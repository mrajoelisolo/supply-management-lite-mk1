using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework.DbUtil
{
    public class ConnectionDatasource
    {
        private String m_Server;

        public String Server
        {
            get { return m_Server; }
            set { m_Server = value; }
        }

        private String m_database;

        public String Database
        {
            get { return m_database; }
            set { m_database = value; }
        }

        private String m_user;

        public String User
        {
            get { return m_user; }
            set { m_user = value; }
        }

        private String m_password;

        public String Password
        {
            get { return m_password; }
            set { m_password = value; }
        }
    }
}
