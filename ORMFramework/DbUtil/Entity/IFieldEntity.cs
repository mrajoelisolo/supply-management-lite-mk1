using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework.DbUtil.Entity
{
    public interface IFieldEntity
    {
        String GetFieldName();
        void SetFieldName(String fieldName);
        Type GetFieldType();
        void SetFieldType(Type fieldType);
        String GetColName();
        void SetColName(String colName);
    }
}
