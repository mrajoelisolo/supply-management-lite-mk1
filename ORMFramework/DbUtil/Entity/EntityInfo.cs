using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework.DbUtil.Entity
{
    public class EntityInfo
    {
        private String m_EntityName;

        public String EntityName
        {
            get { return m_EntityName; }
            set { m_EntityName = value; }
        }

        private String m_Package;
                
        public String Package
        {
            get { return m_Package; }
            set { m_Package = value; }
        }

        private String m_TableName;

        public String TableName
        {
            get { return m_TableName; }
            set { m_TableName = value; }
        }

        private List<IFieldEntity> m_Fields = new List<IFieldEntity>();

        public List<IFieldEntity> Fields
        {
            get { return m_Fields; }
            set { m_Fields = value; }
        }
	
    }
}
