using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework.DbUtil.Entity
{
    public class FieldPrimaryKey : IFieldEntity
    {
        private String m_FieldName;
        private Type m_FieldType;
        private String m_ColName;

        public string GetFieldName()
        {
            return m_FieldName;
        }

        public void SetFieldName(string fieldName)
        {
            m_FieldName = fieldName;
        }

        public Type GetFieldType()
        {
            return m_FieldType;
        }

        public void SetFieldType(Type fieldType)
        {
            m_FieldType = fieldType;
        }

        public string GetColName()
        {
            return m_ColName;
        }

        public void SetColName(string colName)
        {
            m_ColName = colName;
        }
    }
}
