using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using ORMFramework.DbUtil.Entity.Query;

namespace ORMFramework.DbUtil.Entity
{
    public class EntityLoader
    {
        private static EntityLoader m_Instance = new EntityLoader();
        private EntitySchema m_Schema = null;

        private EntityLoader() { }

        public static EntityLoader Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public void SetSchema(EntitySchema schema)
        {
            m_Schema = schema;
        }

        public List<Object> LoadData(Type entityType)
        {
            return LoadData(entityType, "");
        }

        public List<Object> LoadData(Type entityType, String whereClause)
        {
            List<Object> res = new List<Object>();

            if (m_Schema == null) throw new Exception("The schema is not initialized.");

            EntitySchema schema = m_Schema;
            EntityInfo ei = EntityUtil.Instance.GetEntityInfo(entityType, schema);
            //Type t = Reflector.Instance.ReadType("SupplyManagementMk1Sharp", ei.Package + "." + ei.EntityName);
            MySqlConnection cnx = ConnectionManager.Instance.CurrentConnection;
            MySqlCommand cmd = cnx.CreateCommand();
            String szWhere = "";

            if (!String.IsNullOrEmpty(whereClause))
                szWhere += " where " + whereClause;

            cmd.CommandText = "Select * from " + ei.TableName + szWhere + ";";

            cnx.Open();

            try
            {
                MySqlDataReader reader = cmd.ExecuteReader();
         
                while (reader.Read())
                {
                    Object o = Reflector.Instance.CreateInstance(entityType); // t -> entityType

                    foreach (IFieldEntity fe in ei.Fields)
                    {
                        if (reader[fe.GetColName()] != null)
                        {
                            Object val;

                            try
                            {
                                val = System.Convert.ChangeType(reader[fe.GetColName()].ToString(), fe.GetFieldType());
                            }
                            catch (Exception ex)
                            {
                                val = reader[fe.GetColName()];
                                System.Console.WriteLine(ex.Message);
                            }
                            Reflector.Instance.SetPropertyValue(fe.GetFieldName(), o, val);
                        }else
                            Reflector.Instance.SetPropertyValue(fe.GetFieldName(), o, null);
                    }

                    res.Add(o);
                }

                cnx.Close();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                cnx.Close();
            }

            return res;
        }

        public Object LoadData(Type entityType, Object pk)
        {
            try {
                MySqlConnection cnx = ConnectionManager.Instance.CurrentConnection;
                EntityInfo ei = EntityUtil.Instance.GetEntityInfo(entityType, m_Schema);
                MySqlCommand cmd = cnx.CreateCommand();
                FieldPrimaryKey pkField = null;

                if(pk == null) return null;

                foreach(IFieldEntity fe in ei.Fields)
                    if(fe.GetType().Equals((typeof(FieldPrimaryKey))))
                        pkField = (FieldPrimaryKey) fe;

                cnx.Open();
                String query = "Select * From " + ei.TableName + " where " + pkField.GetColName() + " = '" + pk + "'";
                cmd.CommandText = query;
                MySqlDataReader reader = cmd.ExecuteReader();
                if (!reader.HasRows) return null;
                reader.Read();

                Object o = Reflector.Instance.CreateInstance(entityType); // t -> entityType
                foreach (IFieldEntity fe in ei.Fields)
                {
                    if (reader[fe.GetColName()] != null)
                    {
                        Object val;

                        try
                        {
                            val = System.Convert.ChangeType(reader[fe.GetColName()].ToString(), fe.GetFieldType());
                        }
                        catch (Exception ex)
                        {
                            val = reader[fe.GetColName()];
                            System.Console.WriteLine(ex.Message);
                        }
                        Reflector.Instance.SetPropertyValue(fe.GetFieldName(), o, val);
                    }
                    else
                        Reflector.Instance.SetPropertyValue(fe.GetFieldName(), o, null);
                }              

                cnx.Close();

                return o;
            }
            catch (Exception e)
            {
                ConnectionManager.Instance.CurrentConnection.Close();
                throw new Exception(e.Message);                
            }
        }

        public List<Object> LoadData(Type entityType, String whereClause, List<QueryParameter> parameters)
        {
            return LoadData(entityType, whereClause, parameters, "");
        }

        public List<Object> LoadData(Type entityType, String whereClause, List<QueryParameter> parameters, String orderBy)
        {
            List<Object> res = new List<Object>();

            if (m_Schema == null) throw new Exception("The schema is not initialized.");

            EntitySchema schema = m_Schema;
            EntityInfo ei = EntityUtil.Instance.GetEntityInfo(entityType, schema);
            //Type t = Reflector.Instance.ReadType("SupplyManagementMk1Sharp", ei.Package + "." + ei.EntityName);
            MySqlConnection cnx = ConnectionManager.Instance.CurrentConnection;
            MySqlCommand cmd = cnx.CreateCommand();
            String szWhere = "";

            if (!String.IsNullOrEmpty(whereClause))
                szWhere += " where " + whereClause;

            if (!String.IsNullOrEmpty(orderBy))
                szWhere += " order by " + orderBy;

            cmd.CommandText = "Select * from " + ei.TableName + szWhere + ";";

            if(parameters != null)
            foreach (QueryParameter queryParameter in parameters)
                cmd.Parameters.Add(new MySqlParameter(queryParameter.ParameterName, queryParameter.Value));

            cnx.Open();
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Object o = Reflector.Instance.CreateInstance(entityType); // t -> entityType

                foreach (IFieldEntity fe in ei.Fields)
                {
                    if (reader[fe.GetColName()] != null)
                    {
                        Object val;

                        try
                        {
                            val = System.Convert.ChangeType(reader[fe.GetColName()].ToString(), fe.GetFieldType());
                        }
                        catch (Exception ex)
                        {
                            val = reader[fe.GetColName()];
                            System.Console.WriteLine(ex.Message);
                        }
                        Reflector.Instance.SetPropertyValue(fe.GetFieldName(), o, val);
                    }
                    else
                        Reflector.Instance.SetPropertyValue(fe.GetFieldName(), o, null);
                }

                res.Add(o);
            }

            cnx.Close();

            return res;
        }

        public void Save(Object o)
        {
            if (m_Schema == null) throw new Exception("The schema is not initialized.");

            try
            {
                MySqlConnection cnx = ConnectionManager.Instance.CurrentConnection;
                MySqlCommand cmd = cnx.CreateCommand();
                EntityInfo ei = EntityUtil.Instance.GetEntityInfo(o.GetType(), m_Schema);
                String query = "Insert into " + ei.TableName;

                cnx.Open();

                List<QueryParameter> fields = new List<QueryParameter>();
                foreach (IFieldEntity fe in ei.Fields)
                {
                    if (!fe.GetType().Equals(typeof(FieldPrimaryKey)))
                    {
                        Object val = Reflector.Instance.GetPropertyValue(fe.GetFieldName(), o);
                        QueryParameter param = new QueryParameter(fe.GetColName(), val);
                        fields.Add(param);
                        cmd.Parameters.Add(new MySqlParameter(param.ParameterName, param.Value));
                    }
                }

                int i = 0;
                query += "(";
                foreach (QueryParameter field in fields)
                {
                    query += field.ParameterName;

                    if (i++ < fields.Count - 1)
                        query += ", ";
                }
                query += ") ";

                i = 0;
                query += "values (";
                foreach (QueryParameter field in fields)
                {
                    query += "@" + field.ParameterName;

                    if (i++ < fields.Count - 1)
                        query += ", ";
                }
                query += ");";

                cmd.CommandText = query;
                cmd.ExecuteNonQuery();

                //On d�finit le pk une fois que l'entit� est persist�
                foreach (IFieldEntity fe in ei.Fields)
                    if (fe.GetType().Equals(typeof(FieldPrimaryKey)))
                    {
                        Reflector.Instance.SetPropertyValue(fe.GetFieldName(), o, (object) cmd.LastInsertedId);
                        break;
                    }

                cnx.Close();
            }
            catch (Exception e)
            {
                MySqlConnection cnx = ConnectionManager.Instance.CurrentConnection;
                throw new Exception(e.Message);
            }
        }

        public void Update(Object o)
        {
            if (m_Schema == null) throw new Exception("The schema is not initialized.");

            try
            {
                MySqlConnection cnx = ConnectionManager.Instance.CurrentConnection;
                MySqlCommand cmd = cnx.CreateCommand();
                EntityInfo ei = EntityUtil.Instance.GetEntityInfo(o.GetType(), m_Schema);
                String query = "Update " + ei.TableName + " set ";

                cnx.Open();

                List<QueryParameter> fields = new List<QueryParameter>();
                foreach (IFieldEntity fe in ei.Fields)
                {
                    if (!fe.GetType().Equals(typeof(FieldPrimaryKey)))
                    {
                        Object val = Reflector.Instance.GetPropertyValue(fe.GetFieldName(), o);
                        QueryParameter param = new QueryParameter(fe.GetColName(), val);
                        fields.Add(param);
                        cmd.Parameters.Add(new MySqlParameter(param.ParameterName, param.Value));
                    }
                }

                int i = 0;                
                query += "";
                foreach (QueryParameter field in fields)
                {
                    query += field.ParameterName + " = @" + field.ParameterName;

                    if (i++ < fields.Count - 1)
                        query += ", ";
                }

                FieldPrimaryKey fpk = null;
                foreach (IFieldEntity fe in ei.Fields)
                    if (fe.GetType().Equals(typeof(FieldPrimaryKey)))
                    {
                        fpk = (FieldPrimaryKey)fe;
                        break;
                    }

                Object pkVal = Reflector.Instance.GetPropertyValue(fpk.GetFieldName(), o);

                query += " where " + fpk.GetColName() + " = '" + pkVal + "';";

                cmd.CommandText = query;
                cmd.ExecuteNonQuery();

                cnx.Close();
            }
            catch (Exception e)
            {
                MySqlConnection cnx = ConnectionManager.Instance.CurrentConnection;
                throw new Exception(e.Message);
            }
        }

        public void Delete(Object o)
        {
            if (m_Schema == null) throw new Exception("The schema is not initialized.");

            try
            {
                MySqlConnection cnx = ConnectionManager.Instance.CurrentConnection;
                MySqlCommand cmd = cnx.CreateCommand();
                EntityInfo ei = EntityUtil.Instance.GetEntityInfo(o.GetType(), m_Schema);
                String query = "Delete from " + ei.TableName + " where ";

                cnx.Open();

                foreach (IFieldEntity fe in ei.Fields)
                {
                    if (fe.GetType().Equals(typeof(FieldPrimaryKey)))
                    {
                        Object val = Reflector.Instance.GetPropertyValue(fe.GetFieldName(), o);
                        QueryParameter param = new QueryParameter(fe.GetColName(), val);
                        cmd.Parameters.Add(new MySqlParameter(param.ParameterName, param.Value));
                        query += fe.GetColName() + " = @" + fe.GetColName();

                        break;
                    }
                }

                cmd.CommandText = query;
                cmd.ExecuteNonQuery();

                cnx.Close();
            }
            catch (Exception e)
            {
                MySqlConnection cnx = ConnectionManager.Instance.CurrentConnection;
                throw new Exception(e.Message);
            }
        }

        public int ExecuteNonQuery(String query)
        {
            int res = 0;

            MySqlConnection cnx = ConnectionManager.Instance.CurrentConnection;
            MySqlCommand cmd = cnx.CreateCommand();
            cmd.CommandText = query;
            
            cnx.Open();

            res = cmd.ExecuteNonQuery();

            cnx.Close();

            return res;
        }
    }
}
