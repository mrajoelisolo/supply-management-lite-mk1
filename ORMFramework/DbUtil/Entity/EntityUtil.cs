using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework.DbUtil.Entity
{
    public class EntityUtil
    {
        private static EntityUtil m_Instance = new EntityUtil();

        private EntityUtil() { }

        public static EntityUtil Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public EntityInfo GetEntityInfo(Type entityType, EntitySchema schema)
        {
            foreach (EntityInfo ei in schema.EntitySet)
                if (entityType.Name.Equals(ei.EntityName))
                    return ei;

            return null;
        }

        public String GetTableName(Type entityType, EntitySchema schema)
        {
            EntityInfo ei = GetEntityInfo(entityType, schema);
            if (ei == null) return "";
            return ei.TableName;
        }
    }
}
