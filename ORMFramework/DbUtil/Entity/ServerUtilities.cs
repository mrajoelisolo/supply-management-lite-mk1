using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;

namespace ORMFramework.DbUtil.Entity
{
    public class ServerUtilities
    {
        private static ServerUtilities m_Instance = new ServerUtilities();

        private ServerUtilities() { }

        public static ServerUtilities Instance
        {
            get { return m_Instance; }
        }

        public DateTime CurrentTime
        {
            get
            {
                try
                {
                    MySqlConnection cnx = ConnectionManager.Instance.CurrentConnection;
                    if (cnx == null) return DateTime.Now;

                    cnx.Open();
                    MySqlCommand cmd = cnx.CreateCommand();
                    cmd.CommandText = "SELECT CURDATE();";
                    MySqlDataReader reader = cmd.ExecuteReader();
                    reader.Read();
                    return (DateTime) reader[0];
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }
    }
}
