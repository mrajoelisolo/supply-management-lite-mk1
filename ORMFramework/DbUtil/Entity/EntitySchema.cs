using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework.DbUtil.Entity
{
    public class EntitySchema
    {
        private String m_SchemaName;

        public String SchemaName
        {
            get { return m_SchemaName; }
            set { m_SchemaName = value; }
        }

        private List<Entity.EntityInfo> m_EntitySet = new List<Entity.EntityInfo>();

        public List<Entity.EntityInfo> EntitySet
        {
            get { return m_EntitySet; }
            set { m_EntitySet = value; }
        }
	
    }
}
