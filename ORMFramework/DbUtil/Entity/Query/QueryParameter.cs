using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework.DbUtil.Entity.Query
{
    public class QueryParameter
    {
        public QueryParameter(String parameterName, Object value)
        {
            m_ParameterName = parameterName;
            m_Value = value;
        }

        private String m_ParameterName;

        public String ParameterName
        {
            get { return m_ParameterName; }
            set { m_ParameterName = value; }
        }

        private Object m_Value;

        public Object Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }
	
    }
}
