using System;
using System.Collections.Generic;
using System.Text;

namespace ORMFramework.AppUtil
{
    public interface IObserver
    {
        void Notify(String msg);
    }
}
